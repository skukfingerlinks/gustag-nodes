<?php

$lang['subscribe_submit']='Invia candidatura';

$lang['data']='Dati';
$lang['no']='No';
$lang['yes']='S&igrave;';
$lang['facebook']='Facebook';
$lang['fbregistration']='Registrazione Facebook';
$lang['none']='Nessun';
$lang['list']='Elenco';

$lang['username_not_unique']='Questo username &egrave; gi&agrave; in uso. Ti preghiamo di scegliere un altro username';
$lang['usermail_not_unique']='Questa mail &egrave; gi&agrave; in uso. Ti preghiamo di scegliere un altra mail';

$lang['yours']='I tuoi';
$lang['back_to']='Torna a';
$lang['go_to']='Vai a';
$lang['settings']='Impostazioni';
$lang['home'] = 'Home';
$lang['none'] = 'Nessuno';
$lang['male'] = 'Maschio';
$lang['female'] = 'Femmina';
$lang['here']='qui';
$lang['save']='Salva';
$lang['create']='Crea';
$lang['create_new']='Crea nuovo';
$lang['reset']='Reset';
$lang['delete']='Rimuovi';
$lang['update'] = 'Aggiorna';
$lang['create_new']='Crea nuovo';
$lang['back']='Torna indietro';
$lang['continue']='Prosegui';
$lang['edit']='Modifica';
$lang['activate']='Attiva';
$lang['deactivate']='Disattiva';
$lang['must_be_admin']='Per accedere qu&igrave; devi essere un amministratore';

$lang['set_inactive']='Disattiva';
$lang['set_active']='Attiva';


$lang['calendar']='Calendario';
$lang['reset_calendar']='Resetta Calendario';
$lang['define_calendar']='Definisci Calendario';
$lang['calendar_help']='Clicca sulle date del Calendario per definire le date di chiusura del Locale<br />Puoi scorrere tra i mesi cliccando sui link in testa alla tabella (&nbsp;<strong>&gt;&gt;</strong>&nbsp; o &nbsp;<strong>&lt;&lt;</strong>&nbsp;)';
$lang['dates_delete_success']='Date rimosse con successo';
$lang['dates_missing']='Non hai definito nessu giorno di chiusura';


/* ERRORI */
$lang['warning'] = 'Attenzione';
$lang['error'] = 'Errore';
$lang['activate_success'] = 'Attivazione completata con successo';
$lang['deactivate_success'] = 'Disattivazione completata con successo';
$lang['removal_success'] = 'Cancellazione completata con successo';
$lang['update_success'] = 'Aggiornamento completato con successo';
$lang['create_success'] = 'Creazione completata con successo';
$lang['delete_success'] = 'Cancellazione completata con successo';
$lang['success'] = 'Operazione completata con successo';
$lang['login_failed'] = 'Impossibile effettuare il login.<br />Ti preghiamo di verificare i dati inseriti';
$lang['fblogin_failed'] = 'Impossibile effettuare il login con Facebook.<br />Ti preghiamo di verificare i dati inseriti';
$lang['login_success'] = 'Login effettuato con successo!';
$lang['login_title'] = 'Login';
$lang['logout'] = 'Esci';
$lang['login'] = 'Login';
$lang['fblogin']='Facebook Login';

$lang['not_allowed']='Accesso non consentito';
$lang['must_be_sysadmin']='Devi essere un sysadmin per poter accedere';
$lang['must_be_admin']='Devi essere un admin per poter accedere';
$lang['must_be_owner']='Devi essere un ristoratore per poter accedere';
$lang['must_be_agent']='Devi essere un agente per poter accedere';


$lang['user_not_unique'] = '<strong>Attenzione!</strong><br />Questo utente risulta gi&agrave; registrato!';
$lang['username_not_unique'] = 'Questo username risulta gi&agrave; registrato.';
$lang['usermail_not_unique'] = 'Questa mail &egrave; gi&agrave; presente.';

/* GENERIC */

$lang['active'] = 'Attivo';
$lang['inactive'] = 'Disattivo';
$lang['agent_provinces'] = 'Province di riferimento';

$lang['agent_provinces'] = 'Province di riferimento';
$lang['agent_provinces_placeholder'] = 'Di seguito le province di riferimento';
$lang['restaurants'] = 'Ristoranti';
$lang['menus'] = 'Men&ugrave;';
$lang['events'] = 'Eventi';
$lang['dishes'] = 'Piatti';
$lang['dish'] = 'Piatto';
$lang['sysadmins'] = 'Amministratori Sistema';
$lang['admins'] = 'Amministratori';
$lang['ingredients'] = 'Ingredienti';

$lang['restaurant'] = 'Ristorante';
$lang['menu'] = 'Men&ugrave;';
$lang['event'] = 'Evento';
$lang['dish'] = 'Piatto';
$lang['ingredient'] = 'Ingrediente';


$lang['sysadmins']='SuperAdmin';
$lang['admins']='Amministratori';
$lang['owners']='Ristoratori';
$lang['agents']='Agenti';
$lang['users']='Utenti';
$lang['bookings']='Prenotazioni';

$lang['found']='Trovati';
$lang['day'] = 'Giorno';
$lang['from'] = 'dal';
$lang['to'] = 'al';

$lang['from_plural'] = 'dalle';
$lang['to_plural'] = 'alle';



$lang['back_to'] = 'Torna a';

$lang['profile'] = 'Il tuo profilo';
$lang['update_profile'] = 'Aggiorna Profilo Utente';
$lang['update_your_profile'] = 'Aggiorna il tuo profilo';
$lang['male'] = 'Uomo';
$lang['female'] = 'Donna';
$lang['dashboard'] = 'Home';
$lang['logout'] = 'Logout';
$lang['not_available_data'] = '...';

/* ROLES */

$lang['sysadmin'] = 'SysAdmin';
$lang['admin'] = 'Admin';
$lang['owner'] = 'Ristoratore';
$lang['agent'] = 'Agente';
$lang['user'] = 'Utente';


$lang['menus'] = 'Menu';
$lang['menus_create'] = 'Crea Men&ugrave;';
$lang['menus_list'] = 'Lista Men&ugrave;';
$lang['events'] = 'Eventi';
$lang['events_list'] = 'Lista Eventi';
$lang['events_create'] = 'Crea Evento';



/* MENU ITEMS */

$lang['restaurant_create'] = 'Crea Locale';
$lang['restaurant_index'] = 'I miei ristoranti';

/* FORM */

$lang['updated_successfully'] = 'Dati aggiornati con successo';
$lang['created_successfully'] = 'Dati inseriti con successo';


$lang['reset_button'] = 'Annulla';
$lang['register_legend'] = 'Registrati';
$lang['register_button'] = 'Registrati';
$lang['edit_legend'] = 'Modifica';
$lang['edit_button'] = 'Aggiorna';

$lang['login_button'] = 'Effettua il login';

/* FORM LABELS AND PLACEHOLDERS */

$lang['user_h2'] = 'Inserisci i tuoi dati';
$lang['user_register_intro'] = 'In publishing and graphic design, <strong>lorem ipsum[1]</strong> is a placeholder text (filler text) commonly used to demonstrate the graphic elements of a document or visual presentation, such as font, typography, and layout, by removing the distraction of meaningful content.';

$lang['user_agents_label']='Agenti associati'; 
$lang['user_agents_help']='Associa/Rimuovi Agenti';
$lang['user_owners_label']='Ristoratori associati all\'Agente'; 
$lang['user_owners_help']='Associa/Rimuovi Ristoratori all\'Agente';

$lang['user_restaurants_label']='Locali associati'; 
$lang['user_restaurants_help']='Associa/Rimuovi Locali';
$lang['user_owners_label']='Ristoratori associati all\'Agente'; 
$lang['user_owners_help']='Associa/Rimuovi Ristoratori all\'Agente';

$lang['regex_help'] = 'Non sono ammessi caratteri speciali. Ti preghiamo di inserire solo lettere e numeri,punti ( . ),trattini ( - ) o underscore ( _ ). I caratteri ammessi vanno da minimo 4 a massimo 255. Grazie';

$lang['usermail_label'] = 'Mail';
$lang['usermail_placeholder'] = 'Inserisci la tua usermail';
$lang['usermail_help'] = 'La tua usermail sar&agrave; utilizzata per il login e per l\'invio di eventuali informazioni';
$lang['usermail_edit_placeholder'] = 'Cambia la tua usermail';

$lang['username_label'] = 'Username';
$lang['username_placeholder'] = 'Inserisci il tuo nickname';
$lang['username_edit_placeholder'] = 'Cambia il tuo nickname';
$lang['username_dialog_title'] = 'Il tuo username';
$lang['username_dialog_message'] = '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa</p>';



$lang['userpwd_label'] = 'Password';
$lang['userpwd_placeholder'] = 'Inserisci la tua password';

$lang['userpwd_edit_placeholder'] = 'Cambia la tua password';
$lang['confirm_userpwd_label'] = 'Conferma la tua password';
$lang['userpwd_help'] = 'Da un minimo di 4 ad un massimo di 255 caratteri alfanumerici';
$lang['confirm_userpwd_placeholder'] = 'Conferma la tua password';
$lang['first_name_label'] = 'Nome';
$lang['first_name_placeholder'] = 'Inserisci il tuo nome';
$lang['first_name_edit_placeholder'] = 'Cambia il tuo nome';

$lang['user_age_label'] = 'Et&agrave;';
$lang['user_age_help'] = 'Inserisci la tua et&agrave;';
$lang['user_age_placeholder'] = 'Inserisci la tua et&agrave;';



$lang['last_name_label'] = 'Cognome';
$lang['last_name_placeholder'] = 'Inserisci il tuo cognome';
$lang['last_name_edit_placeholder'] = 'Cambia il tuo cognome';
$lang['birthday_label'] = 'Inserisci la tua data di nascita';
$lang['birthday_placeholder'] = 'Inserisci la tua data di nascita';
$lang['sex_label'] = 'Definisci il tuo genere';
$lang['sex_placeholder'] = 'Definisci il tuo genere';
$lang['avatar_label'] = 'Avatar';
$lang['avatar_placeholder'] = 'Scegli una immagine per il tuo profilo';
$lang['avatar_placeholder'] = 'Inserisci il tuo avatar';
$lang['avatar_edit_placeholder'] = 'Cambia il tuo avatar';
$lang['status_label'] = 'Status';
$lang['status_help'] = 'Inserisci una tua breve descrizione o il tuo status attuale';
$lang['status_placeholder'] = 'Inserisci il tuo status';
$lang['status_edit_placeholder'] = 'Cambia il tuo status';
$lang['phone_label'] = 'Recapito telefonico';
$lang['phone_placeholder'] = 'Inserisci il tuo recapito telefonico';
$lang['profile_edit_success'] = 'Profilo aggiornato con successo';

$lang['user_role_label'] = 'Ruolo';
$lang['user_role_placeholder'] = 'Indica il ruolo per cui vuoi iscriverti';
$lang['user_role_help'] = 'Indica il ruolo per cui vuoi iscriverti';
$lang['user_role_placeholder'] = 'Indica il ruolo per cui vuoi iscriverti';



$lang['agent_provinces_label'] = 'Province di riferimento';
$lang['agent_provinces_placeholder'] = 'Seleziona le province di riferimento';
$lang['agent_provinces_help'] = 'Seleziona le province di riferimento';
$lang['agent_provinces_edit_placeholder'] = 'Seleziona le province di riferimento';
$lang['agent_provinces_dialog_title'] = 'Riservato agli Agenti';
$lang['agent_provinces_dialog_message'] = '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p><p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>';
$lang['agent_desc_tips']='Inserisci i dati salienti di uno o pi&ugrave; locali del Ristoratore<br /> Ad Es. Nome, telefono e localit&agrave;';

/**********************************     RESTAURANT       ********************************/

$lang['restaurant_create_help'] = 'Inserisci qui i dati essenziali del tuo Locale.<br />Ti preghiamo di voler compilare i dati obbligatori indicati dall\'asterisco arancione (Ad es: <span class="req">Nome del Locale</span>)';

$lang['restaurant_name_label'] = 'Nome del Locale';
$lang['restaurant_name_help'] = 'Ti preghiamo di non utilizzare caratteri speciali nel nome del Locale';
$lang['restaurant_name_placeholder'] = 'Indica il nome del Locale';

$lang['restaurant_name_label'] = 'Nome del Locale';
$lang['restaurant_name_help'] = 'Ti preghiamo di non utilizzare caratteri speciali nel nome del Locale';
$lang['restaurant_name_placeholder'] = 'Indica il nome del Locale';


$lang['restaurant_desc_label'] = 'Descrizione del Locale';
$lang['restaurant_desc_help'] = 'Inserisci una breve descrizione del Locale';
$lang['restaurant_desc_placeholder'] = 'Inserisci una breve descrizione del Locale';


$lang['restaurant_extra_label'] = 'Tipologia Locale';
$lang['restaurant_extra_placeholder'] = 'Ad es: Locale, Trattoria, Sala Ricevimenti, ecc.';

$lang['restaurant_info_label'] = 'Servizi Locale';
$lang['restaurant_info_placeholder'] = 'Ad es: con parcheggio, all\'aperto, ecc.';
$lang['restaurant_info_tooltip'] = 'Per facilitare la ricerca ti consigliamo di non inserire pi&ugrave; di 3/4 servizi';

$lang['restaurant_phone_label'] = 'Telefono del Locale';
$lang['restaurant_phone_help'] = 'Telefono del Locale';
$lang['restaurant_phone_placeholder'] = 'Telefono del Locale';

$lang['restaurant_mail_label'] = 'Mail del Locale';
$lang['restaurant_mail_help'] = 'Indica, se disponibile, l\'indirizzo mail del Locale';
$lang['restaurant_mail_placeholder'] = 'Indica, se disponibile, l\'indirizzo mail del Locale';

$lang['restaurant_website_label'] = 'Sito Web del Locale';
$lang['restaurant_website_help'] = 'Indica l\'indirizzo completo del ristorante. Ad es: http://www.miosito.it ';
$lang['restaurant_website_placeholder'] = 'Ad es: http://www.miosito.it';


$lang['restaurant_closingday_label'] = 'Giorno/i di chiusura del Locale';
$lang['restaurant_closingday_help'] = 'Indica il giorno o i giorni di chiusura del Locale';
$lang['restaurant_closingday_placeholder'] = 'Indica il giorno o i giorni di chiusura del Locale';


$lang['restaurant_address_label'] = 'Indirizzo del Locale';
$lang['restaurant_address_help'] = 'Comincia a digitare qui l\'indirizzo. Ti verranno proposti alcuni indirizzi dai quali potrai scegliere o modificare';
$lang['restaurant_address_placeholder'] = 'Indirizzo del Locale';

$lang['restaurant_description_label'] = 'Descrizione del Locale';
$lang['restaurant_description_help'] = 'Descrivi brevemente il tuo Locale';
$lang['restaurant_description_placeholder'] = 'Descrivi brevemente il tuo Locale';

$lang['restaurant_submit_button'] = 'Crea Locale';
$lang['restaurant_edit_button'] = 'Aggiorna dati Locale';

$lang['restaurant_no_menus'] = 'Nessun men&ugrave;';
$lang['restaurant_no_events'] = 'Nessun evento';


$lang['restaurant_calendar_create'] = 'Creazione Locale completata<br />Ti preghiamo ora di indicare le date di chiusura del Locale';
$lang['restaurant_calendar_warning']='Ti preghiamo di salvare i dati prima di procedere';

$lang['restaurant_seats_label']='Definisci il numero di posti disponibili per la prenotazione';
$lang['restaurant_seats_placeholder']='Numero di posti disponibili per la prenotazione';


$lang['restaurant_logo_label']='Logo/Immagine principale del Locale';
$lang['restaurant_logo_help']='Inserici il logo o l\'immagine principale del Locale';

$lang['missing_name']='Devi indicare il nome';
$lang['missing_email']='Devi indicare la tua mail';
$lang['missing_phone']='Devi indicare un recapito telefonico';
$lang['missing_address']='Devi indicare l\'indirizzo';

$lang['missing_closingdays']='Devi indicare almeno un giorno settimanale di chiusura del Locale';
$lang['missing_restaurant_extra']='Devi indicare almeno una tipologia del Locale';
$lang['missing_restaurant_info']='Devi indicare almeno una servizio/caratteristica del Locale';
$lang['missing_restaurant_address']='Devi indicare l\'indirizzo del del Locale';
$lang['missing_restaurant_dishes']='Prima di definire un men&ugrave; devi creare almeno un piatto per questo Locale';

$lang['missing_closingdays_warning']='Ti consigliamo di indicare almeno un giorno settimanale di chiusura del Locale';
$lang['missing_restaurant_extra_warning']='Ti consigliamo di indicare almeno una tipologia del Locale';
$lang['missing_restaurant_info_warning']='Ti consigliamo di indicare almeno una servizio/caratteristica del Locale';

/**********************************     MENU       ********************************/

$lang['restaurant_missing']='Attenzione. Devi indicare il ristorante';

$lang['menu_dishes_label'] = 'Indica i piatti che compongono il men&ugrave;';
$lang['menu_create_help'] = 'Inserisci qui i dati essenziali del tuo Men&ugrave;.<br />Ti preghiamo di voler compilare i dati obbligatori indicati dall\'asterisco arancione (Ad es: <span class="req">Nome del Men&ugrave;</span>)';

$lang['menu_name_label'] = 'Nome del Men&ugrave;';
$lang['menu_name_desc'] = 'Nome del Men&ugrave;';
$lang['menu_name_help'] = 'Ti preghiamo di non utilizzare caratteri speciali nel nome del Men&ugrave;';
$lang['menu_name_placeholder'] = 'Ad es: Men&ugrave; del Luned&igrave; Men&ugrave; &quot;Tutti i giorni&quot;,ecc';

$lang['menu_desc_label'] = 'Descrizione del Men&ugrave;';
$lang['menu_desc_help'] = 'Inserisci una breve descrizione del Men&ugrave;';
$lang['menu_desc_placeholder'] = 'Inserisci una breve descrizione del Men&ugrave;';


$lang['menu_info_label'] = 'Tipologia Men&ugrave;';
$lang['menu_info_placeholder'] = 'Indica il &quot;tipo&quot; di men&ugrave;';
$lang['menu_info_help'] = 'Ad es: Men&ugrave; Classico, Men&ugrave; Bambini, ecc.';

$lang['menu_extra_label'] = 'Orario Men&ugrave;';
$lang['menu_extra_placeholder'] = 'Ad es: Pranzo, Cena, Colazione, ecc.';
$lang['menu_extra_help'] = 'Indica l\'orario in cui &egrave; disponibile questo men&ugrave;';

$lang['menu_phone_label'] = 'Telefono del Men&ugrave;';
$lang['menu_phone_help'] = 'Telefono del Men&ugrave;';
$lang['menu_phone_placeholder'] = 'Telefono del Men&ugrave;';

$lang['menu_mail_label'] = 'Mail del Men&ugrave;';
$lang['menu_mail_help'] = 'Indica, se disponibile, l\'indirizzo mail del Men&ugrave;';
$lang['menu_mail_placeholder'] = 'Indica, se disponibile, l\'indirizzo mail del Men&ugrave;';

$lang['menu_website_label'] = 'Sito Web del Men&ugrave;';
$lang['menu_website_help'] = 'Indica l\'indirizzo completo del Men&ugrave;. Ad es: http://www.miosito.it ';
$lang['menu_website_placeholder'] = 'Ad es: http://www.miosito.it';


$lang['menu_activeday_label'] = 'Giorno di disponibilit&agrave; del Men&ugrave;';
$lang['menu_activeday_help'] = 'Indica il giorno in cui &egrave; disponibile il men&ugrave; o seleziona &quot;Tutti i giorni&quot;';
$lang['menu_activeday_placeholder'] = 'Giorno di disponibilit&agrave; del Men&ugrave;';


$lang['menu_address_label'] = 'Indirizzo del Men&ugrave;';
$lang['menu_address_help'] = 'Comincia a digitare qui l\'indirizzo. Ti verranno proposti alcuni indirizzi dai quali potrai scegliere o modificare';
$lang['menu_address_placeholder'] = 'Indirizzo del Men&ugrave;';

$lang['menu_description_label'] = 'Descrizione del Men&ugrave;';
$lang['menu_description_help'] = 'Descrivi brevemente il tuo Men&ugrave;';
$lang['menu_description_placeholder'] = 'Descrivi brevemente il tuo Men&ugrave;';



$lang['menu_submit_button'] = 'Crea Men&ugrave;';
$lang['menu_edit_button'] = 'Aggiorna Men&ugrave;';




/**********************************     EVENT       ********************************/

$lang['event_start_label'] = 'Inizio Evento';
$lang['event_start_help'] = 'Definisci la data di inizio dell\'Evento';
$lang['event_start_desc'] = 'Definisci la data di inizio dell\'Evento';
$lang['event_start_placeholder'] = 'Definisci inizio';

$lang['event_logo_label']='Logo/Immagine principale dell\'Evento';
$lang['event_logo_help']='Inserici il logo o l\'immagine principale dell\'Evento';

$lang['event_recurrent_option'] = 'Evento Ricorrente (ad es. Tutti i Mercoled&igrave dalle 20.30 alle 23.30)';
$lang['event_spot_option'] = 'Evento Singolo (ad es. Venerdì 15 Agosto 2013)';

$lang['event_recurrent_date_label'] = 'Dati Evento Ricorrente';
$lang['event_recurrent_date_desc'] = 'Non inserendo la data di inizio o di fine verranno definiti come inizio la data corrente e come fine l\'intervallo di un anno';
$lang['event_recurrent_date_help'] = 'Indica qui il giorno in cui l\'Evento si ripete o indica in basso la data di inizio e fine dell\'Evento';
$lang['event_recurrent_date_placeholder'] = 'Giorno della settimana';


$lang['event_days_label']='Giorno dell\'Evento';
$lang['event_days_help']='Indica qui il giorno della settimana in cui l\'Evento si ripete';

$lang['event_type_label'] = 'Tipologia Evento';
$lang['event_type_help'] = 'Ad esempio: ' . $lang['event_recurrent_option'] . ' fino al 20 Gennaio 2015, o ' . $lang['event_spot_option'];
$lang['event_type_desc'] = 'Definisci la tipologia dell\'Evento';
$lang['event_type_placeholder'] = 'Definisci la tipologia dell\'Evento';


$lang['event_spot_date_label'] = 'Dati Evento Singolo';
$lang['event_spot_date_help'] = 'Definisci date/ore di inizio e fine dell\'Evento';
$lang['event_spot_date_placeholder'] = 'Definisci date/ore di inizio e fine dell\'Evento';
$lang['event_spot_start_label'] = 'Inizio Evento';
$lang['event_spot_end_label'] = 'Fine Evento';

$lang['event_dates_required_title'] = 'Date/ore dell\'Evento';
$lang['event_dates_required_message'] = 'Se l\'Evento non &egrave; un evento &quot;ricorrente&quot; devi definire le date/ore dell\'Evento';

$lang['event_end_label'] = 'Fine Evento';
$lang['event_end_help'] = 'Definisci la data di fine dell\'Evento';
$lang['event_end_desc'] = 'Definisci la data di fine dell\'Evento';
$lang['event_end_placeholder'] = 'Data fine';

$lang['event_dishes_label'] = 'Indica i piatti che compongono il men&ugrave;';
$lang['event_create_help'] = 'Inserisci qui i dati essenziali del tuo Evento.<br />Ti preghiamo di voler compilare i dati obbligatori indicati dall\'asterisco arancione (Ad es: <span class="req">Nome del Evento</span>)';

$lang['event_name_label'] = 'Nome del Evento';
$lang['event_name_desc'] = 'Nome del Evento';
$lang['event_name_help'] = 'Ti preghiamo di non utilizzare caratteri speciali nel nome del Evento';
$lang['event_name_placeholder'] = 'Ad es: Evento del Luned&igrave; Evento &quot;Tutti i giorni&quot;,ecc';

$lang['event_menu_label'] = 'Men&ugrave; Evento';
$lang['event_menu_placeholder'] = 'Indica, se disponibile il men&ugrave; o i men&ugrave; dell\'Evento';
$lang['event_menu_help'] = 'Indica, se disponibile il men&ugrave; o i men&ugrave; dell\'Evento';
$lang['event_menu_desc'] = 'Indica, se disponibile il men&ugrave; o i men&ugrave; dell\'Evento';


$lang['event_info_label'] = 'Tipologia Evento';
$lang['event_info_placeholder'] = 'Indica il &quot;tipo&quot; di men&ugrave;';
$lang['event_info_help'] = 'Ad es: Evento Classico, Evento Bambini, ecc.';

$lang['event_extra_label'] = 'Orario Evento';
$lang['event_extra_placeholder'] = 'Ad es: Pranzo, Cena, Colazione, ecc.';
$lang['event_extra_help'] = 'Indica l\'orario in cui &egrave; disponibile questo men&ugrave;';

$lang['event_phone_label'] = 'Telefono del Evento';
$lang['event_phone_help'] = 'Telefono del Evento';
$lang['event_phone_placeholder'] = 'Telefono del Evento';

$lang['event_mail_label'] = 'Mail del Evento';
$lang['event_mail_help'] = 'Indica, se disponibile, l\'indirizzo mail del Evento';
$lang['event_mail_placeholder'] = 'Indica, se disponibile, l\'indirizzo mail del Evento';

$lang['event_desc_label'] = 'Descrizione dell\'Evento';
$lang['event_desc_help'] = 'Inserisci se vuoi una breve descrizione dell\'Evento';
$lang['event_desc_placeholder'] = 'Inserisci se vuoi una breve descrizione dell\'Evento';


$lang['event_website_label'] = 'Sito Web del Evento';
$lang['event_website_help'] = 'Indica l\'indirizzo completo del Evento. Ad es: http://www.miosito.it ';
$lang['event_website_placeholder'] = 'Ad es: http://www.miosito.it';


$lang['event_activeday_label'] = 'Giorno di disponibilit&agrave; del Evento';
$lang['event_activeday_help'] = 'Indica il giorno in cui &egrave; disponibile il men&ugrave; o seleziona &quot;Tutti i giorni&quot;';
$lang['event_activeday_placeholder'] = 'Giorno di disponibilit&agrave; del Evento';


$lang['event_address_label'] = 'Indirizzo del Evento';
$lang['event_address_help'] = 'L\'indirizzo proposto &egrave; quello del Locale. Se l\'evento si svolger&agrave; altrove ti preghiamo di inserire l\'indirizzo';
$lang['event_address_placeholder'] = 'Indirizzo del Evento';

$lang['event_description_label'] = 'Descrizione del Evento';
$lang['event_description_help'] = 'Descrivi brevemente il tuo Evento';
$lang['event_description_placeholder'] = 'Descrivi brevemente il tuo Evento';

$lang['event_menus_label'] = 'Men&ugrave; Evento';
$lang['event_menus_help'] = 'Men&ugrave; Evento';
$lang['event_menus_placeholder'] = 'Definisci i Men&ugrave; Evento';
$lang['event_menus_missing']='Devi definire almeni un men&ugrave; per questo Evento';

$lang['event_info_label'] = 'Inserisci le caratteristiche dell\'Evento';
$lang['event_info_help'] = 'Inserisci le caratteristiche dell\'Evento';
$lang['event_info_placeholder'] = 'Inserisci le caratteristiche dell\'Evento';

$lang['event_seats_label']='Definisci il numero di posti disponibili per la prenotazione';
$lang['event_seats_placeholder']='Numero di posti disponibili per la prenotazione';


$lang['event_submit_button'] = 'Crea Evento';
$lang['event_submit_button'] = 'Crea Evento';

$lang['event_recurrent_start_day_missing']='Devi indicare il giorno di inizio dell\'evento';
$lang['event_recurrent_end_day_missing']='Devi indicare il giorno di fine dell\'evento';

$lang['missing_menu_active_days']='Devi indicare almeno un giorno di disponibilit&agrave; del Men&ugrave';
$lang['missing_menu_dishes']='Devi indicare almeno un piatto che compone questo Men&ugrave';
$lang['missing_menu_type']='Devi indicare la tipologia di questo Men&ugrave';
$lang['missing_menu_time']='Devi indicare l\'orario di disponibilit&agrave; di questo Men&ugrave';
$lang['missing_event_menu']='Devi indicare almeno un men&ugrave; per questo Evento';

/**********************************     DISH       ********************************/

$lang['dish_start_label'] = 'Inizio Piatto';
$lang['dish_start_help'] = 'Definisci la data di inizio del Piatto';
$lang['dish_start_desc'] = 'Definisci la data di inizio del Piatto';
$lang['dish_start_placeholder'] = 'Definisci inizio';


$lang['dish_recurrent_option'] = 'Piatto Ricorrente (ad es. Tutti i Mercoled&igrave dalle 20.30 alle 23.30)';
$lang['dish_spot_option'] = 'Piatto Singolo (ad es. Venerdì 15 Agosto 2013)';

$lang['dish_recurrent_date_label'] = 'Dati Piatto Ricorrente';
$lang['dish_recurrent_date_desc'] = 'Non inserendo la data di inizio o di fine verranno definiti come inizio la data corrente e come fine l\'intervallo di un anno';
$lang['dish_recurrent_date_help'] = 'Indica qui il giorno in cui l\'Piatto si ripete o indica in basso la data di inizio e fine del Piatto';
$lang['dish_recurrent_date_placeholder'] = 'Indica qui il giorno in cui l\'Piatto si ripete';

$lang['dish_logo_label']='Logo/Immagine principale del Piatto';
$lang['dish_logo_help']='Inserici il logo o l\'immagine principale del Piatto';

$lang['dish_type_label'] = 'Tipologia Piatto';
$lang['dish_type_help'] = 'Ad esempio: ' . $lang['dish_recurrent_option'] . ' fino al 20 Gennaio 2015, o ' . $lang['dish_spot_option'];
$lang['dish_type_desc'] = 'Definisci la tipologia del Piatto';
$lang['dish_type_placeholder'] = 'Definisci la tipologia del Piatto';


$lang['dish_spot_date_label'] = 'Dati Piatto Singolo';
$lang['dish_spot_date_help'] = 'Definisci date/ore di inizio e fine del Piatto';
$lang['dish_spot_date_placeholder'] = 'Definisci date/ore di inizio e fine del Piatto';
$lang['dish_spot_start_label'] = 'Inizio Piatto';
$lang['dish_spot_end_label'] = 'Fine Piatto';

$lang['dish_dates_required_title'] = 'Date/ore del Piatto';
$lang['dish_dates_required_message'] = 'Se l\'Piatto non &egrave; un disho &quot;ricorrente&quot; devi definire le date/ore del Piatto';

$lang['dish_end_label'] = 'Fine Piatto';
$lang['dish_end_help'] = 'Definisci la data di fine del Piatto';
$lang['dish_end_desc'] = 'Definisci la data di fine del Piatto';
$lang['dish_end_placeholder'] = 'Data fine';

$lang['dish_dishes_label'] = 'Indica i piatti che compongono il men&ugrave;';
$lang['dish_create_help'] = 'Inserisci qui i dati essenziali del tuo Piatto.<br />Ti preghiamo di voler compilare i dati obbligatori indicati dall\'asterisco arancione (Ad es: <span class="req">Nome del Piatto</span>)';

$lang['dish_name_label'] = 'Nome del Piatto';
$lang['dish_name_desc'] = 'Nome del Piatto';
$lang['dish_name_help'] = 'Ti preghiamo di non utilizzare caratteri speciali nel nome del Piatto';
$lang['dish_name_placeholder'] = 'Ad es: Piatto del Luned&igrave; Piatto &quot;Tutti i giorni&quot;,ecc';

$lang['dish_menu_label'] = 'Men&ugrave; Piatto';
$lang['dish_menu_placeholder'] = 'Indica, se disponibile il men&ugrave; o i men&ugrave; del Piatto';
$lang['dish_menu_help'] = 'Indica, se disponibile il men&ugrave; o i men&ugrave; del Piatto';
$lang['dish_menu_desc'] = 'Indica, se disponibile il men&ugrave; o i men&ugrave; del Piatto';


$lang['dish_info_label'] = 'Tipologia Piatto';
$lang['dish_info_placeholder'] = 'Indica il &quot;tipo&quot; di men&ugrave;';
$lang['dish_info_help'] = 'Ad es: Piatto Classico, Piatto Bambini, ecc.';

$lang['dish_extra_label'] = 'Orario Piatto';
$lang['dish_extra_placeholder'] = 'Ad es: Pranzo, Cena, Colazione, ecc.';
$lang['dish_extra_help'] = 'Indica l\'orario in cui &egrave; disponibile questo men&ugrave;';

$lang['dish_phone_label'] = 'Telefono del Piatto';
$lang['dish_phone_help'] = 'Telefono del Piatto';
$lang['dish_phone_placeholder'] = 'Telefono del Piatto';

$lang['dish_mail_label'] = 'Mail del Piatto';
$lang['dish_mail_help'] = 'Indica, se disponibile, l\'indirizzo mail del Piatto';
$lang['dish_mail_placeholder'] = 'Indica, se disponibile, l\'indirizzo mail del Piatto';

$lang['dish_desc_label'] = 'Descrizione del Piatto';
$lang['dish_desc_help'] = 'Inserisci se vuoi una breve descrizione del Piatto';
$lang['dish_desc_placeholder'] = 'Inserisci se vuoi una breve descrizione del Piatto';


$lang['dish_website_label'] = 'Sito Web del Piatto';
$lang['dish_website_help'] = 'Indica l\'indirizzo completo del Piatto. Ad es: http://www.miosito.it ';
$lang['dish_website_placeholder'] = 'Ad es: http://www.miosito.it';


$lang['dish_activeday_label'] = 'Giorno di disponibilit&agrave; del Piatto';
$lang['dish_activeday_help'] = 'Indica il giorno in cui &egrave; disponibile il men&ugrave; o seleziona &quot;Tutti i giorni&quot;';
$lang['dish_activeday_placeholder'] = 'Giorno di disponibilit&agrave; del Piatto';


$lang['dish_address_label'] = 'Indirizzo del Piatto';
$lang['dish_address_help'] = 'L\'indirizzo proposto &egrave; quello del Locale. Se l\'disho si svolger&agrave; altrove ti preghiamo di inserire l\'indirizzo';
$lang['dish_address_placeholder'] = 'Indirizzo del Piatto';

$lang['dish_description_label'] = 'Descrizione del Piatto';
$lang['dish_description_help'] = 'Descrivi brevemente il tuo Piatto';
$lang['dish_description_placeholder'] = 'Descrivi brevemente il tuo Piatto';

$lang['dish_info_label'] = 'Inserisci le caratteristiche del Piatto';
$lang['dish_info_help'] = 'Inserisci le caratteristiche del Piatto';
$lang['dish_info_placeholder'] = 'Inserisci le caratteristiche del Piatto';

$lang['dish_submit_button'] = 'Crea Piatto';
$lang['dish_submit_button'] = 'Crea Piatto';

$lang['dish_recurrent_start_day_missing']='Devi indicare il giorno di inizio del disho';
$lang['dish_recurrent_end_day_missing']='Devi indicare il giorno di fine del disho';


$lang['dish_owner_label']='Propriet&agrave; Piatto';
$lang['dish_owner_help']='Vuoi rendere il tuo piatto disponibile anche agli altri Ristoratori?';

/* TAGS */
$lang['add_tag']='Aggiungi Tag';
$lang['tag_type_label'] = 'Tipologia Tag';
$lang['tag_type_placeholder'] = 'Indica il &quot;tipo&quot; di tag';
$lang['tag_type_help'] = 'Ad es: Tag Classico, Tag Bambini, ecc.';
$lang['tag_name_label'] = 'Nome del Tag';
$lang['tag_name_help'] = 'Ti preghiamo di non utilizzare caratteri speciali nel nome del Tag';
$lang['tag_name_placeholder'] = 'Indica il nome del Tag';
$lang['tag_description_label'] = 'Descrizione del Tag';
$lang['tag_description_help'] = 'Descrivi brevemente il tuo Tag';
$lang['tag_description_placeholder'] = 'Descrivi brevemente il tuo Tag';


/* USERS */
$lang['users']='Utenti';
$lang['user']='Utente';
$lang['user_create']='Crea Utente';

$lang['user_create_help'] = 'Inserisci qui i dati essenziali del tuo Utente.<br />Ti preghiamo di voler compilare i dati obbligatori indicati dall\'asterisco arancione (Ad es: <span class="req">Nome dell\'Utente</span>)';

$lang['user_name_label'] = 'Nome';
$lang['user_name_help'] = 'Ti preghiamo di non utilizzare caratteri speciali nel nome dell\'Utente';
$lang['user_name_placeholder'] = 'Indica il nome dell\'Utente';

$lang['user_roles_label'] = 'Ruoli Gustag';
$lang['user_roles_placeholder'] = 'Indica i Ruoli dell\'Utente';

$lang['user_pwd_label'] = 'Password';
$lang['user_pwd_help'] = 'Ti preghiamo di utilizzare esclusivamente caratteri alfa numerici';
$lang['user_pwd_placeholder'] = 'Password dell\'Utente';


$lang['user_name_label'] = 'Username';
$lang['user_name_help'] = 'Ti preghiamo di non utilizzare caratteri speciali nel nome dell\'Utente';
$lang['user_name_placeholder'] = 'Indica lo Username dell\'Utente';


$lang['user_firstname_label'] = 'Nome';
$lang['user_firstname_help'] = 'Ti preghiamo di non utilizzare caratteri speciali nel nome dell\'Utente';
$lang['user_firstname_placeholder'] = 'Indica il nome dell\'Utente';

$lang['user_lastname_label'] = 'Cognome';
$lang['user_lastname_help'] = 'Ti preghiamo di non utilizzare caratteri speciali nel nome dell\'Utente';
$lang['user_lastname_placeholder'] = 'Indica il Cognome dell\'Utente';

$lang['user_status_label'] = 'Status';
$lang['user_status_help'] = 'Inserisci una breve descrizione dell\'Utente';
$lang['user_status_placeholder'] = 'Inserisci una breve descrizione dell\'Utente';


$lang['user_extra_label'] = 'Tipologia Utente';
$lang['user_extra_placeholder'] = 'Ad es: Utente, Trattoria, Sala Ricevimenti, ecc.';

$lang['user_info_label'] = 'Servizi Utente';
$lang['user_info_placeholder'] = 'Ad es: con parcheggio, all\'aperto, ecc.';
$lang['user_info_tooltip'] = 'Per facilitare la ricerca ti consigliamo di non inserire pi&ugrave; di 3/4 servizi';

$lang['user_phone_label'] = 'Telefono';
$lang['user_phone_help'] = 'Telefono dell\'Utente';
$lang['user_phone_placeholder'] = 'Telefono dell\'Utente';

$lang['user_mail_label'] = 'Mail';
$lang['user_mail_help'] = 'Indica, se disponibile, l\'indirizzo mail dell\'Utente';
$lang['user_mail_placeholder'] = 'Indica, se disponibile, l\'indirizzo mail dell\'Utente';

$lang['user_website_label'] = 'Website';
$lang['user_website_help'] = 'Indica l\'indirizzo completo del ristorante. Ad es: http://www.miosito.it ';
$lang['user_website_placeholder'] = 'Ad es: http://www.miosito.it';


$lang['user_closingday_label'] = 'Giorno/i di chiusura dell\'Utente';
$lang['user_closingday_help'] = 'Indica il giorno o i giorni di chiusura dell\'Utente';
$lang['user_closingday_placeholder'] = 'Indica il giorno o i giorni di chiusura dell\'Utente';


$lang['user_address_label'] = 'Indirizzo dell\'Utente';
$lang['user_address_help'] = 'Comincia a digitare qui l\'indirizzo. Ti verranno proposti alcuni indirizzi dai quali potrai scegliere o modificare';
$lang['user_address_placeholder'] = 'Indirizzo dell\'Utente';

$lang['user_description_label'] = 'Descrizione dell\'Utente';
$lang['user_description_help'] = 'Descrivi brevemente il tuo Utente';
$lang['user_description_placeholder'] = 'Descrivi brevemente il tuo Utente';

$lang['user_submit_button'] = 'Crea Utente';
$lang['user_edit_button'] = 'Aggiorna dati Utente';

$lang['user_no_menus'] = 'Nessun men&ugrave;';
$lang['user_no_events'] = 'Nessun evento';


$lang['user_calendar_create'] = 'Creazione Utente completata<br />Ti preghiamo ora di indicare le date di chiusura dell\'Utente';
$lang['user_calendar_warning']='Ti preghiamo di salvare i dati prima di procedere';

$lang['user_seats_label']='Definisci il numero di posti disponibili per la prenotazione';
$lang['user_seats_placeholder']='Numero di posti disponibili per la prenotazione';

$lang['user_locations_label']='Province Agente';
$lang['user_locations_help']='Definisci le province di competenza dell\'Agente';


$lang['user_avatar_label']='Avatar';
$lang['user_avatar_help']='Definisci l\'avatar dell\'Utente';

/* AGENTS */
$lang['agents_locations_dialog_title']='Creazione Agente';
$lang['agents_locations_dialog_msg']='<strong>Creazione/Registrazione Agente</strong>.<br />In caso di creazione/registrazione di un Agente &egrave, necessario indicare:<br /><ol><li>le <strong>province di competenza</strong></li><li>il <strong>recapito telefonico</strong></ol>';
$lang['missing_user_locations']='Devi indicare almeno una provincia di competenza per l\'Utente';
$lang['missing_user_phone']='Devi indicare almeno un recapito telefonico per l\'Utente';


$lang['missing_user_roles']='Devi indicare almeno un ruolo per l\'Utente';

$lang['missing_name']='Devi indicare il nome';
$lang['missing_email']='Devi indicare la tua mail';
$lang['missing_phone']='Devi indicare un recapito telefonico';
$lang['missing_address']='Devi indicare l\'indirizzo';

$lang['missing_closingdays']='Devi indicare almeno un giorno settimanale di chiusura dell\'Utente';
$lang['missing_user_extra']='Devi indicare almeno una tipologia dell\'Utente';
$lang['missing_user_info']='Devi indicare almeno una servizio/caratteristica dell\'Utente';
$lang['missing_user_address']='Devi indicare l\'indirizzo del dell\'Utente';

$lang['missing_closingdays_warning']='Ti consigliamo di indicare almeno un giorno settimanale di chiusura dell\'Utente';
$lang['missing_user_extra_warning']='Ti consigliamo di indicare almeno una tipologia dell\'Utente';
$lang['missing_user_info_warning']='Ti consigliamo di indicare almeno una servizio/caratteristica dell\'Utente';

/* IMAGE */
$lang['gallery']='Galleria';
$lang['image_upload']='Carica Immagine';
$lang['image_gallery']='Galleria Immagini';
$lang['image_name_label']='Immagine';
$lang['image_label']='Immagine';
$lang['image_desc_label']='Descrizione Immagine';
$lang['image_desc_placeholder']='Inserisci una breve descrizione per questa immagine';
$lang['upload_success']='Upload effettuato con successo';
$lang['missing_upload_image']='Non hai indicato l\'immagine da caricare';

/* GEOPOINTS */
$lang['geopoints']='Luoghi';
$lang['geopoint']='Luogo';
$lang['geopoint_address_label']='Individua Luogo';
$lang['geopoint']='Luogo';
$lang['missing_geopoint_address']='Devi indicare un indirizzo';


