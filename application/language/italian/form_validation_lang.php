<?php

$lang['required'] = "Il campo %s &egrave; obbligatorio";
$lang['isset'] = "Il campo %s non pu&ograve; essere vuoto.";
$lang['valid_email'] = "Il campo %s non &egrave; un indirizzo mail valido.";
$lang['valid_emails'] = "Il campo %s non &egrave; un indirizzo mail valido.";
$lang['valid_url'] = "Il campo %s non &egrave; un indirizzo web valido.";
$lang['valid_ip'] = "Il campo %s non &egrave; un indirizzo IP valido.";
$lang['min_length'] = "Il campo %s deve contenere almeno %s caratteri.";
$lang['max_length'] = "Il campo %s non deve contenere pi&ugrave; di %s caratteri.";
$lang['exact_length'] = "Il campo %s must be exactly %s characters in length.";
$lang['alpha'] = "Il campo %s pu&ograve; contenere solo lettere";
$lang['alpha_numeric'] = "Il campo %s pu&ograve; contenere solo lettere e numeri";
$lang['alpha_dash'] = "Il campo %s pu&ograve; contenere solo lettere e numeri e caratteri speciali(trattini o underscores)";
$lang['numeric'] = "Il campo %s pu&ograve; contenere solo numeri";
$lang['is_numeric'] = "Il campo %s pu&ograve; contenere solo numeri";
$lang['integer'] = "Il campo %s pu&ograve; contenere solo numeri interi";
$lang['regex_match'] = "Il campo %s non &egrave; nel formato corretto.";
$lang['matches'] = "Il campo %s non coincide con il campo %s.";
$lang['is_unique'] = "%s gi&agrave; presente.";//"Questo %s &egrave; gi&agrave; presente.";
$lang['is_natural'] = "Il campo %s pu&ograve; contenere solo numeri positivi";
$lang['is_natural_no_zero'] = "Il campo %s deve contenere solo numeri maggiori di zero";
$lang['decimal'] = "Il campo %s pu&ograve; contenere solo numeri decimali";
$lang['less_than'] = "Il campo %s pu&ograve; contenere solo numeri minori di %s";
$lang['greater_than'] = "Il campo %s pu&ograve; contenere solo numeri maggiori di %s";
$lang['valid_postcode']="Il campo %s non &egrave; un Codice di Avviamento Postale valido";
$lang['valid_password']="Il campo %s non &egrave; una password valida (minimo 5 caratteri - massimo 8 caratteri";

/* End of file form_validation_lang.php */
/* Location: ./application/language/english/form_validation_lang.php */