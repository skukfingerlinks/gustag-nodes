<?php

$lang['upload_userfile_not_set'] = "Non hai definito la variabile userfile.";
$lang['upload_file_exceeds_limit'] = "Le dimensioni del file eccedono quelle consentite";
$lang['upload_file_exceeds_form_limit'] = "Le dimensioni del file eccedono quelle consentite";
$lang['upload_file_partial'] = "Il file &egrave; stato caricato solo parzialmente";
$lang['upload_no_temp_directory'] = "La cartella temporanea &egrave; mancante";
$lang['upload_unable_to_write_file'] = "Non &egrave; stato possibile salvare il file";
$lang['upload_stopped_by_extension'] = "L'estensione del file non &egrave; consentita";
$lang['upload_no_file_selected'] = "Non hai indicato il file da caricare";
$lang['upload_invalid_filetype'] = "L'estensione del file non &egrave; consentita";;
$lang['upload_invalid_filesize'] = "Le dimensioni del file eccedono quelle consentite";
$lang['upload_invalid_dimensions'] = "Le dimensioni (larghezza ed altezza) del file eccedono quelle consentite";
$lang['upload_destination_error'] = "Non &egrave; stato possibile salvare il file";//"A problem was encountered while attempting to move the uploaded file to the final destination.";
$lang['upload_no_filepath'] = "La cartella di destinazione del file &egrave; mancante";
$lang['upload_no_file_types'] = "Non hai indicato le estensioni permesse per il file";
$lang['upload_bad_filename'] = "Il file &egrave; gi&agrave; presente";
$lang['upload_not_writable'] = "La cartella di destinazione del file &egrave; scrivibile";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */