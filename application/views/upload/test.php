<div class="twelve columns content">      
    <h3><?=lang('image_upload')?></h3>
</div>
<?= form_open_multipart("/upload/test", array('id' => 'edit_image')) ?>
<div class="twelve columns">          
    <!--logo-->    
    <p>
        <?= form_label(lang('image_upload'), 'userfile') ?>
        <span><?=img(array('src'=>$this->config->item('restaurant_default_logo'),'class'=>'logo'))?></span>
        <?= form_upload(array('name' => 'userfile', 'id' => 'userfile', 'class' => 'long_input', 'placeholder' => lang('restaurant_logo_help'), 'value' => set_value('logo'))); ?>
        <?= form_error('userfile', '<span class="error" id="userfile_error">', '</span>'); ?>
    </p>      
</div>
<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('create'))) ?></p>
</div>
<?= form_close() ?>


