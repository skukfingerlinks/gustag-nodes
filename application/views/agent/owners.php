<div class="twelve columns">
        <p><?=anchor('/users/'.$html['items_type'],lang('back_to').' '.lang('list').' '.lang($html['items_type']))?></p>
        <h3><?=lang('yours').' '.lang($html['items_type'])?></h3>
        <p>The easiest way to really get started with Skeleton is to check out the full docs and info at <a href="http://www.getskeleton.com">www.getskeleton.com.</a>.</p>
        <p><?= anchor('/agent/create', lang('create').' '.lang('owner'), array('class' => 'button')) ?></p>
        <?=$html['pagination_links']?>
        <table id="paginated" class="tablesorter">
            <caption><?=lang($html['items_type'])?></caption>
            <thead>
                <tr>
                    <th class="first" title="Clicca per ordinare per Stato">Stato</th>                
                    <th title="Clicca per ordinare per Nome" colspan="2">Nome</th>                
                    <th title="Clicca per ordinare per Mail">Mail</th>   
                    <?php if($this->usersession->is_admin){?>
                    <th><?=lang('delete')?></th>   
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($html['found'] as $item){?>
                <tr>
                    <td>
                        <?php if($item->active==1){?>
                            <?php if($this->usersession->is_admin){?>
                                <?=anchor('/nodes/deactivate/'.$item->id,lang('active'),array('class'=>'active tooltip','title'=>lang('set_inactive')))?>
                            <?php }else{ ?>
                                <?=lang('active')?>
                            <?php } ?>
                        <?php }else{ ?>
                            <?php if($this->usersession->is_admin){?>
                                <?=anchor('/nodes/activate/'.$item->id,lang('active'),array('class'=>'active tooltip','title'=>lang('set_active')))?>
                            <?php }else{ ?>
                                <?=lang('inactive')?>
                            <?php } ?>                                                
                        <?php } ?>
                    </td>
                    <td>
                        <?=anchor('/agent/relate_'.$html['relate'].'/'.$item->id,img(array('src'=>$item->avatar,'alt'=>$item->username,'class'=>'avatar')))?>
                    </td>
                    <td>
                        <?php if($this->usersession->is_admin){?>
                                <?=anchor('/agent/relate_'.$html['relate'].'/'.$item->id,$item->username)?>
                        <?php }else{ ?>
                                agent<?=$item->username?>
                        <?php } ?>                           
                    </td>
                    <td><?=$item->usermail?></td>
                    <?php if($this->usersession->is_admin){?>
                    <td><?=anchor('/nodes/delete/'.$item->id,lang('delete'),array('class'=>'delete tooltip','title'=>lang('delete')))?></td>
                    <?php } ?>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?=$html['pagination_links']?>        
    </div>
</div><!-- container -->

