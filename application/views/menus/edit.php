<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$restaurant->id,$restaurant->name)?></li>
        <li><?=anchor('/restaurants/calendar/'.$restaurant->id,lang('calendar'))?></li>
        <li><?=anchor('/dishes/restaurant/'.$restaurant->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$restaurant->id,lang('menus'))?></li>
        <li><?=anchor('/events/restaurant/'.$restaurant->id,lang('events'))?></li>
        <li><?=anchor('/restaurants/gallery/'.$restaurant->id,lang('image_gallery'))?></li>
    </ul>  
    <div class="clear"></div>     
    <h4><?=lang('restaurant').' '.$restaurant->name?></h4>
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$owner->username?></p>
    <h3><?=lang('menu').': '.$data->name?></h3>      

</div>
<?= form_open("/menus/edit/" . $data->id, array('id' => 'edit_menu')) ?>
<div class="six columns">
    <!--name-->
    <p>
        <?= form_label(lang('menu_name_label'), 'name', array('class' => 'req')) ?>
        <?= form_error('usermail', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'name', 'id' => 'name', 'class' => 'long_input small-corners', 'placeholder' => lang('menu_name_placeholder'), 'value' => $data->name)); ?></span>
    </p>
    <!--desc-->
    <p>
        <?= form_label(lang('menu_desc_label'), 'desc', array('class' => 'req')) ?>
        <?= form_error('desc', '<span class="error" id="desc_error">', '</span>'); ?>
        <span><?= form_textarea(array('name' => 'desc','cols'=>80, 'rows'=>4, 'id' => 'desc', 'class' => 'long_input small-corners', 'placeholder' => lang('menu_desc_placeholder'), 'value' => $data->desc)); ?></span>
    </p>     
    <p>
        <?= form_label(lang('menu_activeday_label'), 'active_days') ?>
        <?= form_error('active_days', '<span class="error">', '</span>'); ?>
        <span><?= form_multiselect('active_days[]', $week_days, $menu_days, 'class="chosen small-corners" multiple="true"') ?></span>
    </p>
    <!--tags-->
    <?php if (isset($tags)) {
        foreach ($tags as $tag) { ?>
            <p>
                <?= form_label($tag['description'], $tag['name']) ?>
        <?= form_error($tag['name'], '<span class="error">', '</span>'); ?>
                <span><?= form_multiselect($tag['name'] . '[]', $tag['options'], $tag['selected'], 'class="chosen small-corners" multiple="true"') ?></span>
            </p>
    <?php }
} ?>
</div>
<div class="six columns">
    <!--dishes-->
    <?php foreach ($dishes as $type => $dish) { ?>
        <?= form_label($type, 'dishes') ?>
        <?= form_error($type, '<span class="error">', '</span>'); ?>
        <span><?= form_multiselect('dishes[]', $dish, $menu_dishes, 'class="chosen small-corners" multiple="true"') ?></span>
<?php } ?>
</div>
<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('menu_edit_button'))) ?></p>
</div>
<?= form_close() ?>


