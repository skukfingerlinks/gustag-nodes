<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$restaurant->id,$restaurant->name)?></li>
        <li><?=anchor('/restaurants/calendar/'.$restaurant->id,lang('calendar'))?></li>
        <li><?=anchor('/dishes/restaurant/'.$restaurant->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$restaurant->id,lang('menus'))?></li>
        <li><?=anchor('/events/restaurant/'.$restaurant->id,lang('events'))?></li> 
        <li><?=anchor('/restaurants/gallery/'.$restaurant->id,lang('image_gallery'))?></li>
    </ul>  
    <div class="clear"></div>        
    <h3><?=lang('create').' '.lang('menu').' '.$restaurant->name ?></h3> 
</div>
<?= form_open("/menus/create/" . $restaurant->id, array('id' => 'edit_menu')) ?>
<div class="six columns">
    <!--name-->
    <p>
        <?= form_label(lang('menu_name_label'), 'name', array('class' => 'req')) ?>
        <?= form_error('usermail', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'name', 'id' => 'name', 'class' => 'long_input small-corners', 'placeholder' => lang('menu_name_placeholder'), 'value' => set_value('address'))); ?></span>
    </p>
    <!--desc-->
    <p>
        <?= form_label(lang('menu_desc_label'), 'desc', array('class' => 'req')) ?>
        <?= form_error('desc', '<span class="error" id="desc_error">', '</span>'); ?>
        <span><?= form_textarea(array('name' => 'desc','cols'=>80, 'rows'=>4, 'id' => 'desc', 'class' => 'long_input small-corners', 'placeholder' => lang('menu_desc_placeholder'), 'value' => set_value('desc'))); ?></span>
    </p>     
    <p>
        <?= form_label(lang('menu_activeday_label'), 'active_days', array('class' => 'req')) ?>
        <?= form_error('active_days', '<span class="error">', '</span>'); ?>
        <span><?= form_multiselect('active_days[]', $week_days, '0-Tutti i giorni', 'class="chosen small-corners" multiple="true"') ?></span>
    </p>
    <!--tags-->
    <?php if (isset($tags)) {foreach ($tags as $tag) { ?>
            <p>
                <?= form_label($tag['description'], $tag['name'], array('class' => 'req')) ?>
                <?= form_error($tag['name'], '<span class="error">', '</span>'); ?>
                <span><?= form_multiselect($tag['name'] . '[]', $tag['options'], $tag['selected'], 'id="' . $tag['name'] . '" class="chosen small-corners" multiple="true"') ?></span>
            </p>
    <?php }} ?>    
</div>
<div class="six columns">
    <!--dishes-->
        <!--<p>Se il piatto che cerchi non &egrave; presente crealo <?= anchor('/dishes/create/' . $restaurant->id, 'qui') ?></p>-->
        <?php foreach ($dishes as $type => $dish) { ?>
        <p>
        <?= form_label($type, 'dishes', array('class' => 'req')) ?>
        <?= form_error($type, '<span class="error">', '</span>'); ?>
        <span><?= form_multiselect('dishes[]', $dish, array(), 'class="chosen small-corners" multiple="true"') ?></span>
        </p>
<?php } ?>
</div>
<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('create'))) ?></p>
</div>
<?= form_close() ?>


