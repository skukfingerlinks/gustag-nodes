<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Benvenuto su Gustag.com | Discover your food</title>

        <style type="text/css">

            ::selection{ background-color: #E13300; color: white; }
            ::moz-selection{ background-color: #E13300; color: white; }
            ::webkit-selection{ background-color: #E13300; color: white; }

            body {
                background-color: #fff;
                margin: 40px;
                font: 13px/20px normal Helvetica, Arial, sans-serif;
                color: #4F5155;
            }

            a {
                color: #003399;
                background-color: transparent;
                font-weight: normal;
            }
            ul{
                list-style-type: none;
            }

            h1 {
                color: #444;
                background-color: transparent;
                border-bottom: 1px solid #D0D0D0;
                font-size: 19px;
                font-weight: normal;
                margin: 0 0 14px 0;
                padding: 14px 15px 10px 15px;
            }

            code {
                font-family: Consolas, Monaco, Courier New, Courier, monospace;
                font-size: 12px;
                background-color: #f9f9f9;
                border: 1px solid #D0D0D0;
                color: #002166;
                display: block;
                margin: 14px 0 14px 0;
                padding: 12px 10px 12px 10px;
            }

            #body{
                margin: 0 15px 0 15px;
            }

            p.footer{
                text-align: right;
                font-size: 11px;
                border-top: 1px solid #D0D0D0;
                line-height: 32px;
                padding: 0 10px 0 10px;
                margin: 20px 0 0 0;
            }

            #container{
                margin: 10px;
                border: 1px solid #D0D0D0;
                -webkit-box-shadow: 0 0 8px #D0D0D0;
            }
        </style>
    </head>
    <body>
        <div id="container">

            <h1><?= $subject ?></h1>
            <div id="body">
                <p>Grazie <strong><?= $user->username ?></strong>.</p>
                <p>La tua registrazione &egrave; stata completata con successo</p>
                <p>Di seguito il riepilogo dei dati di registrazione</p>
                <ul>
                    <li>la tua username: <?= $user->username ?></li>
                    <li>la tua email: <?= $user->usermail ?></li>                    
                    <?php if (isset($user->plainpwd)) { ?><li>la tua password: <?= $user->plainpwd ?></li><?php } ?>
                    <?php if (isset($user->first_name)) { ?><li>il tuo nome: <?= $user->first_name ?></li><?php } ?>
                    <?php if (isset($user->last_name)) { ?><li>il tuo cognome: <?= $user->last_name ?></li><?php } ?>
                    <?php if (isset($user->age) AND $user->age > 0) { ?><li>la tua et&agrave;: <?= $user->age ?></li><?php } ?>
                    <?php if (isset($user->phone) AND strlen($user->phone) > 0) { ?><li>il tuo recapito telefonico: <?= $user->phone ?></li><?php } ?>
                    <li>i tuoi ruoli:
                        <ul>
                    <?php foreach($user->roles as $role){ ?>
                    <li><?= $role->description ?></li>
                    <?php } ?>
                        </ul>
                    </li>                    
                </ul>
                <p>Potrai procedere al login <?=anchor('/login','qui')?></p>
            </div>
            <p class="footer">Gustag.com | Discover your food</p>
        </div>
    </body>
</html>