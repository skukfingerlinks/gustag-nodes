<?= $subject ?>\r\n\r\n
Grazie ?= $user->username ?>.\r\n\r\n
Il suo profilo &egrave; stato attivato\r\n\r\n
Di seguito il riepilogo dei dati di registrazione\r\n\r\n
la tua username: <?= $user->username ?>\r\n\r\n
la tua email: <?= $user->usermail ?>\r\n\r\n                    
<?php if (isset($user->plainpwd)) { ?>\tla tua password: <?= $user->plainpwd ?>\r\n\r\n<?php } ?>
<?php if (isset($user->first_name)) { ?>\til tuo nome: <?= $user->first_name ?>\r\n\r\n<?php } ?>
<?php if (isset($user->last_name)) { ?>\til tuo cognome: <?= $user->last_name ?>\r\n\r\n<?php } ?>
<?php if (isset($user->age) AND $user->age > 0) { ?>\tla tua et&agrave;: <?= $user->age ?>\r\n\r\n<?php } ?>
<?php if (isset($user->phone) AND strlen($user->phone) > 0) { ?>\til tuo recapito telefonico: <?= $user->phone ?>\r\n\r\n<?php } ?>
i tuoi ruoli:\r\n\r\n
<?php foreach($user->roles as $role){ ?>
\t<?= $role->description ?>\r\n\r\n
<?php } ?>

