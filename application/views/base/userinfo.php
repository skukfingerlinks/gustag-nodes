<div class="four columns">
    <?php if(isset($controller_help)){ ?>
    <h4>controller_help</h4>
    <?=lang($controller_help)?>
    <p>Skeleton is a small collection of well-organized CSS files that can help you rapidly develop sites that look beautiful at any size, be it a 17" laptop screen or an iPhone.</p>    
    <?php } ?>    
    <div id="userbox" class="small-corners">
        <p><?= img(array('src' => $usersession->avatar, 'class' => 'avatar')) ?></p>
        <p><?= $usersession->username ?></p>
        <p><?= $usersession->status ?></p>
        <p><?=anchor('/users/profile',lang('profile'),'id="profile"')?></p>
    </div>

    <!--
    <ul class="top-level">
        <?php if (isset($role_menu)) { ?>
            <?php foreach ($role_menu as $link) { ?>
                <li><?= anchor($link['url'], $link['link'], $link['props']) ?>
                    <?php if (isset($link['submenu'])) { ?>
                        <ul class="sub-level">
                            <?php foreach ($link['submenu'] as $link) { ?>
                                <li><?= anchor($link['url'], $link['link'], $link['props']) ?></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        <?php } ?>
    </ul>
    -->
    <!--
    <ul class="top-level">
        <li><a href="#">Home</a>
            <ul class="sub-level">
                <li><a href="#">Sub Menu Item 1</a>
                </li>
                <li>
                    <a href="#">Sub Menu Item 2</a>
                    <ul class="sub-level">
                        <li><a href="#">Sub Sub Menu Item 1</a></li>
                        <li><a href="#">Sub Sub Menu Item 2</a></li>
                        <li><a href="#">Sub Sub Menu Item 3</a></li>
                        <li><a href="#">Sub Sub Menu Item 4</a></li>
                    </ul>
                </li>
                <li><a href="#">Sub Menu Item 3</a></li>
                <li><a href="#">Sub Menu Item 3</a></li>
            </ul>
        </li>
        <li><a href="#">About</a></li>
        <li><a href="#">Contact</a></li>
        <li>
            <a href="#">FAQ</a>
            <ul class="sub-level">
                <li><a href="#">Sub Menu Item 1</a></li>
                <li>
                    <a href="#">Sub Menu Item 3</a>
                    <ul class="sub-level">
                        <li><a href="#">Sub Sub Menu Item 1</a></li>
                        <li><a href="#">Sub Sub Menu Item 2</a></li>
                        <li><a href="#">Sub Sub Menu Item 3</a></li>
                        <li><a href="#">Sub Sub Menu Item 4</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">News</a>
            <ul class="sub-level">
                <li><a href="#">Sub Menu Item 1</a>
                    <ul class="sub-level">
                        <li><a href="#">Sub Sub Menu Item 1</a></li>
                        <li><a href="#">Sub Sub Menu Item 2</a></li>
                        <li><a href="#">Sub Sub Menu Item 3</a></li>
                        <li><a href="#">Sub Sub Menu Item 4</a></li>
                    </ul>
                </li>
                <li><a href="#">Sub Menu Item 2</a></li>
                <li><a href="#">Sub Menu Item 3</a></li>
            </ul>
        </li>
    </ul>
    -->
<p>Skeleton is a small collection of well-organized CSS files that can help you rapidly develop sites that look beautiful at any size, be it a 17" laptop screen or an iPhone. It's based on a responsive grid, but also provides very basic CSS for typography, buttons, forms and media queries. Go ahead, resize this super basic page to see the grid in action.</p>    
</div>