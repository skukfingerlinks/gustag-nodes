<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="<?= $html['head_lang'] ?>"> <!--<![endif]-->
    <head>
        <meta charset="<?= $html['meta_charset'] ?>">
        <title><?= $html['page_title'] ?></title>
        <meta name="description" content="<?= $html['meta_description'] ?>">
        <meta name="author" content="<?= $html['meta_author'] ?>">
        <meta name="viewport" content="<?= $html['meta_viewport'] ?>">
        <?php if (isset($html['global_css'])) { ?>
            <?php foreach ($html['global_css'] as $stylesheet) { ?>
                <link rel="stylesheet" href="<?= $stylesheet['src'] ?>" media="<?= $stylesheet['media'] ?>">
            <?php } ?>
        <?php } ?>
        <?php if (isset($html['extra_css'])) { ?>
            <?php foreach ($html['extra_css'] as $stylesheet) { ?>
                <link rel="stylesheet" href="<?= $stylesheet['src'] ?>" media="<?= $stylesheet['media'] ?>">
            <?php } ?>
        <?php } ?>
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link rel="shortcut icon" href="/assets/img/favicon.ico">
        <link rel="apple-touch-icon" href="/assets/img/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/assets/img/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/assets/img/apple-touch-icon-114x114.png">
    </head>
    <body>
        <div id="wrapper">
            <div class="container" id="header">
                <div class="sixteen columns">
                    <h1><?= $html['page_title'] ?></h1>
                        <p><?= $html['h2_payoff'] ?></p>
                </div>
                <div class="sixteen columns">
                    <ul class="dropdown" id="menutop">                        
                        <?php if (isset($role_menu)) { ?>
                            <?php foreach ($role_menu as $link) { ?>
                                <li><?= anchor($link['url'], $link['link'], $link['props']) ?>
                                    <?php if (isset($link['submenu'])) { ?>
                                        <ul>
                                            <?php foreach ($link['submenu'] as $link) { ?>
                                                <li><?= anchor($link['url'], $link['link'], $link['props']) ?></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        <?php } else { ?>
                            <li><a href="/" title="Home">Home</a>
                            <li><a href="#" title="Page1">Page1</a>
                                <ul>
                                    <li><a href="#" title="SubPage1.1">SubPage1.1</a></li>
                                    <li><a href="#" title="SubPage1.2">SubPage1.2</a></li>
                                </ul>
                            </li>
                            <li class="last"><a href="/login" title="Login">Login</a></li>
                        <?php } ?>
                    </ul>                    
                </div>
            </div>
            <div class="container" id="topmenu">
                <div class="sixteen columns">

                </div>
            </div>
            <div class="container" id="content">
                <?php if (isset($showuserinfo)) $this->load->view('base/userinfo',$this->viewdata) ?>