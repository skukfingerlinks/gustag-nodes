</div>
<div class="container" id="bottommenu">
<div class="sixteen columns">    
    <ul class="dropup" id="menubottom">
        <li><a href="/" title="Home">Home</a></li>
        <li><a href="#" title="Page1">Page1</a>
            <ul>
                <li><a href="#" title="SubPage1.1">SubPage1.1</a></li>
                <li><a href="#" title="SubPage1.2">SubPage1.2</a></li>
            </ul>
        </li>
        <li class="last"><a href="/login" title="Login">Login</a></li>        
    </ul>
</div>    
</div>
<div class="container" id="footer">
<div class="sixteen columns">
<p><a href="/user_guide/">User Guide</a></p>
<p>Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>
</div>
</div>
<div id="img_preview" class="hidden">
    <p class="center"><img id="img" src="" /></p>
    <!--</p> id="img_preview_content" class="center" >-->
</div>
<?php if ($this->session->flashdata('error')) { ?>
<div id="dialog" class="hidden" title="<?= lang('error') ?>"><p><?= $this->session->flashdata('error') ?></p></div>
<?php } else if ($this->session->flashdata('success')) { ?>
<div id="dialog" class="hidden" title="<?= lang('success') ?>"><p><?= $this->session->flashdata('success') ?></p></div>
<?php } ?>
<?php if (isset($html['global_js'])) { ?>
<?php foreach ($html['global_js'] as $script) { ?>
<script type="text/javascript" src="<?= $script['src'] ?>"></script>
<?php } ?>
<?php } ?>
<script type="text/javascript">
$(document).ready(function() {
    
<?php if ($this->session->flashdata('error')) { ?>
$("#dialog").dialog({
    width: 550,
    height:150,
    modal:true,
});
<?php } else if ($this->session->flashdata('success')) { ?>
$("#dialog").dialog({
    width: 550,
    height:150,
    modal:true,
});
<?php } ?>
<?php if (isset($data->latitude) AND isset($data->longitude)) { ?>
showPoint(<?= $data->latitude ?>,<?= $data->longitude ?>, 'small_map', 10);
<?php } ?>
<?php if (isset($restaurant->latitude) AND isset($restaurant->longitude)) { ?>
showPoint(<?= $restaurant->latitude ?>,<?= $restaurant->longitude ?>, 'small_map', 10);
<?php } ?>    
})
</script> 
</body>
</html>