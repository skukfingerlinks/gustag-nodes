<?php

echo '<pre>';
print_r($this->postrequest);
echo '</pre>';
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta property="og:title" content="Gustag"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="<?= base_url() ?>" />
        <meta property="og:site_name" content="Gustag" />
        <meta property="og:description" content=""/>
        <link rel="canonical" href="<?= base_url() ?>"/>
        <title>Gustag.com | Discover your food</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">
        <link rel="shortcut icon" href="/assets/img/favicon.png">
        <link rel="apple-touch-icon" href="/assets/img/landing/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/assets/imgs/landing/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/assets/imgs/landing/apple-touch-icon-114x114.png">
        <!--    GLOBAL STYLES    -->
        <link rel="stylesheet" href="/assets/css/landing/skeleton.css">
        <link rel="stylesheet" href="/assets/css/landing/gustag.css">
        <link rel="stylesheet" href="/assets/css/landing/layout.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900">
        <!--    EXTRA STYLES    -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
        <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/additional-methods.min.js"></script>
        <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/localization/messages_it.js"></script>
        <!--    EXTRA STYLES    -->
        <link rel="stylesheet" href="/assets/css/landing/agents.css">
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/assets/js/chosen/chosen.css">
        <link rel="stylesheet" href="/assets/css/landing/chosen.css">

    </head>
    <body>
        <div id="wrapper">
            <!--TOP-->
            <div class="head"><a href="/" title="Gustag | Discover your food"><img src="/assets/img/landing/logo.png" border="0" /></a></div>
            <!--MAIN--><div class="body container">
                <div class="sixteen columns">
                    <div class="block agenti">
                        <div class="block-copy">
                            <h1>Inviaci la tua richiesta di affiliazione come Agente Gustag</h1>
                            <?= form_open("/agenti", array('id' => 'agents_form')) ?>
                            <?= form_fieldset() ?>
                            <span class="element">
                                <?= form_label(lang('first_name_label'), 'first_name', array('class' => 'req')) ?>
                                <?= form_error('first_name', '<span id="first_name_error" class="error">', '</span>'); ?>
                                <span><?= form_input(array('name' => 'first_name', 'id' => 'first_name', 'class' => 'long_input small-corners', 'placeholder' => lang('first_name_placeholder'), 'value' => $this->input->post('first_name'))); ?></span>
                            </span>
                            <span class="element">
                                <?= form_label(lang('last_name_label'), 'last_name', array('class' => 'req')) ?>
                                <?= form_error('last_name', '<span id="last_name_error" class="error">', '</span>'); ?>
                                <span><?= form_input(array('name' => 'last_name', 'id' => 'last_name', 'class' => 'long_input small-corners', 'placeholder' => lang('last_name_placeholder'), 'value' => $this->input->post('last_name'))); ?></span>
                            </span>
                            <span class="element">
                                <?= form_label(lang('user_age_label'), 'age', array('class' => 'req')) ?>
                                <?= form_error('age', '<span id="age_error" class="error">', '</span>'); ?>
                                <span><?= form_input(array('name' => 'age', 'id' => 'age', 'class' => 'long_input small-corners', 'placeholder' => lang('user_age_placeholder'), 'value' => $this->input->post('age'))); ?></span>
                            </span>
                            <span class="element">
                                <?= form_label(lang('user_mail_label'), 'usermail', array('class' => 'req')) ?>
                                <?= form_error('usermail', '<span id="usermail_error" class="error">', '</span>'); ?>
                                <span><?= form_email(array('name' => 'usermail', 'id' => 'usermail', 'class' => 'long_input small-corners', 'placeholder' => lang('user_mail_placeholder'), 'value' => $this->input->post('usermail'))); ?></span>
                            </span>
                            <span class="element">
                                <?= form_label(lang('user_phone_label'), 'phone', array('id' => 'phone_label')) ?>
                                <?= form_error('phone', '<span class="error">', '</span>'); ?>
                                <span><?= form_input(array('name' => 'phone', 'id' => 'phone', 'class' => 'medium_input small-corners', 'placeholder' => lang('user_phone_placeholder'), 'value' => $this->input->post('phone'))); ?></span>
                            </span>
                            <span class="element">
                                <?= form_label(lang('user_locations_label'), 'user_locations', array('class' => 'req')) ?>
                                <?= form_error('user_locations', '<span id="user_locations_error" class="error">', '</span>'); ?>
                                <span><?= form_multiselect('user_locations[]', $locations, $this->input->post('user_locations'), 'class="chosen small-corners" multiple="true"') ?></span>
                            </span>
                            <?= form_input(array('name' => 'submit', 'id' => 'submit', 'type' => 'submit', 'value' => lang('subscribe_submit'))) ?>
                            <?= form_fieldset_close() ?>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div><!--FOOTER-->
            <div class="footer-top">
                <div class="container">
                    <div class="sixteen columns"></div>
                    <div class="five alpha columns">&nbsp;</div>
                    <div class="seven columns"><div class="bottom-logo"><img src="/assets/img/landing/logo_sotto.png"></div></div>
                    <div class="four omega columns likes">
                        <iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.gustag.com&amp;layout=button_count&amp;show_faces=true&amp;width=50&amp;action=like&amp;colorscheme=light&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowTransparency="true"></iframe><a href="#" onclick="
        window.open(
                'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(location.href),
                'facebook-share-dialog',
                'width=626,height=436');
        return false;"><img src="/assets/img/landing/share.png" /></a>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="sixteen columns">
                        <div class="phones"></div>
                    </div>
                    <div class="five alpha columns">&nbsp;</div>
                    <div class="eleven omega columns">
                        <div class="closure-copy">Gustag &egrave; la nuova piattaforma per Android e iOS che ti permette di scegliere attraverso migliaia di piatti e pietanze, nel tuo quartiere, nella tua citt&agrave; o mentre sei in viaggio.<br />
                            Potrai scegliere quelli pi&ugrave; sfiziosi, quelli pi&ugrave; particolari o semplicemente quelli pi&ugrave; di tuo gradimento.<br />
                            Sfida i tuoi amici in una caccia al piatto e colleziona i bonus! Gustag &egrave; l'unica piattaforma che ti concede premi <strong>VERI</strong> nei locali del network. <br />
                            Potrai provarlo a breve, e nel mentre trattieni la tua acquolina :) </div>
                    </div>
                    <div class="sixteen columns credits">
                        <div class="five alpha columns made-by">Made by <img src="/assets/img/landing/logo_fl.png" align="absmiddle" /></div>
                        <div class="eleven omega columns mobile-logos"><img src="/assets/img/landing/ico_apple.png"><img src="/assets/img/landing/ico_android.png"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($this->session->flashdata('error')) { ?>
            <div id="dialog" class="hidden" title="<?= lang('error') ?>"><p><?= $this->session->flashdata('error') ?></p></div>
        <?php } ?>
        <!--    EXTRA STYLES    -->
        <script src="/assets/js/chosen/chosen.jquery.js"></script>
        <script src="/assets/js/chosen/init.js"></script>
        <script src="/assets/js/landing/getprovince.js"></script>
        <script src="/assets/js/landing/validate_agents.js"></script>
        <script>
<?php if ($this->session->flashdata('error')) { ?>
                $("#dialog").dialog({width: 350, height: 150});//
<?php } ?>
        </script>
    </body>
</html>