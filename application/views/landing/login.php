
<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta property="og:title" content="Gustag"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="<?= base_url() ?>" />
        <meta property="og:site_name" content="Gustag" />
        <meta property="og:description" content=""/>
        <link rel="canonical" href="<?= base_url() ?>"/>
        <title>Gustag.com | Discover your food</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">
        <link rel="shortcut icon" href="/assets/img/favicon.png">
        <link rel="apple-touch-icon" href="/assets/img/landing/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/assets/imgs/landing/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/assets/imgs/landing/apple-touch-icon-114x114.png">
        <!--    GLOBAL STYLES    -->
        <link rel="stylesheet" href="/assets/css/landing/skeleton.css">
        <link rel="stylesheet" href="/assets/css/landing/gustag.css">
        <link rel="stylesheet" href="/assets/css/landing/layout.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900">
        <!--    EXTRA STYLES    -->
        <link rel="stylesheet" href="/assets/css/landing/agents.css">

    </head>
    <body>
        <div id="wrapper">
            <!--TOP-->
            <div class="head"><a href="/" title="Gustag | Discover your food"><img src="/assets/img/landing/logo.png" border="0" /></a></div>
            <!--MAIN--><div class="body container">
                <div class="nine columns offset-by-three">
                    <div class="block login">
                        <div class="block-copy">
                            <h1>Login</h1>
                            <form action="/login" method="post" accept-charset="utf-8" id="agents_form">
                                <fieldset>
                                    <?php if($this->session->flashdata('error')){?>
                                    <span class="element">
                                      <label class="req">Errore</label>
                                      <span class="desc"><?=$this->session->flashdata('error');?></span> 
                                    </span>
                                    <?php } ?>
                                    <span class="element">
                                        <label class="req">email</label>
                                        <?= form_error('usermail', '<span id="usermail_error" class="error">', '</span>'); ?>
                                        <span class="desc">inserisci la tua mail</span>
                                        <span><input type="text" name="usermail" id="usermail" placeholder="la tua mail"></span>
                                    </span>
                                    <span class="element">
                                        <label class="req">password</label>
                                        <?= form_error('userpwd', '<span id="userpwd_error" class="error">', '</span>'); ?>
                                        <span class="desc">inserisci la tua password</span>
                                        <span><input type="password" name="userpwd" id="userpwd" placeholder="la tua password"></span>
                                    </span>
                                    <input type="submit" name="submit" id="submit" value="Login">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--FOOTER-->
            <div class="footer-top">
                <div class="container">
                    <div class="sixteen columns"></div>
                    <div class="five alpha columns">&nbsp;</div>
                    <div class="seven columns"><div class="bottom-logo"><img src="/assets/img/landing/logo_sotto.png"></div></div>
                    <div class="four omega columns likes">
                        <iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.gustag.com&amp;layout=button_count&amp;show_faces=true&amp;width=50&amp;action=like&amp;colorscheme=light&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowTransparency="true"></iframe><a href="#" onclick="
        window.open(
                'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(location.href),
                'facebook-share-dialog',
                'width=626,height=436');
        return false;"><img src="/assets/img/landing/share.png" /></a>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="sixteen columns">
                        <div class="phones"></div>
                    </div>
                    <div class="five alpha columns">&nbsp;</div>
                    <div class="eleven omega columns">
                        <div class="closure-copy">Gustag &egrave; la nuova piattaforma per Android e iOS che ti permette di scegliere attraverso migliaia di piatti e pietanze, nel tuo quartiere, nella tua citt&agrave; o mentre sei in viaggio.<br />
                            Potrai scegliere quelli pi&ugrave; sfiziosi, quelli pi&ugrave; particolari o semplicemente quelli pi&ugrave; di tuo gradimento.<br />
                            Sfida i tuoi amici in una caccia al piatto e colleziona i bonus! Gustag &egrave; l'unica piattaforma che ti concede premi <strong>VERI</strong> nei locali del network. <br />
                            Potrai provarlo a breve, e nel mentre trattieni la tua acquolina :) </div>
                    </div>
                    <div class="sixteen columns credits">
                        <div class="five alpha columns made-by">Made by <img src="/assets/img/landing/logo_fl.png" align="absmiddle" /></div>
                        <div class="eleven omega columns mobile-logos"><img src="/assets/img/landing/ico_apple.png"><img src="/assets/img/landing/ico_android.png"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>