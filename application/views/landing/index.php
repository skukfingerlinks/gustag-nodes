<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta property="og:title" content="Gustag"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="http://www.nodes.local" />
        <meta property="og:site_name" content="Gustag" />
        <meta property="og:description" content=""/>
        <link rel="canonical" href="http://www.nodes.local"/>
        <title>Gustag.com | Discover your food</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">
        <link rel="shortcut icon" href="/assets/img/favicon.png">
        <link rel="apple-touch-icon" href="/assets/img/landing/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/assets/imgs/landing/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/assets/imgs/landing/apple-touch-icon-114x114.png">
        <!--    GLOBAL STYLES    -->
        <link rel="stylesheet" href="/assets/css/landing/skeleton.css">
        <link rel="stylesheet" href="/assets/css/landing/gustag.css">
        <link rel="stylesheet" href="/assets/css/landing/layout.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900">
        <!--    EXTRA STYLES    -->
        <script src="http://code.jquery.com/jquery-1.4.2.min.js"></script><script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-1.4.2.min.js"><\/script>')</script>
        <script src="/assets/js/landing/jquery.lightbox_me.js"></script><script>window.jQuery || document.write('<script src="/assets/js/landing/jquery.lightbox_me.js"><\/script>')</script>
        <script src="/assets/js/landing/lightbox-register.js"></script><script>window.jQuery || document.write('<script src="/assets/js/landing/lightbox-register.js"><\/script>')</script>
        <script src="/assets/js/landing/subscribe.js"></script><script>window.jQuery || document.write('<script src="/assets/js/landing/subscribe.js"><\/script>')</script>
        <!--    EXTRA STYLES    -->
        <link rel="stylesheet" href="/assets/css/landing/agents.css">

    </head>
    <body>
        <div id="wrapper">
            <!--TOP-->
            <div class="head"><a href="/" title="Gustag | Discover your food"><img src="/assets/img/landing/logo.png" border="0" /></a></div>
            <!--MAIN--><div class="body container">
                <div class="sixteen columns body-bg">
                    <div class="four alpha columns">&nbsp;</div>
                    <div class="twelve omega columns">
                        <h1 class="opening">Nasce Gustag, il nuovo modo di scoprire e assaporare i piatti intorno a te!</h1>
                        <div class="block">
                            <div class="block-copy">
                                <h1 class="first">Trova centinaia di nuovi piatti e pietanze nei quartieri della tua citt&agrave;</h1>
                                Sapresti dire nella tua citt&agrave; chi fa la migliore pizza? O chi le migliori fettuccine fatte a mano? Sapresti trovare velocemente una cucina etnica o una vegetariana apprezzata da decine di utenti? Grazie a Gustag tutto questo &egrave; possibile in pochissimi tap sul tuo cellulare. Perch&egrave; la nostra applicazione ha come focus principale il piatto, ed &egrave; questa la vera differenza da gustare.
                            </div></div>
                        <div class="block">
                            <div class="block-copy">
                                <h1 class="second">Gioca, vota e condividi con i tuoi amici.</h1>
                                Sedersi a tavola &egrave; solo il primo passo. Sblocca tutte le azioni mediante dei QRCode dedicati: iscriviti e segui gli stream dei singoli piatti o dei tuoi locali preferiti. Condividi, tagga e consiglia ai tuoi amici i piatti migliori che riesci a scovare.In fondo mangiare &egrave; è ancora pi&ugrave; gustoso se fatto con la giusta compagnia.
                            </div>
                        </div>
                        <div class="block">
                            <div class="block-copy">
                                <h1 class="third">Vinci ricompense, aumenta la tua reputazione e fai crescere il network.</h1>
                                Sei pronto a diventare il miglior degustatore della pizza quattro formaggi nel tuo quartiere? Con Gustag puoi far crescere il tuo personaggio, guadagnare ricompense, buoni sconto e molto altro ancora. Potrai inoltre intraprendere una carriera all'interno del social per consigliare e rendere ancora più unica l'esperienza di una cena a lume di candela o di una bella serata in compagnia dei tuoi migliori amici.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sign_up">
                <h3>Vuoi partecipare al nostro programma di pre-registrazione?</h3>
                <span>Lascia qui il tuo indirizzo email, ti invieremo tutti gli aggiornamenti e le notizie della nostra app di prossima uscita.</span>
                <div id="sign_up_form">
                    <form action="/landing/index" method="post" accept-charset="utf-8" id="register_form">
                        <input type="email" name="usermail" placeholder="inserisci un'email valida" id="usermail"/>
                        <span class="checkbox"><input type="checkbox" name="owner">Si, ho un locale e vorrei saperne di pi&ugrave; del programma di affiliazione.</span>
                        <input type="submit" name="submit" id="submit" value="invia" class="iscriviti">
                    </form>
                </div>
                <a href="#" class="close sprited" id="close_x">close</a>
            </div><!--FOOTER-->
            <div class="footer-top">
                <div class="container">
                    <div class="sixteen columns"><div class="subscribe"><a href="#" id="go">Vuoi partecipare?</a></div></div>
                    <div class="five alpha columns">&nbsp;</div>
                    <div class="seven columns"><div class="bottom-logo"><img src="/assets/img/landing/logo_sotto.png"></div></div>
                    <div class="four omega columns likes">
                        <iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.nodes.local&amp;layout=button_count&amp;show_faces=true&amp;width=50&amp;action=like&amp;colorscheme=light&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowTransparency="true"></iframe><a href="#" onclick="
                window.open(
                        'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(location.href),
                        'facebook-share-dialog',
                        'width=626,height=436');
                return false;"><img src="/assets/img/landing/share.png" /></a>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="sixteen columns">
                        <div class="phones"></div>
                    </div>
                    <div class="five alpha columns">&nbsp;</div>
                    <div class="eleven omega columns">
                        <div class="closure-copy">Gustag &egrave; la nuova piattaforma per Android e iOS che ti permette di scegliere attraverso migliaia di piatti e pietanze, nel tuo quartiere, nella tua citt&agrave; o mentre sei in viaggio.<br />
                            Potrai scegliere quelli pi&ugrave; sfiziosi, quelli pi&ugrave; particolari o semplicemente quelli pi&ugrave; di tuo gradimento.<br />
                            Sfida i tuoi amici in una caccia al piatto e colleziona i bonus! Gustag &egrave; l'unica piattaforma che ti concede premi <strong>VERI</strong> nei locali del network. <br />
                            Potrai provarlo a breve, e nel mentre trattieni la tua acquolina :) </div>
                    </div>
                    <div class="sixteen columns credits">
                        <div class="five alpha columns made-by">Made by <img src="/assets/img/landing/logo_fl.png" align="absmiddle" /></div>
                        <div class="eleven omega columns mobile-logos"><img src="/assets/img/landing/ico_apple.png"><img src="/assets/img/landing/ico_android.png"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--    EXTRA STYLES    -->
        <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
        <script>window.jQuery || document.write('<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"><\/script>')</script>
        <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/additional-methods.min.js"></script>
        <script>window.jQuery || document.write('<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/additional-methods.min.js"><\/script>')</script>
        <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/localization/messages_it.js"></script>
        <script>window.jQuery || document.write('<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/localization/messages_it.js"><\/script>')</script>
        <script src="/assets/js/landing/validate_user.js"></script>
        <script>window.jQuery || document.write('<script src="/assets/js/landing/validate_user.js"><\/script>')</script>
    </body>
</html>