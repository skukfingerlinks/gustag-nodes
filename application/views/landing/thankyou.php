<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta property="og:title" content="Gustag"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="<?=base_url()?>" />
        <meta property="og:site_name" content="Gustag" />
        <meta property="og:description" content=""/>
        <link rel="canonical" href="<?=base_url()?>"/>
        <title>Gustag.com | Discover your food</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">
        <link rel="shortcut icon" href="/assets/img/favicon.png">
        <link rel="apple-touch-icon" href="/assets/img/landing/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/assets/imgs/landing/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/assets/imgs/landing/apple-touch-icon-114x114.png">        
        <!--    GLOBAL STYLES    -->
        <link rel="stylesheet" href="/assets/css/landing/skeleton.css">
        <link rel="stylesheet" href="/assets/css/landing/gustag.css">
        <link rel="stylesheet" href="/assets/css/landing/layout.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900">
        <!--    EXTRA STYLES    -->
        <link rel="stylesheet" href="/assets/css/landing/agents.css">

    </head>
    <body>
        <div id="wrapper">
            <!--TOP-->
            <div class="head"><a href="/" title="Gustag | Discover your food"><img src="/assets/img/landing/logo.png" border="0" /></a></div> 
            <!--MAIN--><div class="body container response">
                <div class="sixteen columns">
                    <div class="three alpha columns">&nbsp;</div>
                    <div class="thirteen omega columns">
                        <div class="block message">
                            <h1><span class="thank-you">Grazie <?=$user->username?></span></h1>
                            <div class="block-copy">
                                <p>Grazie <strong><?=$user->username?></strong>!</p>                                
                                <p>Hai effettuato la registrazione a Gustag come:  <?=lang($user->roles[0]->name)?><p>
                                <?php if(isset($user->geopoints)){?>
                                <p>Province di competenza indicate:<p/>
                                <blockquote>
                                    <?php foreach($user->geopoints as $geopoint){ ?>
                                    <?=$geopoint->administrative_area_level_3?> (<?=$geopoint->administrative_area_level_1?>)<br />
                                    <?php } ?>
                                </blockquote>
                                <?php } ?>
                                <p>A breve riceverai una mail all'indirizzo <?=$user->usermail?> con il riepilogo dei tuoi dati e la procedura per il login al sistema.<p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--FOOTER-->
            <div class="footer-top">
                <div class="container">
                    <div class="sixteen columns"></div>
                    <div class="five alpha columns">&nbsp;</div>
                    <div class="seven columns"><div class="bottom-logo"><img src="/assets/img/landing/logo_sotto.png"></div></div>
                    <div class="four omega columns likes">
                        <iframe src="http://www.facebook.com/plugins/like.php?href=<?=base_url()?>&amp;layout=button_count&amp;show_faces=true&amp;width=50&amp;action=like&amp;colorscheme=light&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowTransparency="true"></iframe><a href="#" onclick="
        window.open(
                'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(location.href),
                'facebook-share-dialog',
                'width=626,height=436');
        return false;"><img src="/assets/img/landing/share.png" /></a>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="sixteen columns">
                        <div class="phones"></div>
                    </div>
                    <div class="five alpha columns">&nbsp;</div>
                    <div class="eleven omega columns">
                        <div class="closure-copy">Gustag &egrave; la nuova piattaforma per Android e iOS che ti permette di scegliere attraverso migliaia di piatti e pietanze, nel tuo quartiere, nella tua citt&agrave; o mentre sei in viaggio.<br />
                            Potrai scegliere quelli pi&ugrave; sfiziosi, quelli pi&ugrave; particolari o semplicemente quelli pi&ugrave; di tuo gradimento.<br />
                            Sfida i tuoi amici in una caccia al piatto e colleziona i bonus! Gustag &egrave; l'unica piattaforma che ti concede premi <strong>VERI</strong> nei locali del network. <br />
                            Potrai provarlo a breve, e nel mentre trattieni la tua acquolina :) </div>
                    </div>
                    <div class="sixteen columns credits">
                        <div class="five alpha columns made-by">Made by <img src="/assets/img/landing/logo_fl.png" align="absmiddle" /></div>
                        <div class="eleven omega columns mobile-logos"><img src="/assets/img/landing/ico_apple.png"><img src="/assets/img/landing/ico_android.png"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>