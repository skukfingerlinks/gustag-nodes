<div class="sixteen columns">
    <h3>Login</h3>
    <?= form_open("/login", array('id' => 'login_form')) ?>
    <p>
        <?= form_label(lang('usermail_label'), 'usermail', array('class' => 'req')) ?>
        <?= form_error('usermail', '<span class="error">', '</span>'); ?>
        <span><?= form_email(array('name' => 'usermail', 'id' => 'usermail', 'class' => 'long_input', 'placeholder' => lang('usermail_placeholder'), 'value' => set_value('usermail'))); ?></span>
    </p>
    <p>
        <?= form_label(lang('userpwd_label'), 'userpwd', array('class' => 'req')) ?>
        <?= form_error('userpwd', '<span class="error">', '</span>'); ?>
        <span><?= form_password(array('name' => 'userpwd', 'id' => 'userpwd', 'class' => 'long_input', 'placeholder' => lang('userpwd_placeholder'), 'value' => set_value('plainpwd'))); ?></span>
    </p>
    <p>
        <?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('login'))) ?>
    </p>
    <?= form_close() ?>
</div>
