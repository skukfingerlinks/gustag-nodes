<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$data->id,$data->name)?></li>
        <li><?=anchor('/restaurants/calendar/'.$data->id,lang('calendar'))?></li>
        <li><?=anchor('/dishes/restaurant/'.$data->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$data->id,lang('menus'))?></li>
        <li><?=anchor('/events/restaurant/'.$data->id,lang('events'))?></li>        
    </ul>  
    <div class="clear"></div>        
    <h3><?=lang('calendar').' '.lang('restaurant').' '.$data->name ?></h3> 
</div>
<div class="twelve columns">
    <?php if($usersession->is_admin) { ?>
    <h3><?=lang('restaurants')?></h3>
    <?php }else{ ?>
    <h3><?=lang('yours').' '.lang('restaurants')?></h3>
    <?php } ?>
    <p><?= anchor('/restaurants/create', lang('create'), array('class' => 'button')) ?></p>
    <?php if($usersession->is_admin) echo 'admin'; ?>
    <?php if (isset($html['found'])) { ?>
        <p><?= $total_rows ?></p>
        <?= $html['pagination_links'] ?>
        <table id="paginated" class="tablesorter">
            <caption><?= lang($html['items_type']) ?></caption>
            <thead>
                <tr>
                    <th class="first" title="Clicca per ordinare per Stato">Stato</th>    
                    <?php if($usersession->is_admin) {?>
                    <th title="Clicca per ordinare per Gestore">Gestore</th>            
                    <?php } ?>
                    <th title="Clicca per ordinare per Nome">Nome</th>            
                    <!--<th title="Clicca per ordinare per Indirizzo">Indirizzo</th>-->
                    <th title="Clicca per ordinare per Eventi">Eventi</th>                      
                    <th title="Clicca per ordinare per Menu">Men&ugrave;</th>            
                    <th title="Clicca per ordinare per Rating">Rating</th>            
                    <th>Elimina</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($html['found'] as $item) { ?>
                    <tr>
                        <td><?php echo $active = ($item->active == 1) ? lang('active') : lang('inactive'); ?></td>
                        <?php if($usersession->is_admin) {?>
                        <td><?= anchor('/users/edit/' . $item->users[0]->node_id, $item->users[0]->username) ?></td>
                        <?php } ?>                        
                        <td><?= anchor($html['items_type'] . '/edit/' . $item->id, $item->name) ?></td>
                        <!--<td><?= $item->address ?></td>-->
                        <td class="center">
                            <?php if (isset($item->menus)) { ?>
                                <?= anchor('/menus/restaurant/' . $item->id, sizeof($item->menus)) ?>
                            <?php } else { ?>
                                <?= lang('not_available_data') ?>
                            <?php } ?>
                        </td>  
                        <td class="center">
                            <?php if (isset($item->events)) { ?>
                                <?= anchor('/events/restaurant/' . $item->id, sizeof($item->events)) ?>
                            <?php } else { ?>
                                <?= lang('not_available_data') ?>
                            <?php } ?>
                        </td>                     
                        <td class="center">
                            <?php if (isset($item->rating)) { ?>
                                <?= $item->rating ?>
                            <?php } else { ?>
                                <?= lang('not_available_data') ?>                        
                            <?php } ?>                     
                        </td>  
                        <td><?= anchor('/restaurants/delete/' . $item->id, lang('delete')) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?= $html['pagination_links'] ?>
    <?php } ?>
</div>


