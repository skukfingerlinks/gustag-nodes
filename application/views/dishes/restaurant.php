<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('list').' '.lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$restaurant->id,$restaurant->name)?></li>
        <li><?=anchor('/restaurants/calendar/'.$restaurant->id,lang('calendar'))?></li>
        <li><span><?=lang('dishes')?></li>
        <li><?=anchor('/menus/restaurant/'.$restaurant->id,lang('menus'))?></li>
        <li><?=anchor('/events/restaurant/'.$restaurant->id,lang('events'))?></li>  
        <li><?=anchor('/restaurants/gallery/'.$restaurant->id,lang('image_gallery'))?></li>
    </ul>  
    <div class="clear"></div>   
    <h4><?=lang('restaurant').' '.$restaurant->name?></h4>
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$owner->username?></p>
    <h3><?=lang('list').' '.lang('dishes')?></h3>      
    <p><?= anchor('/dishes/create/' . $restaurant->id, lang('create_new').' '.lang('dish'), array('class' => 'button')) ?></p>    
</div>
<div class="twelve columns">
<?php if ($html['found']) { ?>

        <?= $html['pagination_links'] ?>
        <table id="paginated" class="tablesorter">
            <caption><?= lang($html['items_type']) ?></caption>
            <thead>
                <tr>
                    <th class="first" title="Clicca per ordinare per Stato">Stato</th>
                    <th title="Clicca per ordinare per Nome" <!--colspan="2"-->Mio</th>
                    <th title="Clicca per ordinare per Nome" <!--colspan="2"-->Nome</th>
                    <th title="Clicca per ordinare per Immagini">Immagini</th>
                    
                    <?php if($usersession->is_admin) {?>
                    <th>Elimina</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($html['found'] as $item) { ?>
                    <tr>
                        <td>
                            <?php if($item->active==1){?>
                            <?= anchor($html['items_type'].'/deactivate/' . $item->id, lang('deactivate')) ?>
                            <?php }else{ ?>
                            <?= anchor($html['items_type'].'/activate/' . $item->id, lang('activate')) ?>
                            <?php } ?>
                        </td>
                        <!--<td width="5%" style="background-color: lime;margin:0;padding:10px 0 0 0"><?=img(array('src'=>$item->logo,'class'=>'avatar'))?></td>-->
                        <td>
                            <?php if($item->owner_id==$this->usersession->id){?>
                            <?=lang('yes')?>
                            <?php }else{ ?>
                            <?=lang('no')?>
                            <?php } ?>
                        </td>                          
                        <td><?= anchor($html['items_type'] . '/edit/' . $item->id, $item->name) ?></td>
                        <td><?= anchor($html['items_type'] . '/gallery/' . $item->id, sizeof($item->media)) ?></td>
                        <?php if($usersession->is_admin) {?>
                        <td><?= anchor($html['items_type'].'/delete/' . $item->id, lang('delete')) ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?= $html['pagination_links'] ?>
    
<?php }else{ ?>
<p><?=lang('no').' '.lang('dish').' '.lang('found')?></p>    
<?php } ?>
</div>



