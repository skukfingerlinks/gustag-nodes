<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('list').' '.lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$restaurant->id,$restaurant->name)?></li>
        <li><?=anchor('/restaurants/calendar/'.$restaurant->id,lang('calendar'))?></li>
        <li><?=anchor('/dishes/restaurant/'.$restaurant->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$restaurant->id,lang('menus'))?></li>
        <li><?=anchor('/events/restaurant/'.$restaurant->id,lang('events'))?></li>  
        <li><?=anchor('/restaurants/gallery/'.$restaurant->id,lang('image_gallery'))?></li>
    </ul>  
    <div class="clear"></div>   
    <h4><?=lang('restaurant').' '.$restaurant->name?></h4>
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$owner->username?></p>
    <h3><?=lang('dish').': '.$data->name?></h3>      
    <p><?= anchor('/dishes/create/' . $restaurant->id, lang('create_new').' '.lang('dish'), array('class' => 'button')) ?></p>    
</div>
<?= form_open_multipart("/dishes/edit/".$data->id, array('id' => 'edit_dish')) ?>
<div class="six columns">
    <!--type-->
    <?php if (isset($tags)) {foreach ($tags as $tag) { ?>
    <?php if ($tag['name'] == 'dish_type') { ?>
    <p>
        <?= form_label($tag['description'], $tag['name'], array('class' => 'req')) ?>
        <!--<span><?= lang($tag['name'] . '_help') ?></span>-->
        <?= form_error($tag['name'], '<span class="error">', '</span>'); ?>
        <?=form_dropdown($tag['name'], $dish_type, $type_of_dish);?>
        </p>
    <?php } ?>
    <?php }} ?>
    <!--name-->
    <p>
        <?= form_label(lang('dish_name_label'), 'name', array('class' => 'req')) ?>
        <span><?= lang('dish_name_help') ?></span>
        <?= form_error('usermail', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'name', 'id' => 'name', 'class' => 'long_input small-corners', 'placeholder' => lang('dish_name_placeholder'), 'value' => $data->name)); ?></span>
    </p>
    <!--logo-->    
    <p>
        <?= form_label(lang('dish_logo_label'), 'userfile') ?>
        <span><?=img(array('src'=>$data->logo,'class'=>'logo'))?></span>
        <?= form_upload(array('name' => 'userfile', 'id' => 'userfile', 'class' => 'long_input', 'placeholder' => lang('dish_logo_help'), 'value' => set_value('logo'))); ?>
        <?= form_error('userfile', '<span class="error" id="userfile_error">', '</span>'); ?>
    </p>     
    <!--desc-->
    <p>
        <?= form_label(lang('dish_desc_label'), 'desc', array('class' => 'req')) ?>
        <span><?= lang('dish_desc_help') ?></span>
        <?= form_error('desc', '<span class="error">', '</span>'); ?>
        <span><?= form_textarea(array('name' => 'desc', 'id' => 'desc', 'rows' => 4, 'class' => 'long_input small-corners', 'placeholder' => lang('dish_desc_placeholder'), 'value' => $data->desc)); ?></span>
        </p>
</div>
<div class="six columns">
    <!--tags-->
    <?php if (isset($tags)) { foreach ($tags as $tag) { ?>
    <?php if ($tag['name'] !== 'dish_type') { ?>
    <p>
        <?= form_label($tag['description'], $tag['name']) ?>
        <span><?= lang($tag['name'] . '_help') ?></span>
        <?= form_error($tag['name'], '<span class="error">', '</span>'); ?>
        <span><?= form_multiselect($tag['name'] . '[]', $tag['options'], $tag['selected'], 'class="chosen small-corners" multiple="true"') ?></span>
    </p>
    <?php } ?>
    <?php }} ?>
    <!--private/public-->
    <p>
    <?= form_label(lang('dish_owner_label'), 'name') ?>
    <span><?= lang('dish_owner_help') ?></span>
    <?= form_error('owner', '<span class="error">', '</span>'); ?>
    <span><?= form_checkbox('owner', 'private', TRUE); ?></span>
    </p>
</div>

<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('update'))) ?></p>
</div>
<?= form_close() ?>


