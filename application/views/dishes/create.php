<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$restaurant->id,$restaurant->name)?></li>
        <li><?=anchor('/restaurants/calendar/'.$restaurant->id,lang('calendar'))?></li>
        <li><?=anchor('/dishes/restaurant/'.$restaurant->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$restaurant->id,lang('menus'))?></li>
        <li><?=anchor('/events/restaurant/'.$restaurant->id,lang('events'))?></li>  
        <li><?=anchor('/restaurants/gallery/'.$restaurant->id,lang('image_gallery'))?></li>
    </ul>  
    <div class="clear"></div>        
    <h3><?=$restaurant->name.': '.lang('create').' '.lang('dish') ?></h3> 
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$owner->username?></p>
</div>
<?= form_open_multipart("/dishes/create/" . $restaurant->id, array('id' => 'edit_dish')) ?>
<div class="six columns">
    <!--type-->
    <?php if (isset($tags)) {foreach ($tags as $tag) { ?>
    <?php if ($tag['name'] == 'dish_type') { ?>
    <p>
        <?= form_label($tag['description'], $tag['name'], array('class' => 'req')) ?>
        <!--<span><?= lang($tag['name'] . '_help') ?></span>-->
        <?= form_error($tag['name'], '<span class="error">', '</span>'); ?>
        <?=form_dropdown($tag['name'], $types_dish, $tag['selected']);?>
        </p>
    <?php } ?>
    <?php }} ?>
    <!--name-->
    <p>
        <?= form_label(lang('dish_name_label'), 'name', array('class' => 'req')) ?>
        <span><?= lang('dish_name_help') ?></span>
        <?= form_error('usermail', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'name', 'id' => 'name', 'class' => 'long_input small-corners', 'placeholder' => lang('dish_name_placeholder'), 'value' => set_value('address'))); ?></span>
    </p>
    <!--logo-->    
    <p>
        <?= form_label(lang('dish_logo_label'), 'userfile') ?>
        <span><?=img(array('src'=>$this->config->item('dish_default_logo'),'class'=>'logo'))?></span>
        <?= form_upload(array('name' => 'userfile', 'id' => 'userfile', 'class' => 'long_input', 'placeholder' => lang('dish_logo_help'), 'value' => set_value('logo'))); ?>
        <?= form_error('userfile', '<span class="error" id="userfile_error">', '</span>'); ?>
    </p>      
    <!--desc-->
    <p>
        <?= form_label(lang('dish_desc_label'), 'desc', array('class' => 'req')) ?>
        <span><?= lang('dish_desc_help') ?></span>
<?= form_error('desc', '<span class="error">', '</span>'); ?>
        <span><?= form_textarea(array('name' => 'desc', 'id' => 'desc', 'rows' => 4, 'class' => 'long_input small-corners', 'placeholder' => lang('dish_desc_placeholder'), 'value' => set_value('desc'))); ?></span>
    </p>
</div>
<div class="six columns">
    <!--tags-->
        <?php if (isset($tags)) {
            foreach ($tags as $tag) { ?>
                <?php if ($tag['name'] !== 'dish_type') { ?>
                <p>
            <?= form_label($tag['description'], $tag['name']) ?>
                    <span><?= lang($tag['name'] . '_help') ?></span>
                <?= form_error($tag['name'], '<span class="error">', '</span>'); ?>
                    <span><?= form_multiselect($tag['name'] . '[]', $tag['options'], $tag['selected'], 'class="chosen small-corners" multiple="true"') ?></span>
                </p>
                <?php } ?>
            <?php }
        } ?>
    <!--private/public-->
    <p>
    <?= form_label(lang('dish_owner_label'), 'name') ?>
    <span><?= lang('dish_owner_help') ?></span>
    <?= form_error('owner', '<span class="error">', '</span>'); ?>
    <span><?= form_checkbox('owner', 'private', TRUE); ?></span>
    </p>
</div>

<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('create'))) ?></p>
</div>
<?= form_close() ?>


