<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('list').' '.lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$restaurant->id,$restaurant->name)?></li>
        <li><?=anchor('/restaurants/calendar/'.$restaurant->id,lang('calendar'))?></li>
        <li><?=anchor('/dishes/restaurant/'.$restaurant->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$restaurant->id,lang('menus'))?></li>        
        <li><span><?=lang('events')?></span></li>    
        <li><?=anchor('/restaurants/gallery/'.$restaurant->id,lang('image_gallery'))?></li>
    </ul>  
    <div class="clear"></div>     
    <div class="clear"></div>   
    <h4><?=lang('restaurant').' '.$restaurant->name?></h4>
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$owner->username?></p>
    <h3><?=lang('list').' '.lang('events')?></h3>      
    <p><?= anchor('/events/create/' . $restaurant->id, lang('create_new').' '.lang('event'), array('class' => 'button')) ?></p>    

</div>
<div class="twelve columns">
<?php if ($html['found']) { ?>    
        <?= $html['pagination_links'] ?>
        <table id="paginated" class="tablesorter">
            <caption><?= lang($html['items_type']) ?></caption>
            <thead>
                <tr>
                    <th class="first" title="Clicca per ordinare per Stato">Stato</th>
                    <th title="Clicca per ordinare per Nome">Nome</th>
                    <th title="Clicca per ordinare per Immagini">Immagini</th>
                    <th title="Clicca per ordinare per Menu">Menu</th>
                    <?php if($usersession->is_admin) {?>
                    <th>Elimina</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($html['found'] as $item) { ?>
                    <tr>
                        <td>
                            <?=$item->active?>
                            <?php if($item->active==1){?>
                            <?= anchor('/nodes/deactivate/' . $item->id, lang('deactivate'),'title="'.lang('activate').'"') ?>
                            <?php }else{ ?>
                            <?= anchor('/nodes/activate/' . $item->id, lang('activate'),'title="'.lang('deactivate').'"') ?>
                            <?php } ?>
                        </td>
                        <td><?= anchor($html['items_type'] . '/edit/' . $item->id, $item->name) ?></td>
                        <td><?= anchor($html['items_type'] . '/gallery/' . $item->id, sizeof($item->media)) ?></td>
                        <td><?= anchor($html['items_type'] . '/menus/' . $item->id, sizeof($item->menus)) ?></td>
                        <?php if($usersession->is_admin) {?>
                        <td><?= anchor($html['items_type'].'/delete/' . $item->id, lang('delete')) ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?= $html['pagination_links'] ?>
<?php }else{ ?>
    <p><?=lang('no').' '.lang('event').' '.lang('found')?></p>
<?php } ?>
</div>


