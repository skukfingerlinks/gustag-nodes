<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('list').' '.lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$restaurant->id,$restaurant->name)?></li>
        <li><?=anchor('/restaurants/calendar/'.$restaurant->id,lang('calendar'))?></li>
        <li><?=anchor('/dishes/restaurant/'.$restaurant->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$restaurant->id,lang('menus'))?></li>        
        <li><?=anchor('/events/restaurant/'.$restaurant->id,lang('events'))?></li>   
        <li><?=anchor('/restaurants/gallery/'.$restaurant->id,lang('image_gallery'))?></li>
    </ul>  
    <div class="clear"></div>   
    <h4><?=lang('restaurant').' '.$restaurant->name?></h4>
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$owner->username?></p>
    <h3><?=lang('calendar').': '.$data->name?></h3>    
    <ul class="dropdown">
        <li><?=anchor('/events/edit/'.$data->id,$data->name)?></li>        
        <li><?=anchor('/events/calendar/'.$data->id,lang('calendar'))?></li>
        <li><?=anchor('/events/gallery/'.$data->id,lang('image_gallery'))?></li>
    </ul>    
</div>
<div class="twelve columns">
    <p><?= lang('calendar_help') ?></p>
    <?= form_open($_SERVER['REQUEST_URI'], array('id' => 'calendar_form')) ?>
    <p class="hidden"><?= form_input(array('name' => 'nid', 'id' => 'nid', 'value' => $data->id)); ?></p>
    <?= $calendar ?>
    <p><?= form_textarea(array('name' => 'dates', 'id' => 'dates', 'class' => 'hidden')) ?></p>
    <p class="center">
        <!--<?= form_button(array('name' => 'submit', 'id' => 'submit', 'type' => 'submit', 'content' => lang('reset_calendar'))) ?>-->
        <?= anchor('#', lang('reset_calendar'), array('id' => 'reset', 'class' => 'button')) ?>
    </p>
    <?= form_close() ?>
</div>
</div><!-- container -->
<div id="eventcalendar_dialog" title="Data Rimossa">
    <p id="eventcalendar_dialog_content" class="center"></p>
</div>
