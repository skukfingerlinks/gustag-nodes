<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('list').' '.lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$restaurant->id,$restaurant->name)?></li>
        <li><?=anchor('/restaurants/calendar/'.$restaurant->id,lang('calendar'))?></li>
        <li><?=anchor('/dishes/restaurant/'.$restaurant->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$restaurant->id,lang('menus'))?></li>        
        <li><?=anchor('/events/restaurant/'.$restaurant->id,lang('events'))?></li>        
        <li><?=anchor('/restaurants/gallery/'.$restaurant->id,lang('image_gallery'))?></li>
    </ul>  
    <div class="clear"></div>   
    <h4><?=lang('restaurant').' '.$restaurant->name?></h4>
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$owner->username?></p>
    <h3><?=lang('event').': '.$data->name?></h3>    
    <ul class="dropdown">
        <li><?=anchor('/events/calendar/'.$data->id,lang('calendar'))?></li>
        <li><?=anchor('/events/gallery/'.$data->id,lang('image_gallery'))?></li>
    </ul>    
</div>
<?= form_open_multipart("/events/edit/" . $data->id, array('id' => 'edit_event')) ?>
<div class="six columns">        
    <!--name-->
    <p>
        <?= form_label(lang('event_name_label'), 'name', array('class' => 'req')) ?>
        <?= form_error('usermail', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'name', 'id' => 'name', 'class' => 'long_input small-corners', 'placeholder' => lang('event_name_placeholder'), 'value' => $data->name)); ?></span>
    </p>
    <p>
        <?= form_label(lang('event_logo_label'), 'userfile') ?>
        <span><?=img(array('src'=>$data->logo,'class'=>'logo'))?></span>
        <?= form_upload(array('name' => 'userfile', 'id' => 'userfile', 'class' => 'long_input', 'placeholder' => lang('event_logo_help'), 'value' => set_value('logo'))); ?>
        <?= form_error('userfile', '<span class="error" id="userfile_error">', '</span>'); ?>
    </p>      
    <!--email-->
    <p>
        <?= form_label(lang('event_desc_label'), 'desc', array('class' => 'req help','title'=>lang('event_desc_help'))) ?>        
        <?= form_error('desc', '<span class="error">', '</span>'); ?>
        <span><?= form_textarea(array('name' => 'desc', 'id' => 'desc', 'rows' => 4, 'class' => 'long_input small-corners', 'placeholder' => lang('event_desc_placeholder'), 'value' => $data->desc)); ?></span>
    </p>
    <!--email-->
    <p>
        <?= form_label(lang('event_mail_label'), 'email') ?>
        <?= form_error('email', '<span class="error">', '</span>'); ?>
        <span><?= form_email(array('name' => 'email', 'id' => 'email', 'class' => 'long_input small-corners', 'placeholder' => lang('event_mail_placeholder'), 'value' => $data->email)); ?></span>
    </p>
    <!--phone-->
    <p>
        <?= form_label(lang('event_phone_label'), 'phone') ?>
        <?= form_error('phone', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'phone', 'id' => 'phone', 'class' => 'medium_input small-corners', 'placeholder' => lang('event_phone_placeholder'), 'value' => $data->phone)); ?></span>
    </p>
    <!--website-->
    <p>
        <?= form_label(lang('event_website_label'), 'website') ?>
        <?= form_error('website', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'website', 'id' => 'website', 'class' => 'long_input small-corners', 'placeholder' => lang('event_website_placeholder'), 'value' => $data->website)); ?></span>
    </p>           
</div>
<div class="six columns">
    <p>
        <?= form_label(lang('event_days_label'), 'event_days') ?>
        <span class="desc"><?=lang('event_days_help')?></span>
        <?= form_error('event_days', '<span class="error">', '</span>'); ?>
        <span><?= form_multiselect('event_days[]', $week_days, $event_days, 'class="chosen small-corners" multiple="true"') ?></span>
    </p>    
    <!--address-->
    <p>
        <?= form_label(lang('event_address_label'), 'name', array('class' => 'req')) ?>
        <span><?= form_input(array('name' => 'address', 'id' => 'geocomplete', 'class' => 'long_input small-corners', 'placeholder' => lang('event_address_placeholder'), 'value' => $data->address)); ?></span>
        <span id="small_map" class="medium-corners"></span>
        <span><?= form_textarea(array('name' => 'addressinfo', 'id' => 'jsonaddress', 'class' => 'hidden')) ?></span>

    </p>
    <!--seats-->
    <!--
    <p>
        <?= form_label(lang('event_seats_label'), 'seats') ?>
        <?= form_error('seats', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'seats', 'id' => 'seats', 'class' => 'small_input small-corners', /* 'placeholder' => lang('event_seats_placeholder'), */ 'value' => $data->seats)); ?></span>
    </p>    
    -->
    <p>
        <?= form_label(lang('event_menus_label'), 'menus', array('class' => 'req')) ?>
        <?= form_error('menus', '<span class="error">', '</span>'); ?>
        <span><?= form_multiselect('menus[]', $restaurant_menus, $event_menus, 'class="chosen small-corners" multiple="true"') ?></span>
    </p>         
    <!--tags-->
    <?php if (isset($tags)) {foreach ($tags as $tag) { ?>
    <p>
        <?= form_label($tag['description'], $tag['name']) ?>
        <?= form_error($tag['name'], '<span class="error">', '</span>'); ?>
        <span><?= form_multiselect($tag['name'] . '[]', $tag['options'], $tag['selected'], 'class="chosen small-corners" multiple="true"') ?></span>
    </p>
    <?php }} ?>
</div>
<div class="twelve columns right center">
    <!--submit-->
    <p><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('update'))) ?></p>
</div>
<?= form_close() ?>
