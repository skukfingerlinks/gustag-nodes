<div class="twelve columns">
    <h3><?=lang('update_profile')?></h3>
</div>
<?= form_open_multipart("/users/profile", array('id' => 'edit_user')) ?>
<div class="six columns">   
    <?php if(isset($locations)){?>
    <p>
        <?= form_label(lang('user_locations_label'), 'user_locations', array('class' => 'req')) ?>
        <span><?= lang('user_locations_help') ?></span>
        <?= form_error('user_locations', '<span id="user_locations_error" class="error">', '</span>'); ?>
        <span><?= form_multiselect('user_locations[]', $locations, $user_geopoints, 'class="chosen small-corners" multiple="true"') ?></span>
    </p>
    <?php } ?>    
    <!--mail-->
    <p>
        <?= form_label(lang('user_mail_label'), 'usermail', array('class' => 'req')) ?>
        <?= form_error('usermail', '<span id="usermail_error" class="error">', '</span>'); ?>
        <span><?= form_email(array('name' => 'usermail', /*'disabled'=>'true',*/'id' => 'usermail', 'class' => 'long_input small-corners ', 'placeholder' => lang('user_mail_placeholder'), 'value' => $data->usermail)); ?></span>
    </p>    
    <!--name-->
    <p>
        <?= form_label(lang('user_name_label'), 'username', array('class' => 'req')) ?>
        <?= form_error('username', '<span id="username_error" class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'username',/*'disabled'=>'true', */'id' => 'username', 'class' => 'long_input small-corners ', 'placeholder' => lang('user_name_placeholder'), 'value' => $data->username)); ?></span>
    </p>
    
    <!--pwd-->
    <p>
        <?= form_label(lang('user_pwd_label'), 'plainpwd', array('class' => 'req')) ?>
        <?= form_error('plainpwd', '<span id="plainpwd_error" class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'plainpwd', 'id' => 'plainpwd', 'class' => 'long_input small-corners', 'placeholder' => lang('user_pwd_placeholder'), 'value' => $plainpwd)); ?></span>
    </p> 
    <!--status-->
    <p>
        <?= form_label(lang('user_status_label'), 'status', array('class' => 'req')) ?>
        <?= form_error('desc', '<span id="status_error" class="error">', '</span>'); ?>
        <span><?= form_textarea(array('name' => 'status','cols'=>80, 'rows'=>4, 'id' => 'status', 'class' => 'long_input small-corners', 'placeholder' => lang('user_status_placeholder'), 'value' => $data->status)); ?></span>
    </p>         
</div>
<div class="six columns">
    <!--avatar-->    
    <p>
        <?= form_label(lang('user_avatar_label'), 'userfile') ?>
        <span><?=img(array('src'=>$data->avatar,'class'=>'logo'))?></span>
        <?= form_upload(array('name' => 'userfile', 'id' => 'userfile', 'class' => 'long_input', 'placeholder' => lang('user_avatar_help'), 'value' => set_value('userfile'))); ?>
        <?= form_error('userfile', '<span class="error" id="userfile_error">', '</span>'); ?>
    </p>     
    <!--firstname-->
    <p>
        <?= form_label(lang('user_firstname_label'), 'first_name', array('class' => 'req')) ?>
        <?= form_error('first_name', '<span id="first_name_error" class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'first_name', 'id' => 'first_name', 'class' => 'long_input small-corners', 'placeholder' => lang('user_firstname_placeholder'), 'value' => $data->first_name)); ?></span>
    </p>   
    <!--lastname-->
    <p>
        <?= form_label(lang('user_lastname_label'), 'last_name', array('class' => 'req')) ?>
        <?= form_error('last_name', '<span id="last_name_error" class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'last_name', 'id' => 'last_name', 'class' => 'long_input small-corners', 'placeholder' => lang('user_lastname_placeholder'), 'value' => $data->last_name)); ?></span>
    </p>     
    <!--phone-->
    <p>
        <?= form_label(lang('user_phone_label'), 'phone', array('class' => 'req')) ?>
        <?= form_error('phone', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'phone', 'id' => 'phone', 'class' => 'medium_input small-corners', 'placeholder' => lang('user_phone_placeholder'), 'value' => $data->phone)); ?></span>
    </p>

</div>
<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('update'))) ?></p>
</div>
<?= form_close() ?>


