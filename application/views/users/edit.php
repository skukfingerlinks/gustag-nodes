<div class="twelve columns">
    <h4><?= lang('update_profile') ?></h4>
    <h3><?= $data->username ?></h3>
</div>
<div class="twelve columns">
    <p>
        <?= form_label(lang('facebook'), '') ?>    
        <?php if($data->fbregistration==1){?>
        <span><?=lang('fbregistration')?>: <?=form_checkbox(array('name'=> lang('fbregistration'),'value'=> lang('fbregistration'),'checked'=> TRUE,'disabled'=>TRUE));?></span>
        <?php }else{ ?>
        <span><?=lang('fbregistration')?>: <?=form_checkbox(array('name'=> lang('fbregistration'),'value'=> lang('fbregistration'),'checked'=> FALSE,'disabled'=>TRUE));?></span>
<?php } ?>
    </p>
</div>    
<?= form_open_multipart("/users/edit/" . $data->id, array('id' => 'edit_user')) ?>
<div class="twelve columns">
    <p>
        <?= form_label(lang('user_roles_label'), 'user_roles') ?>
        <span class="help"><?= lang('user_roles_help') ?></span>
        <?= form_error('user_roles', '<span id="user_roles_error" class="error">', '</span>'); ?>
        <span><?= form_multiselect('user_roles[]', $user_roles, $roles_user, 'class="chosen long_input small-corners" multiple="true"') ?></span>
    </p>
</div>
<?php if (isset($is_owner)) { ?>
    <div class="twelve columns">
        <p>
            <?= form_label(lang('user_agents_label'), 'user_agents', array('class' => 'req')) ?>
            <span><?= lang('user_agents_help') ?></span>
            <?= form_error('user_agents', '<span id="user_agents_error" class="error">', '</span>'); ?>
            <span><?= form_multiselect('user_agents[]', $all_agents, $user_agents, 'class="long_input chosen small-corners"') ?></span>
        </p>
    </div>
    <div class="twelve columns">
        <p>
            <?= form_label(lang('user_restaurants_label'), 'user_restaurants', array('class' => 'req')) ?>
            <span><?= lang('user_restaurants_help') ?></span>
            <?= form_error('user_restaurants', '<span id="user_restaurants_error" class="error">', '</span>'); ?>
            <span><?= form_multiselect('user_restaurants[]', $all_restaurants, $user_restaurants, 'class="long_input chosen small-corners"') ?></span>
        </p>
    </div>
<?php } ?>
<?php if (isset($is_agent)) { ?>
    <div class="six columns">
        <!--province-->
        <p>
            <?= form_label(lang('user_locations_label'), 'user_locations', array('class' => 'req')) ?>
            <span><?= lang('user_locations_help') ?></span>
            <?= form_error('user_locations', '<span id="user_locations_error" class="error">', '</span>'); ?>
            <span><?= form_multiselect('user_locations[]', $locations, $user_geopoints, 'class="long_input chosen small-corners" multiple="true"') ?></span>
        </p>
    </div>
    <div class="six columns">
        <!--ristoratori-->
        <p>
            <?= form_label(lang('user_owners_label'), 'user_owners', array('class' => 'req')) ?>
            <span><?= lang('user_owners_help') ?></span>
            <?= form_error('user_owners', '<span id="user_owners_error" class="error">', '</span>'); ?>
            <span><?= form_multiselect('user_owners[]', $all_owners, $user_owners, 'class="long_input chosen small-corners" multiple="true"') ?></span>
        </p>
    </div>
<?php } ?>
<div class="six columns">
    <!--mail-->
    <p>
        <?= form_label(lang('user_mail_label'), 'usermail', array('class' => 'req')) ?>
        <span class="desc"><?= lang('user_locations_help') ?></span>
        <?= form_error('usermail', '<span id="usermail_error" class="error">', '</span>'); ?>
        <span><?= form_email(array('name' => 'usermail', /* 'disabled'=>'true', */ 'id' => 'usermail', 'class' => 'long_input small-corners ', 'placeholder' => lang('user_mail_placeholder'), 'value' => $data->usermail)); ?></span>
    </p>
    <!--name-->
    <p>
        <?= form_label(lang('user_name_label'), 'username', array('class' => 'req')) ?>
        <span class="desc"><?= lang('user_locations_help') ?></span>
        <?= form_error('username', '<span id="username_error" class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'username', /* 'disabled'=>'true', */ 'id' => 'username', 'class' => 'long_input small-corners ', 'placeholder' => lang('user_name_placeholder'), 'value' => $data->username)); ?></span>
    </p>

    <!--pwd-->
    <p>
        <?= form_label(lang('user_pwd_label'), 'plainpwd', array('class' => 'req')) ?>
        <span class="desc"><?= lang('user_locations_help') ?></span>
        <?= form_error('plainpwd', '<span id="plainpwd_error" class="error">', '</span>'); ?>
        <span ><?= form_input(array('name' => 'plainpwd', 'id' => 'plainpwd', 'class' => 'long_input small-corners', 'placeholder' => lang('user_pwd_placeholder'), 'value' => $plainpwd)); ?></span>
    </p>
    <!--firstname-->
    <p>
        <?= form_label(lang('user_firstname_label'), 'first_name', array('class' => 'req')) ?>
        <span class="desc"><?= lang('user_locations_help') ?></span>
        <?= form_error('first_name', '<span id="first_name_error" class="error">', '</span>'); ?>
        <span ><?= form_input(array('name' => 'first_name', 'id' => 'first_name', 'class' => 'long_input small-corners', 'placeholder' => lang('user_firstname_placeholder'), 'value' => $data->first_name)); ?></span>
    </p>
    <!--lastname-->
    <p>
        <?= form_label(lang('user_lastname_label'), 'last_name', array('class' => 'req')) ?>
        <span class="desc"><?= lang('user_locations_help') ?></span>
        <?= form_error('last_name', '<span id="last_name_error" class="error">', '</span>'); ?>
        <span ><?= form_input(array('name' => 'last_name', 'id' => 'last_name', 'class' => 'long_input small-corners', 'placeholder' => lang('user_lastname_placeholder'), 'value' => $data->last_name)); ?></span>
    </p>
    <!--phone-->
    <p>
        <?= form_label(lang('user_phone_label'), 'phone', array('class' => 'req')) ?>
        <span class="desc"><?= lang('user_phone_help') ?></span>
        <?= form_error('phone', '<span class="error">', '</span>'); ?>
        <span ><?= form_input(array('name' => 'phone', 'id' => 'phone', 'class' => 'medium_input small-corners', 'placeholder' => lang('user_phone_placeholder'), 'value' => $data->phone)); ?></span>
    </p>
</div>
<div class="six columns">
    <!--avatar-->
    <p>
        <?= form_label(lang('user_avatar_label'), 'userfile') ?>
        <span class="desc"><?= lang('user_locations_help') ?></span>
        <span class="center"><?= img(array('src' => $data->avatar, 'class' => 'logo')) ?></span>
        <?= form_upload(array('name' => 'userfile', 'id' => 'userfile', 'class' => 'long_input', 'placeholder' => lang('user_avatar_help'), 'value' => set_value('userfile'))); ?>
        <?= form_error('userfile', '<span class="error" id="userfile_error">', '</span>'); ?>
    </p>
    <!--status-->
    <p>
        <?= form_label(lang('user_status_label'), 'status', array('class' => 'req')) ?>
        <?= form_error('desc', '<span id="status_error" class="error">', '</span>'); ?>
        <span><?= form_textarea(array('name' => 'status', 'cols' => 80, 'rows' => 4, 'id' => 'status', 'class' => 'long_input small-corners', 'placeholder' => lang('user_status_placeholder'), 'value' => $data->status)); ?></span>
    </p>
</div>
<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('update'))) ?></p>
</div>
<?=form_close()?>