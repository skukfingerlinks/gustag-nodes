<div class="twelve columns">
        <!--<p><?=anchor('/users/'.$html['items_type'],lang('back_to').' '.lang('list').' '.lang($html['items_type']))?></p>-->
        <h3><?=lang('list').' '.lang($html['items_type'])?></h3>
        <p>The easiest way to really get started with Skeleton is to check out the full docs and info at <a href="http://www.getskeleton.com">www.getskeleton.com.</a>.</p>
        <p><?= anchor('/users/create', lang('create').' '.lang($html['role']), array('class' => 'button')) ?></p>
        <p><?=lang($html['items_type']).' '.lang('found').': <strong>'.$html['total_items'].'</strong>'?></p>
        <?=$html['pagination_links']?>        
        <table id="paginated" class="tablesorter">
            <caption><?=lang($html['items_type'])?></caption>
            <thead>
                <tr>
                    <th class="first" title="Clicca per ordinare per Stato">Stato</th>                
                    <th title="Clicca per ordinare per Nome" colspan="2">Nome</th>                
                    <!--<th title="Clicca per ordinare per Mail">Mail</th>-->
                    <?php if($html['items_type']=='agents'){?>
                    <th title="Clicca per ordinare per numero Province">Province</th>                
                    <th title="Clicca per ordinare per numero Ristoratori">Ristoratori</th>                
                    <th title="Clicca per ordinare per numero Telefonico">Telefono</th>                                    
                    <?php }else if($html['items_type']=='owners'){?>
                    <th title="Clicca per ordinare per numero Agenti">Agenti</th>                
                    <th title="Clicca per ordinare per numero Ristoranti">Ristoranti</th>                                    
                    <?php } ?>
                    <th><?=lang('delete')?></th>        
                </tr>
            </thead>
            <tbody>
                <?php foreach($html['found'] as $item){?>
                <?php
                    
                    $class='';
                    if($html['items_type']=='agents'){
                        if($item->active==0 OR sizeof($item->geopoints)<1 OR sizeof($item->owners)<1 OR !isset($item->phone)) $class='inactive';
                    }else if($html['items_type']=='owners'){
                        if($item->active==0 OR sizeof($item->agents)<1 OR sizeof($item->restaurants)<1) $class='inactive';
                    }
                
                ?>
                <tr class="<?=$class?>">
                    <td>
                        <?php if($item->active==1){?>
                        <?=anchor('/nodes/deactivate/'.$item->id,lang('active'),array('class'=>'deactivate tooltip','title'=>lang('set_inactive')))?>
                        <?php }else{ ?>
                        <?=anchor('/nodes/activate/'.$item->id,lang('inactive'),array('class'=>'activate tooltip','title'=>lang('set_active')))?>
                        <?php } ?>
                    </td>
                    <td class="center number"><?=anchor('/users/edit/'.$item->id,img(array('src'=>$item->avatar,'title'=>$item->username,'alt'=>$item->username,'class'=>'avatar')))?></td>
                    <td><?=anchor('/users/edit/'.$item->id,$item->username)?></td>
                    <!--<td><?=$item->usermail?></td>-->
                    <?php if($html['items_type']=='agents'){?>
                        <td class="center number"><?=sizeof($item->geopoints)?></td>                
                        <td class="center number"><?=sizeof($item->owners)?></td>  
                        <td class="center number"><?=$item->phone?></td>                           
                    <?php }else if($html['items_type']=='owners'){?>
                        <td class="center number"><?=sizeof($item->agents)?></td>                
                        <td class="center number"><?=sizeof($item->restaurants)?></td>                                       
                    <?php } ?>                    
                    <td class="center"><?=anchor('/nodes/delete/'.$item->id,lang('delete'),array('class'=>'delete tooltip','title'=>lang('delete')))?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?=$html['pagination_links']?>        
    </div>
</div><!-- container -->

