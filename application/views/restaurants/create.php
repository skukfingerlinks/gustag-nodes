<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('list').' '.lang('restaurants'))?></li>
    </ul>  
    <div class="clear"></div>        
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$usersession->username?></p>
    <h3><?=lang('create').' '.lang('restaurant')?></h3>     
</div>
<?= form_open_multipart("/restaurants/create", array('id' => 'edit_restaurant')) ?>
<div class="six columns">          
    <!--name-->
    <p>
        <?= form_label(lang('restaurant_name_label'), 'name', array('class' => 'req')) ?>
        <?= form_error('name', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'name', 'id' => 'name', 'class' => 'long_input small-corners', 'placeholder' => lang('restaurant_name_placeholder'), 'value' => set_value('name'))); ?></span>
    </p>
    <!--logo-->    
    <p>
        <?= form_label(lang('restaurant_logo_label'), 'userfile') ?>
        <span><?=img(array('src'=>$this->config->item('restaurant_default_logo'),'class'=>'logo'))?></span>
        <?= form_upload(array('name' => 'userfile', 'id' => 'userfile', 'class' => 'long_input', 'placeholder' => lang('restaurant_logo_help'), 'value' => set_value('logo'))); ?>
        <?= form_error('userfile', '<span class="error" id="userfile_error">', '</span>'); ?>
    </p>      
    <!--desc-->
    <p>
        <?= form_label(lang('restaurant_desc_label'), 'desc', array('class' => 'req')) ?>
        <?= form_error('desc', '<span class="error">', '</span>'); ?>
        <span><?= form_textarea(array('name' => 'desc','cols'=>80, 'rows'=>4, 'id' => 'desc', 'class' => 'long_input small-corners', 'placeholder' => lang('restaurant_desc_placeholder'), 'value' => set_value('desc'))); ?></span>
    </p>    
    <!--email-->
    <p>
        <?= form_label(lang('restaurant_mail_label'), 'email') ?>
        <?= form_error('email', '<span class="error">', '</span>'); ?>
        <span><?= form_email(array('name' => 'email', 'id' => 'email', 'class' => 'long_input small-corners', 'placeholder' => lang('restaurant_mail_placeholder'), 'value' => set_value('email'))); ?></span>
    </p>
    <!--phone-->
    <p>
        <?= form_label(lang('restaurant_phone_label'), 'phone', array('class' => 'req')) ?>
        <?= form_error('phone', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'phone', 'id' => 'phone', 'class' => 'medium_input small-corners', 'placeholder' => lang('restaurant_phone_placeholder'), 'value' => set_value('phone'))); ?></span>
    </p>
    <!--website-->
    <p>
        <?= form_label(lang('restaurant_website_label'), 'website') ?>
        <?= form_error('website', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'website', 'id' => 'website', 'class' => 'long_input small-corners', 'placeholder' => lang('restaurant_website_placeholder'), 'value' => set_value('website'))); ?></span>
    </p>
    <p>
        <?= form_label(lang('restaurant_closingday_label'), 'closing_days') ?>
        <?= form_error('closing_days', '<span class="error">', '</span>'); ?>
        <span><?= form_multiselect('closing_days[]', $week_days,array(), 'class="chosen small-corners" multiple="true"') ?></span>
    </p>        
</div>
<div class="six columns">
    <!--address-->
    <p>
        <?= form_label(lang('restaurant_address_label'), 'name', array('class' => 'req')) ?>
        <span><?= form_input(array('name' => 'address', 'id' => 'geocomplete', 'class' => 'long_input small-corners', 'placeholder' => lang('restaurant_address_placeholder'), 'value' => set_value('address'))); ?></span>
        <span id="small_map" class="medium-corners"></span>
        <span><?= form_textarea(array('name' => 'addressinfo', 'id' => 'jsonaddress', 'class' => 'hidden')) ?></span>

    </p>
    <!--
    <p>
        <?= form_label(lang('restaurant_seats_label'), 'seats') ?>
        <?= form_error('seats', '<span class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'seats', 'id' => 'seats', 'class' => 'mini_input small-corners',/* 'placeholder' => lang('restaurant_seats_placeholder'),*/ 'value' => set_value('address'))); ?></span>
    </p> 
    -->
    <!--tags-->
    <?php if (isset($tags)) { foreach ($tags as $tag) { $tag['selected'] = (isset($tag['selected'])) ? $tag['selected']: array();?>
    <p>
        <?= form_label($tag['description'], $tag['name']) ?>
        <?= form_error($tag['name'], '<span class="error">', '</span>'); ?>
        <span><?= form_multiselect($tag['name'] . '[]', $tag['options'], $tag['selected'], 'class="chosen small-corners" multiple="true"') ?></span>
    </p>
    <?php }} ?>
</div>
<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('create'))) ?></p>
</div>
<?= form_close() ?>


