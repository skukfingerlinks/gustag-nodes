<div class="twelve columns">    
    <?php if($usersession->is_admin) { ?>
    <h3><?=lang('list').' '.lang('restaurants')?></h3>
    <?php }else{ ?>
    <h3><?=lang('list').' '.lang('yours').' '.lang('restaurants')?></h3>
    <?php } ?>
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$usersession->username?></p>
    <p><?= anchor('/restaurants/create', lang('create'), array('class' => 'button')) ?></p>
    <?php if (isset($html['found'])) { ?>
        <p><?= $total_rows ?></p>
        <?= $html['pagination_links'] ?>
        <table id="paginated" class="tablesorter">
            <caption><?= lang($html['items_type']) ?></caption>
            <thead>
                <tr>
                    <th class="first" title="Clicca per ordinare per Stato">Stato</th>
                    <?php if($usersession->is_admin) {?>
                    <th title="Clicca per ordinare per Gestore">Gestore</th>
                    <?php } ?>
                    <th title="Clicca per ordinare per Nome">Nome</th>
                    <!--<th title="Clicca per ordinare per Indirizzo">Indirizzo</th>-->
                    <th title="Clicca per ordinare per Piatti">Piatti</th>
                    <th title="Clicca per ordinare per Menu">Men&ugrave;</th>
                    <th title="Clicca per ordinare per Eventi">Eventi</th>
                    <th title="Clicca per ordinare per Immagini">Immagini</th>
                    <th title="Clicca per ordinare per Rating">Rating</th>
                    <?php if($usersession->is_admin) {?>
                    <th>Elimina</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($html['found'] as $item) { ?>
                <?php                    
                    $class='';
                    if($item->active==0 OR sizeof($item->dishes)<1 OR sizeof($item->menus)<1) $class='inactive';
                    //if($item->active==0 OR sizeof($item->dishes)<1 OR sizeof($item->owners)<1 OR !isset($item->menus)) $class='inactive';                
                ?>
                <tr class="<?=$class?>">
                         <td>
                            <?php if($item->active==1){?>
                            <?= anchor($html['items_type'].'/deactivate/' . $item->id, lang('deactivate')) ?>
                            <?php }else{ ?>
                            <?= anchor($html['items_type'].'/activate/' . $item->id, lang('activate')) ?>
                            <?php } ?>
                        </td>
                        <?php if($usersession->is_admin) {?>
                        <td><?= anchor('/users/edit/' . $item->users[0]->node_id, $item->users[0]->username) ?></td>
                        <?php } ?>
                        <td><?= anchor($html['items_type'] . '/edit/' . $item->id, $item->name) ?></td>
                        <!--<td><?= $item->address ?></td>-->
                        <td class="center">
                            <?php if (isset($item->dishes)) { ?>
                                <?= anchor('/dishes/restaurant/' . $item->id, sizeof($item->dishes),array('class'=>'circle')) ?>
                            <?php } else { ?>
                                <?= anchor('/dishes/create/' . $item->id, lang('create')) ?>
                            <?php } ?>
                        </td>
                        <td class="center">
                            <?php if (isset($item->menus)) { ?>
                                <?= anchor('/menus/restaurant/' . $item->id, sizeof($item->menus),array('class'=>'circle')) ?>
                            <?php } else { ?>
                                <?= anchor('/menus/create/' . $item->id, lang('create')) ?>
                            <?php } ?>
                        </td>
                        <td class="center">
                            <?php if (isset($item->events)) { ?>
                                <?= anchor('/events/restaurant/' . $item->id, sizeof($item->events),array('class'=>'circle')) ?>
                            <?php } else { ?>
                                <?= anchor('/events/create/' . $item->id, lang('create')) ?>
                            <?php } ?>
                        </td>
                        
                        <td class="center"><?= anchor($html['items_type'] . '/gallery/' . $item->id, sizeof($item->media)) ?></td>
                        
                        <td class="center">
                            <?php if (isset($item->rating)) { ?>
                                <?= $item->rating ?>
                            <?php } else { ?>
                                <?= lang('not_available_data') ?>
                            <?php } ?>
                        </td>
                        <?php if($usersession->is_admin) {?>
                        <td><?= anchor($html['items_type'].'/delete/' . $item->id, lang('delete')) ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?= $html['pagination_links'] ?>
    <?php } ?>
</div>


