<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('list').' '.lang('restaurants'))?></li>
        <li><span><?=$data->name?></span></li>
        <li><?=anchor('/restaurants/calendar/'.$data->id,lang('calendar'))?></li>
        <li><?=anchor('/dishes/restaurant/'.$data->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$data->id,lang('menus'))?></li>
        <li><?=anchor('/events/restaurant/'.$data->id,lang('events'))?></li>        
        <li><?=anchor('/restaurants/gallery/'.$data->id,lang('image_gallery'))?></li>
    </ul>  
    <div class="clear"></div>    
    <h4><?=lang('restaurant').' '.$data->name?></h4>
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$owner->username?></p>
    <h3><?=lang('update')?></h3>     
</div>
<?= form_open_multipart("/restaurants/edit/" . $data->id, array('id' => 'edit_restaurant')) ?>
<div class="six columns">
    <?php if($usersession->is_admin) {?>
    <p>
        <?= form_label(lang('owner'), 'owner', array('class' => 'req')) ?>
        <!--<span><?= lang($tag['name'] . '_help') ?></span>-->
        <?= form_error('owner', '<span class="error">', '</span>'); ?>
        <?=form_dropdown('owner', $owners, $owner);?>
    </p>  
    <?php } ?>
    <!--name-->
    <p>
        <?= form_label(lang('restaurant_name_label'), 'name', array('class' => 'req')) ?>
        <?= form_error('usermail', '<span class="error" id="name_error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'name', 'id' => 'name', 'class' => 'long_input small-corners', 'placeholder' => lang('restaurant_name_placeholder'), 'value' => $data->name)); ?></span>
    </p>
    <!--logo-->    
    <p>
        <?= form_label(lang('restaurant_logo_label'), 'userfile') ?>
        <span><?=anchor('#',img(array('src'=>$data->logo,'class'=>'logo')),'class="preview" rel="'.$data->logo.'"')?></span>
        <?= form_upload(array('name' => 'userfile', 'id' => 'userfile', 'class' => 'long_input', 'placeholder' => lang('restaurant_logo_help'), 'value' => set_value('logo'))); ?>
        <?= form_error('userfile', '<span class="error" id="userfile_error">', '</span>'); ?>
    </p>      
    <!--desc-->
    <p>
        <?= form_label(lang('restaurant_desc_label'), 'desc', array('class' => 'req')) ?>
        <?= form_error('desc', '<span class="error" id="desc_error">', '</span>'); ?>
        <span><?= form_textarea(array('name' => 'desc','cols'=>80, 'rows'=>4, 'id' => 'desc', 'class' => 'long_input small-corners', 'placeholder' => lang('restaurant_desc_placeholder'), 'value' => $data->desc)); ?></span>
    </p>     
    <!--email-->
    <p>
        <?= form_label(lang('restaurant_mail_label'), 'email', array('class' => 'req')) ?>
        <?= form_error('email', '<span class="error" id="email_error">', '</span>'); ?>
        <span><?= form_email(array('name' => 'email', 'id' => 'email', 'class' => 'long_input small-corners', 'placeholder' => lang('restaurant_mail_placeholder'), 'value' => $data->email)); ?></span>
    </p>
    <!--phone-->
    <p>
        <?= form_label(lang('restaurant_phone_label'), 'phone', array('class' => 'req')) ?>
        <?= form_error('phone', '<span class="error" id="phone_error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'phone', 'id' => 'phone', 'class' => 'medium_input small-corners', 'placeholder' => lang('restaurant_phone_placeholder'), 'value' => $data->phone)); ?></span>
    </p>
    <!--website-->
    <p>
        <?= form_label(lang('restaurant_website_label'), 'website', array('class' => 'req')) ?>
        <?= form_error('website', '<span class="error" id="website_error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'website', 'id' => 'website', 'class' => 'long_input small-corners', 'placeholder' => lang('restaurant_website_placeholder'), 'value' => $data->website)); ?></span>
    </p>
    <p>
        <?= form_label(lang('restaurant_closingday_label'), 'closing_days') ?>
        <?= form_error('closing_days', '<span class="error" id="closing_days_error">', '</span>'); ?>
        <span><?= form_multiselect('closing_days[]', $week_days, $closed_days, 'class="chosen small-corners" multiple="true"') ?></span>
    </p>
</div>
<div class="six columns">
    <!--address-->
    <p>
        <?= form_label(lang('restaurant_address_label'), 'name', array('class' => 'req')) ?>
        <span><?= form_input(array('name' => 'address', 'id' => 'geocomplete', 'class' => 'long_input small-corners', 'placeholder' => lang('restaurant_address_placeholder'), 'value' => $data->address)); ?></span>
        <span id="small_map" class="medium-corners"></span>
        <span><?= form_textarea(array('name' => 'addressinfo', 'id' => 'jsonaddress', 'class' => 'hidden')) ?></span>
    </p>
    <!--
    <p>
        <?= form_label(lang('restaurant_seats_label'), 'seats') ?>
        <?= form_error('seats', '<span class="error" id="seats_error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'seats', 'id' => 'seats', 'class' => 'mini_input small-corners', /* 'placeholder' => lang('restaurant_seats_placeholder'), */ 'value' => $data->seats)); ?></span>
    </p>
    -->
    <!--tags-->
    <?php if (isset($tags)) { foreach ($tags as $tag) { ?>
    <p>
        <?= form_label($tag['description'], $tag['name']) ?>
        <?= form_error($tag['name'], '<span class="error" id="'.$tag['name'].'_error">', '</span>'); ?>
        <span><?= form_multiselect($tag['name'] . '[]', $tag['options'], $tag['selected'], 'class="chosen small-corners" multiple="true"') ?></span>
    </p>
    <?php }} ?>
</div>
<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('update'))) ?></p>
</div>
<?= form_close() ?>

