<div class="sixteen columns">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('list').' '.lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$data->id,$data->name)?></li>
        <li><span><?=lang('calendar')?></span></li>
        <li><?=anchor('/dishes/restaurant/'.$data->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$data->id,lang('menus'))?></li>
        <li><?=anchor('/events/restaurant/'.$data->id,lang('events'))?></li>  
        <li><?=anchor('/restaurants/gallery/'.$data->id,lang('calendar'))?></li>
    </ul>  
    <div class="clear"></div>        
    <h3><?=$data->name.': '.lang('calendar')?></h3>    
    <table id="week">
        <thead>
            <tr>
             <th>Fasce Orarie</th>
             <th>Lun</th>   
             <th>Mar</th>
             <th>Mer</th>
             <th>Gio</th>
             <th>Ven</th>
             <th>Sab</th>
             <th>Dom</th>
            </tr>
        </thead>
        <tbody>
            <tr>
             <th>Fasce Orarie</th>
             <td class="center"><span>Lun</span><span><a class="numup" href="#">+</a><a class="numdown" href="#">-</a></span></td>   
             <td class="center">Mar</td>
             <td class="center">Mer</td>
             <td class="center">Gio</td>
             <td class="center">Ven</td>
             <td class="center">Sab</td>
             <td class="center">Dom</td>
            </tr>       
            <tr>
             <th>Fasce Orarie</th>
             <td class="center">Lun</td>   
             <td class="center">Mar</td>
             <td class="center">Mer</td>
             <td class="center">Gio</td>
             <td class="center">Ven</td>
             <td class="center">Sab</td>
             <td class="center">Dom</td>
            </tr>
            <tr>
             <th>Fasce Orarie</th>
             <td class="center">Lun</td>   
             <td class="center">Mar</td>
             <td class="center">Mer</td>
             <td class="center">Gio</td>
             <td class="center">Ven</td>
             <td class="center">Sab</td>
             <td class="center">Dom</td>
            </tr>
            <tr>
             <th>Fasce Orarie</th>
             <td class="center">Lun</td>   
             <td class="center">Mar</td>
             <td class="center">Mer</td>
             <td class="center">Gio</td>
             <td class="center">Ven</td>
             <td class="center">Sab</td>
             <td class="center">Dom</td>
            </tr>
            <tr>
             <th>Fasce Orarie</th>
             <td class="center">Lun</td>   
             <td class="center">Mar</td>
             <td class="center">Mer</td>
             <td class="center">Gio</td>
             <td class="center">Ven</td>
             <td class="center">Sab</td>
             <td class="center">Dom</td>
            </tr>            
        </tbody>
    </table>
</div>



