<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('list').' '.lang('restaurants'))?></li>        
        <li><?=anchor('/restaurants/edit/'.$data->id,$data->name)?></li>
        <li><?=anchor('/restaurants/calendar/'.$data->id,lang('calendar'))?></li>
        <li><?=anchor('/dishes/restaurant/'.$data->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$data->id,lang('menus'))?></li>
        <li><?=anchor('/events/restaurant/'.$data->id,lang('events'))?></li> 
        <li><span><?=lang('image_gallery')?></span></li>        
    </ul>  
    <div class="clear"></div>        
    <h3><?=$data->name.': '.lang('image_gallery')?></h3>
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$owner->username?></p>
</div>

<?= form_open_multipart("/restaurants/gallery/" . $data->id, array('id' => 'edit_restaurant')) ?>
<div class="twelve columns">
    <p>
        <?= form_label(lang('image_label'), 'userfile',array('class' => 'req')) ?>
        <?= form_upload(array('name' => 'userfile', 'id' => 'userfile', 'class' => 'long_input', 'placeholder' => lang('image_help'), 'value' => set_value('logo'))); ?>
        <?= form_error('userfile', '<span class="error" id="userfile_error">', '</span>'); ?>
    </p>  
    <p>
        <?= form_label(lang('image_desc_label'), 'desc') ?>
        <?= form_error('desc', '<span class="error">', '</span>'); ?>
        <span><?= form_textarea(array('name' => 'desc','cols'=>80, 'rows'=>4, 'id' => 'desc', 'class' => 'long_input small-corners', 'placeholder' => lang('image_desc_placeholder'), 'value' => set_value('desc'))); ?></span>
    </p>     
</div>        
<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('image_upload'))) ?></p>
</div>
<?= form_close() ?>
<?php foreach($data->media as $media){ ?>
<div class="three columns media">    
    <p class="media_actions">
        <?=anchor('/restaurants/deleteimage/'.$media->node_id,lang('delete'),array('class'=>''))?>
    </p>      
    <p class="media_image center"><?=img(array('src'=>$media->url,'alt'=>$media->alt,'class'=>'thumb'))?></p>
    <p class="media_desc center"><?=$desc = (isset($media->desc)) ? $media->desc: '' ;?></p> 
</div>
<?php } ?>



