<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/restaurants',lang('list').' '.lang('restaurants'))?></li>
        <li><?=anchor('/restaurants/edit/'.$data->id,$data->name)?></li>
        <li><span><?=lang('calendar')?></span></li>
        <li><?=anchor('/dishes/restaurant/'.$data->id,lang('dishes'))?></li>
        <li><?=anchor('/menus/restaurant/'.$data->id,lang('menus'))?></li>
        <li><?=anchor('/events/restaurant/'.$data->id,lang('events'))?></li>   
        <li><?=anchor('/restaurants/gallery/'.$data->id,lang('image_gallery'))?></li>
    </ul>  
    <div class="clear"></div>     
    <h4><?=lang('restaurant').' '.$data->name?></h4>
    <p><strong><?=lang('owner')?></strong>:&nbsp;<?=$owner->username?></p>
    <h3><?=lang('calendar')?></h3>     
</div>
<div class="twelve columns">
    <p><?=lang('calendar_help')?></p>
    <?= form_open($_SERVER['REQUEST_URI'], array('id' => 'calendar_form')) ?>
    <p class="hidden"><?= form_input(array('name' => 'nid', 'id' => 'nid','value' => $data->id)); ?></p>
    <?=$calendar?>
    <p><?=form_textarea(array('name'=>'dates','id'=>'dates','class'=>'hidden'))?></p>
</div>
<div class="twelve columns offset-by-four">
    <p class="center">
            <!--<?= anchor('/dishes/create/'.$data->id,lang('continue'),array('id' => 'continue', 'class'=>'button')) ?>-->
            <!--<?= anchor('#',lang('reset_calendar'),array('id' => 'reset', 'class'=>'button')) ?>-->
    </p>
    <?=form_close()?>       
</div>
<div id="calendar_dialog" class="hidden" title="<?=lang('reset_calendar')?>">
    <div id="calendar_dialog_content"></div>
</div>


