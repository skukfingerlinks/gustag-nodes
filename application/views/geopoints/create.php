<div class="twelve columns content">
    <ul class="dropdown">
        <li><?=anchor('/geopoints',lang('list').' '.lang('geopoints'))?></li>
    </ul>  
    <div class="clear"></div>        
    <h3><?=lang('create').' '.lang('geopoint')?></h3>
</div>
<?= form_open_multipart("/geopoints/create", array('id' => 'edit_geopoint')) ?>
<div class="twelve columns">          
    <!--address-->
    <p>
        <?= form_label(lang('geopoint_address_label'), 'name', array('class' => 'req')) ?>
        <span><?= form_input(array('name' => 'address', 'id' => 'geocomplete', 'class' => 'long_input small-corners', 'placeholder' => lang('geopoint_address_placeholder'), 'value' => set_value('address'))); ?></span>
        <span id="small_map" class="medium-corners"></span>
        <span><?= form_textarea(array('name' => 'addressinfo', 'id' => 'jsonaddress', 'class' => 'hidden')) ?></span>
    </p>
</div>
<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('create'))) ?></p>
</div>
<?= form_close() ?>


