<div class="twelve columns">
    <h4><?= lang('update').' '.lang('geopoint') ?></h4>
    <h3><?= $data->formatted_address ?></h3>
    <?=anchor('/geopoints',lang('list').' '.lang('geopoints'))?>
</div>
<?= form_open_multipart("/geopoint/edit/" . $data->id, array('id' => 'edit_user')) ?>
<div class="twelve columns">
    <p>
        <?= form_label(lang('geopoint'), 'username', array('class' => 'req')) ?>
        <span class="desc"><?= lang('user_locations_help') ?></span>
        <?= form_error('username', '<span id="username_error" class="error">', '</span>'); ?>
        <span><?= form_input(array('name' => 'username', /* 'disabled'=>'true', */ 'id' => 'username', 'class' => 'long_input small-corners ', 'placeholder' => lang('user_name_placeholder'), 'value' => $data->username)); ?></span>
    </p>
    <!--address-->
    <p>
        <?= form_label(lang('geopoint_address_label'), 'name', array('class' => 'req')) ?>
        <span><?= form_input(array('name' => 'address', 'id' => 'geocomplete', 'class' => 'long_input small-corners', 'placeholder' => lang('geopoint_address_placeholder'), 'value' => set_value('address'))); ?></span>
        <span id="small_map" class="medium-corners"></span>
        <span><?= form_textarea(array('name' => 'addressinfo', 'id' => 'jsonaddress', 'class' => 'hidden')) ?></span>
    </p>    
</div>
<div class="twelve columns center offset-by-four">
    <!--submit-->
    <p class="buttons"><?= form_button(array('name' => 'submit', 'id' => 'login', 'type' => 'submit', 'content' => lang('update'))) ?></p>
</div>
<?=form_close()?>