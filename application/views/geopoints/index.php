<div class="twelve columns content">
    <div class="clear"></div>        
    <h3><?=lang('list').' '.lang($html['items_type'])?></h3>
    <p>The easiest way to really get started with Skeleton is to check out the full docs and info at <a href="http://www.getskeleton.com">www.getskeleton.com.</a>.</p>
    <p><?= anchor('/geopoints/create', lang('create').' '.lang($html['item']), array('class' => 'button')) ?></p>
    <p><?=lang($html['items_type']).' '.lang('found').': <strong>'.$html['total_items'].'</strong>'?></p>    
</div>
<div class="twelve columns">
    <?php if (isset($html['found'])) { ?>
        <?= $html['pagination_links'] ?>
        <table id="paginated" class="tablesorter">
            <caption><?= lang($html['items_type']) ?></caption>
            <thead>
                <tr>
                    <th class="first" title="Clicca per ordinare per Stato">Stato</th>    
                    <th title="Clicca per ordinare per Indirizzo">Indirizzo</th>            
                    <th title="Clicca per ordinare per Provincia">Provincia</th>    
                    <!--<th title="Clicca per ordinare per Regione">Regione</th>-->
                    <th>Elimina</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($html['found'] as $item) { ?>
                    <tr>
                        <td><?php echo $active = ($item->active == 1) ? lang('active') : lang('inactive'); ?></td>
                        <td><?= anchor($html['items_type'].'/edit/' . $item->id, $item->formatted_address) ?></td>
                        <td><?=$item->administrative_area_level_3?></td>
                        <!--<td><?=$item->administrative_area_level_1?></td>-->
                        <td><?= anchor($html['items_type'].'/delete/' . $item->id, lang('delete')) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?= $html['pagination_links'] ?>
    <?php } ?>
</div>


