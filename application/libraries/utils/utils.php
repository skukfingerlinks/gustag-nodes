<?php

class Utils {

    public function __construct() {

        $CI = &get_instance();
        $CI->config->load('settings', TRUE);
        $this->global_config = $CI->config->item('settings');
    }

    public function _clean_obj(&$obj, $active = TRUE) {

        foreach ($obj as $k => $v) {

            //if($active) if(!is_array($v) AND is_object($v) AND isset($obj->active) AND $obj->$k==0) unset($obj->$k);
            if (is_null($v) AND !is_array($v) AND !strlen($v))
                unset($obj->$k);
            if (!is_object($v) AND !is_array($v) AND !strlen($v))
                unset($obj->$k);
            if (in_array($k, $this->private_properties))
                unset($obj->$k);
            if (is_array($v))
                foreach ($v as $item)
                    $this->_clean_obj($item);
            if (is_object($v))
                $this->_clean_obj($v);
        }
    }

    public function paginate($items, $default_limit = 1) {

        $paginated['paginated'] = TRUE;
        $paginated['request_time'] = $this->dateLong();
        $total = sizeof($items);
        //SE GET_page NON E' DEFINITO O MINORE DI 1 LO SETTA A 1 ALTRIMENTI CONSERVA IL VALORE
        $page = (!isset($_GET['page']) OR $_GET['page'] < 1 ) ? 1 : $_GET['page'];
        //SE GET_limit NON E' DEFINITO O MINORE DI 1 LO SETTA A 1 ALTRIMENTI CONSERVA IL VALORE
        $limit = (!isset($_GET['limit']) OR $_GET['limit'] < 1 ) ? $default_limit : $_GET['limit'];
        //SE $limit E' MINORE DEL TOTALE DEI RISULTATI LO SETTA AL TOTALE DEI RISULTATI
        $paginated['limit'] = $limit;


        if ($limit > $total)
            $limit = $total;
        //SPLITTO L'ARRAY IN $limit ARRAY
        //$chunks = array_chunk($items, $limit);
        //DEFINISCO L'ARRAY DA RESTITUIRE
        $paginated['total_items'] = $total;
        $chunks = array_chunk($items, $limit, true);
        $paginated['total_pages'] = sizeof($chunks);
        $paginated['actual_page'] = (int) $page;
        if (!isset($chunks[$page - 1])) {
            $paginated['items'] = $chunks[0];
            $paginated['actual_page'] = 1;
        } else {
            $paginated['items'] = $chunks[$page - 1];
        }

        $prev = $paginated['actual_page'] - 1;
        $next = $paginated['actual_page'] + 1;

        if ($prev > 0) {
            $paginated['prev_link'] = current_url() . '?page=' . $prev;
        }
        if ($next <= $paginated['total_pages']) {
            $paginated['next_link'] = current_url() . '?page=' . $next;
        }
        //RIMUOVO GLI INDICI DALL'ARRAY ITEMS
        $paginated['items'] = array_values((array) $paginated['items']);



        return $this->array2object($paginated);
    }

    public function array2object($array) {
        return json_decode(json_encode($array), FALSE);
    }

    public function object2array($object) {
        return json_decode(json_encode($object), TRUE);
    }

    public function dateLong($time = FALSE) {
        $timestamp = time();
        if ($time)
            $timestamp = $time;
        return $timestamp * 1000;
    }

    public function jsonOutput($data) {
        header('content-type: application/json; charset=utf-8');
        echo json_encode($data, JSON_NUMERIC_CHECK);
        exit;
    }

    public function jsonError($data){
        $response = array(
            'success' => FALSE,
            'error_code' => $data['error_code'],
            'error_msg' => $data['error_msg'],
        );
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($response, JSON_NUMERIC_CHECK); //,JSON_NUMERIC_CHECK
        die;        
    }
    
    
    public function jsonResponse($data) {

        $result = FALSE;
        if (isset($data->items) AND sizeof($data->items) > 1) {
            $items_per_page = $this->global_config['pagination_default'];
            $result = $this->paginate($data->items, $items_per_page);
        } else {
            $result = $data;
        }
        $response = array(
            'success' => TRUE,
            'data' => $result,
        );
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($response, JSON_BIGINT_AS_STRING); //,JSON_NUMERIC_CHECK
        die;

        header('content-type: application/json; charset=utf-8');
        echo json_encode($data, JSON_NUMERIC_CHECK);
        exit;
    }

    public function dump($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        
    }

    public function generateRandomString($length = FALSE) {
        if(!$length) $length = $this->global_config['pwd_min_lenght'];
        return substr(str_shuffle(implode(array_merge(range(0, 9), range('A', 'Z'), range('a', 'z')))), 0, $length);
    }

    public function encrypt($str) {
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->global_config['encryption_salt'], $str, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

    public function decrypt($str) {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->global_config['encryption_salt'], base64_decode($str), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }

    public function chosenOptions($data, $key = 'id', $name = 'name') {
        $chosen_options = array();
        foreach ($data as $item) {
            $chosen_options[$item->$key.'-'.trim($item->$name)] = trim($item->$name);
        }
        return $chosen_options;
    }

    public function chosenResult($data) {
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $explode = explode('-', $v);
                $chosen_values[] = $explode[0];
            }
        } else {
            $chosen_values[] = $data;
        }
        return $chosen_values;
    }

    public function formatGoogleAddress($jsonaddress) {
        $json = json_decode($jsonaddress);



        $address = array();
        $address['googleid'] = $json->id;
        $address['formatted_address'] = $json->formatted_address;
        foreach ($json->address_components as $address_component) {

            $address[$address_component->types[0]] = $address_component->long_name;
            if ($address_component->types[0] == 'administrative_area_level_2' AND isset($address_component->short_name)) {
                $address['administrative_area_level_2_label'] = $address_component->short_name;
            }
        }
        if (!isset($address['administrative_area_level_3']))
            $address['administrative_area_level_3'] = $address['administrative_area_level_2'];

        if (isset($json->geometry->location->lb))
            $address['latitude'] = $json->geometry->location->lb;
        if (isset($json->geometry->location->nb))
            $address['latitude'] = $json->geometry->location->nb;

        if (isset($json->geometry->location->ob))
            $address['longitude'] = $json->geometry->location->ob;
        if (isset($json->geometry->location->mb))
            $address['longitude'] = $json->geometry->location->mb;
        if (isset($json->geometry->location->pb))
            $address['longitude'] = $json->geometry->location->pb;

        //$address['latitude'] = (isset($json->geometry->location->lb)) ? $json->geometry->location->lb : $json->geometry->location->nb;
        //$address['longitude'] = $json->geometry->location->mb;

        if ($json->id)
            $address['url'] = $json->url;

        //$this->dump($address);

        return $this->array2object($address);
    }

    function thismonthdates($startDate = FALSE, $range = 'next month') {

        if (!$startDate)
            $startDate = date('Y-m-01');
        //$endDate = date('Y-m-d',strtotime('next month',strtotime($startDate)));
        $endDate = date('Y-m-d', strtotime($range, strtotime($startDate)));

//            echo $startDate.'<br />';
//            echo $endDate.'<br />';
//            die;
        $arr = array();
        $now = strtotime($startDate);
        $last = strtotime($endDate);

        while ($now <= $last) {
            $arr[] = array(
                'date' => date('d-m-Y', $now),
                'day' => date('d', $now),
                'month' => date('m', $now),
                'year' => date('Y', $now),
                'weekday' => date('N', $now),
                'weekday_str' => date('l', $now),
                'datelong' => strtotime(date('Y-m-d', $now)) * 1000,
            );
            $now = strtotime('+1 day', $now);
        }
        array_pop($arr);
        return $this->array2object($arr);
    }

    public function make_dir($path, $permissions = 0777) {
        if (!file_exists($path)) {
            $oldmask = umask(0);
            mkdir($path, $permissions);
            umask($oldmask);
        }
    }

    public function make_dir_tree($structure, $path = __DIR__) {
        foreach ($structure as $folder => $sub_folder) {
            // Folder with subfolders
            if (is_array($sub_folder)) {
                $new_path = "{$path}/{$folder}";
                if (!is_dir($new_path))
                    $this->make_dir($new_path);
                call_user_func(array(__CLASS__, 'make_dir_tree'), $sub_folder, $new_path);
            } else {
                $new_path = "{$path}/{$sub_folder}";
                if (!is_dir($new_path))
                    $this->make_dir($new_path);
            }
        }
    }
    
    public function rrmdir($dir) { 
       if (is_dir($dir)) { 
         $objects = scandir($dir); 
         foreach ($objects as $object) { 
           if ($object != "." && $object != "..") { 
             if (filetype($dir."/".$object) == "dir"){
                $this->rrmdir($dir."/".$object);             
             }else{
                 unlink($dir."/".$object); 
             }
           } 
         } 
         reset($objects); 
         rmdir($dir); 
       } 
     }     
    /*
    public function rrmdir($dir) {
        foreach(glob($dir . '/*') as $file) {
            if(is_dir($file))
                echo "rrmdir($file);<br />";
            else
                echo "unlink($file);<br />";                
        }
        echo "rmdir($dir);<br />";
    }   
    */
     
    public function dateFormat($date, $format = 'm/d/Y') {

        $date = DateTime::createFromFormat($format, $date);
        return strtotime($date->format('Y-m-d')) * 1000;
    }     
     
    public function valid_email($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL))
            return TRUE;
        return FALSE;
    }

    public function valid_url($url) {
        if (filter_var($url, FILTER_VALIDATE_URL))
            return TRUE;
        return FALSE;
    }

    public function valid_pwd($pwd) {
        //$valid = "#.*^(?=.{5,8})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#";
        $valid = "/^[A-Za-z0-9._-]{4,255}$/";
        if (preg_match($valid, $pwd))
            return TRUE;
        return FALSE;
    }

    public function valid_username($username) {
        //$valid = '/^[A-Za-z]{1}[A-Za-z0-9]{4,10}$/';
        //$valid = "/^[a-zA-Z0-9_]+((\.-?|-\.?)[a-zA-Z0-9_]+)*$/";
        $valid = "/^[A-Za-z0-9._-]{4,255}$/";
        if (preg_match($valid, $username))
            return TRUE;
        return FALSE;
    }

    public function valid_latitude($latitude) {
        $min = -90.0000000;
        $max = 90.0000000;
        if (filter_var($latitude, FILTER_VALIDATE_FLOAT) AND ($min - $latitude) < 0.00001 AND ($max - $latitude) > 0.00001)
            return TRUE;
        $this->jsonResponse('INVALID_LATITUDE', FALSE);
    }

    public function valid_longitude($longitude) {
        $min = -180.0000000;
        $max = 180.0000000;
        if (filter_var($longitude, FILTER_VALIDATE_FLOAT) AND ($min - $longitude) < 0.00001 AND ($max - $longitude) > 0.00001)
            return TRUE;
        $this->jsonResponse('INVALID_LONGITUDE', FALSE);
    }     

}

