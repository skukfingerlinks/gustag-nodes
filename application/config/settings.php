<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* SECRETS */
$config['encryption_salt'] = 'drupallike3';
$config['pwd_min_lenght'] = 5;
/* MILES */
$config['miles'] = 0.000621371192;

/* PAGINATION */
$config['items_per_page'] = 1;
$config['pagination_default'] = 5;

$config['googleApiKey'] = 'AIzaSyBUtfP4x5cszPUh0WZn6Kkiy_TIrvr1YMY';