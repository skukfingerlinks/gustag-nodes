<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['allowed_formats'] = 'gif|jpg|jpeg|png';
$config['max_width'] = 640;
$config['max_height'] = 480;