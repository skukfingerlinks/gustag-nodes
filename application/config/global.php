<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* APP KEY */
$config['app_key'] = '494905f4f71ad609f730155bc1499dadaa933512';//sha1('appkey');

/* SECRETS */
$config['encryption_salt']='drupallike3';

/* MILES */
$config['miles']=0.000621371192;

/* PAGINATION */
$config['items_per_page']=10;
$config['pagination_default'] = 5;

$config['googleApiKey']='AIzaSyBUtfP4x5cszPUh0WZn6Kkiy_TIrvr1YMY';
$config['qrcodespath']=$_SERVER['DOCUMENT_ROOT'].'assets/qrcodes/';
$config['qrcodespathurl']=$_SERVER['HTTP_HOST'].'/assets/qrcodes/';

/* USER DIRS */
$config['user_dirs_path'] = $_SERVER['DOCUMENT_ROOT'].'assets/users/';
$config['user_dirs_url'] = base_url('/assets/users/');
$config['user_dirs'][] = '/avatar';
$config['user_dirs'][] = '/gallery';
$config['user_default_logo'] = '/assets/img/default-avatar.png';

/* RESTAURANT DIRS */
$config['restaurant_dirs_path'] = $_SERVER['DOCUMENT_ROOT'].'assets/restaurants/';
$config['restaurant_dirs_url'] = base_url('/assets/restaurants/');
$config['restaurant_dirs'][] = '/logo';
$config['restaurant_dirs'][] = '/gallery';
$config['restaurant_default_logo'] = '/assets/img/default-restaurant.png';

/* EVENT DIRS */
$config['event_dirs_path'] = $_SERVER['DOCUMENT_ROOT'].'assets/events/';
$config['event_dirs_url'] = base_url('/assets/events/');
$config['event_dirs'][] = '/logo';
$config['event_dirs'][] = '/gallery';
$config['event_default_logo'] = '/assets/img/default-event.png';

/* DISH DIRS */
$config['dish_dirs_path'] = $_SERVER['DOCUMENT_ROOT'].'assets/dishes/';
$config['dish_dirs_url'] = base_url('/assets/dishes/');
$config['dish_dirs'][] = '/logo';
$config['dish_dirs'][] = '/gallery';
$config['dish_default_logo'] = '/assets/img/default-dish.png';



/* UPLOADS */

$config['uploads'] = $_SERVER['DOCUMENT_ROOT'].'/assets/uploads/';
$config['uploads_url'] = base_url().'assets/uploads/';
$config['uploads_path'] = '/assets/uploads/';

/* DOWNLOADS */

$config['downloads'] = $_SERVER['DOCUMENT_ROOT'].'/assets/downloads/';
$config['downloads_url'] = base_url().'assets/downloads/';
$config['downloads_path'] = '/assets/downloads/';

/* DISTANCE CONVERSION */;

$config['places_distance_range'] = 500; //meters
$config['miles_conversion']=0.621371192;
$config['meters_miles_conversion']=1609.34;
$config['meter_to_feet']=3.2808399;
$config['feet_to_mile']=0.000189393939;
$config['miles_to_meters']=1609.34;
$config['meters_to_miles']=0.000621371;
$config['places_distance_range_miles'] = ($config['places_distance_range']/100)*$config['miles_conversion'];