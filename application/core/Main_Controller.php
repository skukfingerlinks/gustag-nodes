<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main_Controller extends CI_Controller {

    public $viewdata = array();
    public $usersession = array();

    public function __construct() {
        parent::__construct();

        $this->load->config('global');

        $this->output->enable_profiler(FALSE);

        $this->usersession = FALSE;

        /* BOOTSTRAP */
        $this->_load_utils();
        $this->_handleRequests();
        $this->_load_props();
        $this->initFacebook();

        /* MOBILE */


        $this->load->library('mobiledetect/Mobile_Detect');
        $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
            $this->is_mobile = TRUE;
        } else {
            $this->is_mobile = FALSE;
        }


        /* LANGUAGE */
        $this->default_language = 'italian';
        $this->lang->load('global', $this->default_language);
        $this->config->set_item('language', $this->default_language);


        /* TEMPLATE INIT */
        $this->_setTemplate();

        /* MODELS */
        $this->load->model('Node_Model', 'NodeModel');
        $this->load->model('User_Model', 'UserModel');
        $this->load->model('Role_Model', 'RoleModel');
        $this->load->model('_Menu_Model', '_MenuModel');
        $this->load->model('Tag_Model', 'TagModel');
        $this->load->model('Geopoint_Model', 'GeopointModel');

        /* GET SESSION */

        $this->_get_usersession();
        
        //$this->_set_closed();
    }
    
    /* SET CLOSED */
    protected function _set_closed(){
        $this->load->model('Restaurant_Model','RestaurantModel');
        $this->RestaurantModel->set_close_by_day();
        $this->RestaurantModel->set_close_by_date();
        $this->load->model('Event_Model','EventModel');
        $this->EventModel->set_all_close_by_default();        
        $this->EventModel->set_open();        
    }



    /* FACEBOOK */
    
    protected function initFacebook() {
        /* FACEBOOK LIBRARY */
        $this->load->config('facebook');
        $this->fbscopes = $this->config->item('fbscopes');
        $this->load->library('facebook', array('appId' => $this->config->item('fbappid'), 'secret' => $this->config->item('fbsecret'), 'fileUpload' => TRUE));
    }    
    
    function fbapiCall($request, $method = 'get', $params = array()) {

        try {
            $response = $this->facebook->api($request, $method, $params);
            return $this->utils->array2object($response);
        } catch (FacebookApiException $e) {

            $this->utils->dump($e);

            $fbexception = $e->getResult();
            $response = array(
                'success' => FALSE,
                'error_code' => $e->getType(),
                'error_lang' => $this->language,
                'error_msg' => $fbexception['error']['type'] . ' ' . $fbexception['error']['message'],
            );
            header('content-type: application/json; charset=utf-8');
            echo json_encode($response, TRUE);
            die;
        }
    }

    function fbapiFQL($query, $params = array()) {


        $fql = "https://graph.facebook.com/fql?q=" . urlencode($query) . "&access_token=" . $params['access_token'];
        $params = array(
            'method' => 'fql.query',
            'query' => $query,
            'access_token' => $params['access_token'],
        );

        try {
            $response = $this->facebook->api($params);
            return $this->utils->array2object($response);
        } catch (FacebookApiException $e) {

            $this->utils->dump($e);

            $fbexception = $e->getResult();
            $response = array(
                'success' => FALSE,
                'error_code' => $e->getType(),
                'error_lang' => $this->language,
                'error_msg' => $fbexception['error']['type'] . ' ' . $fbexception['error']['message'],
            );
            header('content-type: application/json; charset=utf-8');
            echo json_encode($response, TRUE);
            die;
        }
    }    
    
    /* PROPS */

    protected function _load_props() {
        $this->private_properties = array(
            /*
            'userpwd',
            'plainpwd',
            'registration_mail',
            'recurrent',
            'fbtoken',
            'user_token',
            'created',
            */
                //'node_id',
                /*
                  'updated',
                  'active',

                  'type_id',
                  'type',
                  'object',
                 */
        );
        $this->genders = array(
            'm' => $this->lang->line('male'),
            'f' => $this->lang->line('female'),
        );
        $this->week_days = array(
            0 => 'Tutti i giorni',
            1 => 'Luned&igrave;',
            2 => 'Marted&igrave;',
            3 => 'Mercoled&igrave;',
            4 => 'Gioved&igrave;',
            5 => 'Venerd&igrave;',
            6 => 'Sabato',
            7 => 'Domenica',
        );
    }

    /* UTILS */

    protected function _load_utils() {
        //UTILS
        $this->load->library('utils/utils');
        //$this->load->library('mobiledetect/Mobile_Detect');
        //MOBILE DETECTION - https://code.google.com/p/php-mobile-detect/wiki/Mobile_Detect
        //include APPPATH . 'third_party/Mobile_Detect.php';
        //$this->Mobile_Detect = new Mobile_Detect;
        //USER AGENT
        $this->load->library('user_agent');
    }

    /* REQUESTS */

    protected function _handleRequests() {
        /* REQUESTS */

        //HTTP HEADER REQUESTS
        $this->httpheaders = FALSE;
        $httpheaders = $this->input->request_headers();
        if ($httpheaders)
            $this->httpheaders = $httpheaders;

        //HTTP RAW DATA
        $this->rawrequest = FALSE;
        $raw = file_get_contents("php://input");
        if (isset($raw))
            $this->rawrequest = $this->utils->array2object($raw);

        //HTTP GET REQUESTS
        $this->getrequest = FALSE;
        $get = $this->input->get();
        if ($get)
            $this->getrequest = $this->utils->array2object($get);

        //HTTP POST REQUESTS
        $this->postrequest = FALSE;
        $post = $this->input->post();
        if ($post)
            $this->postrequest = $this->utils->array2object($post);

        //HTTP FILES REQUESTS
        $this->filesrequest = FALSE;
        $files = $_FILES;
        if ($files)
            $this->filesrequest = $this->utils->array2object($files);
    }

    /* TEMPLATES */

    public function _setTemplate() {

        $this->base_template = 'base';
        $this->viewdata = array();
        $this->viewdata['html']['page_title'] = 'Gustag';
        $this->viewdata['html']['head_lang'] = 'en';
        $this->viewdata['html']['meta_charset'] = 'utf-8';
        $this->viewdata['html']['meta_description'] = 'description';
        $this->viewdata['html']['meta_author'] = 'Author';
        $this->viewdata['html']['meta_viewport'] = 'width=device-width, initial-scale=1, maximum-scale=1';
        $this->viewdata['html']['h1_title'] = $this->viewdata['html']['page_title'];
        $this->viewdata['html']['h2_payoff'] = 'Discover your food!';
        $this->viewdata['html']['global_css'][] = array('src' => '//fonts.googleapis.com/css?family=Open+Sans', 'media' => 'all');
        $this->viewdata['html']['global_css'][] = array('src' => '/assets/css/base.css', 'media' => 'all');
        $this->viewdata['html']['global_css'][] = array('src' => '/assets/css/skeleton.css', 'media' => 'all');
        $this->viewdata['html']['global_css'][] = array('src' => '/assets/css/layout.css', 'media' => 'all');
        $this->viewdata['html']['global_css'][] = array('src' => '/assets/css/style.css', 'media' => 'all');
    }

    protected function _set_Jquery() {
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery-1.9.1/jquery.min.js');
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery-1.9.1/jquery-ui.min.js');
        $this->viewdata['html']['global_js'][] = array('src' => '//code.jquery.com/jquery-migrate-1.2.1.js');
        $this->viewdata['html']['extra_css'][] = array('src' => '/assets/js/jquery-1.9.1/smoothness/jquery-ui-1.9.2.custom.min.css', 'media' => 'all');
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/custom/utils.js');
    }

    protected function _set_ChosenJS() {
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/chosen/chosen.jquery.js');
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/chosen/init.js');
        $this->viewdata['html']['extra_css'][] = array('src' => '/assets/js/chosen/chosen.css', 'media' => 'all');
    }

    protected function _set_JqueryTablesorter() {
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery.tablesorter/jquery.tablesorter.min.js');
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery.tablesorter/init.js');
    }

    protected function _set_JqueryValidate() {
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery.validate/jquery.validate.min.js');
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery.validate/additional-methods.min.js');
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery.validate/messages_it.js');
    }

    protected function _set_GMap() {
        $this->viewdata['html']['global_js'][] = array('src' => '//maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places');
    }

    protected function _set_JqueryGeocomplete() {
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery.geocomplete/jquery.geocomplete.js');
    }

    protected function _set_JqueryMultiDatesPicker() {
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery.multidatespicker/jquery.ui.datepicker.js');
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery.multidatespicker/jquery-ui.multidatespicker.js');
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery.multidatespicker/datespicker.js');
        $this->viewdata['html']['global_js'][] = array('src' => '/assets/js/jquery.multidatespicker/jquery.ui.datepicker-it.js');
        $this->viewdata['html']['extra_css'][] = array('src' => '/assets/js/jquery.multidatespicker/mdp.css', 'media' => 'all');
    }

    protected function _set_custom_js($js) {
        $this->viewdata['html']['global_js'][] = array('src' => $js);
    }

    protected function _set_custom_css($css, $media = 'all') {
        $this->viewdata['html']['extra_css'][] = array('src' => $css, 'media' => $media);
    }

    public function show($view, $tpl = FALSE) {
        if (!$tpl)
            $tpl = $this->base_template;
        $this->load->view($tpl . '/header', $this->viewdata);
        $this->load->view($view, $this->viewdata);
        $this->load->view($tpl . '/footer', $this->viewdata);
    }

    /* LOGIN/LOGOUT */

    protected function _get_usersession() {
        if ($this->session->userdata('usersession')) {
            $this->usersession = $this->viewdata['usersession'] = $this->session->userdata('usersession');
            $this->viewdata['showuserinfo'] = TRUE;
            $this->_set_rolemenu();
        }
    }

    protected function _is_logged() {
        if (!$this->usersession->is_logged) {
            $this->_logout();
        }
    }

    protected function _login($nid) {

        $node = $this->_get($nid, TRUE);

        //$this->utils->dump($node);

        $user = new stdClass;
        $user->id = $node->id;
        $user->username = $node->username;
        $user->usermail = $node->usermail;
        $user->avatar = $node->avatar;
        $user->status = $node->status;
        $user->fbuser = $node->fbuser;

        foreach ($node->roles as $role) {

            if (in_array($role->id, array(1)))
                $user->is_sysadmin = TRUE;
            if (in_array($role->id, array(2)))
                $user->is_admin = TRUE;
            if (in_array($role->id, array(3)))
                $user->is_owner = TRUE;
            if (in_array($role->id, array(4)))
                $user->is_agent = TRUE;
            $user->roles[] = $role->id;
        }

        $user->is_logged = TRUE;
        $user->logintime = $this->utils->dateLong();
        $user->logouttime = (time() + $this->config->item('sess_expiration')) * 1000;

        $this->session->set_userdata('usersession', $user);
    }

    protected function _logout() {
        $this->session->sess_destroy();
        redirect('/login');
    }

    protected function _set_error($error, $redirecturi = FALSE) {
        if ($this->is_mobile) {
            $this->utils->jsonError();
        } else {
            $this->session->set_flashdata('error', $error);
            redirect($redirecturi, 'refresh');
        }
    }

    public function _set_rolemenu() {

        $menu_links = array();
        $user_roles = array();
        if (isset($this->usersession->roles))
            $user_roles = $this->usersession->roles;

        $menu = $this->_MenuModel->set_roles_menu($user_roles);
        if ($menu) {

            foreach ($menu as $item) {
                $submenu = $this->_MenuModel->set_roles_submenu($item->id, $user_roles);
                if ($submenu)
                    foreach ($submenu as $submenu_item)
                        $item->submenu[] = $submenu_item;
                $menu_links[] = $item;
            }
        }
        //$this->utils->dump($menu_links);
        $this->viewdata['role_menu'] = array();
        foreach ($menu_links as $item) {

            $this->viewdata['role_menu'][$item->name] = array(
                'url' => $item->url,
                'link' => $this->lang->line($item->name),
            );
            if (isset($item->attribute_id))
                $this->viewdata['role_menu'][$item->name]['props'] = array('id' => $item->attribute_id);
            if (isset($item->attribute_class))
                $this->viewdata['role_menu'][$item->name]['props'] = array('id' => $item->attribute_class);
            if (isset($item->submenu)) {
                foreach ($item->submenu as $submenu) {
                    $this->viewdata['role_menu'][$item->name]['submenu'][$submenu->name] = array(
                        'url' => $submenu->url,
                        'link' => $this->lang->line($submenu->name),
                    );
                    if (isset($submenu->attribute_id))
                        $this->viewdata['role_menu'][$item->name]['submenu'][$submenu->name]['props'] = array('id' => $submenu->attribute_id);
                    if (isset($submenu->attribute_class))
                        $this->viewdata['role_menu'][$item->name]['submenu'][$submenu->name]['props'] = array('class' => $submenu->attribute_class);
                }
            }
        }

        if (isset($this->usersession->roles))
            unset($this->viewdata['role_menu']['login']);
    }

    /* NODES */

    protected function _paginate($url, $count, $found, $per_page = FALSE) {

        if (!$per_page)
            $per_page = $this->config->item('items_per_page');

        $config['base_url'] = site_url($url);
        $config['total_rows'] = $count;
        $config['per_page'] = $per_page;
        $config['num_links'] = ceil($config['total_rows'] / $config['per_page']);
        $config['full_tag_open'] = '<p id="pagination">';
        $config['full_tag_close'] = '<p>';
        $config['next_tag_open'] = '<span>';
        $config['next_tag_close'] = '</span>';
        $config['prev_tag_open'] = '<span>';
        $config['prev_tag_close'] = '</span>';
        $config['num_tag_open'] = '<span>';
        $config['num_tag_close'] = '</span>';
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = '</span>';

        //$this->utils->dump($config);die;

        $this->pagination->initialize($config);
        $this->viewdata['html']['pagination_links'] = $this->pagination->create_links();
        $this->viewdata['html']['total_items'] = $count;
        $this->viewdata['html']['found'] = $found;
    }

    protected function _clean_obj(&$obj, $active = TRUE) {
        /*
          return $this->utils->array2object(array_filter($this->utils->object2array($obj)));
         */

        foreach ($obj as $k => $v) {
            if (is_null($v) AND !is_array($v) AND !strlen($v))
                unset($obj->$k);
            if (!is_object($v) AND !is_array($v) AND !strlen($v))
                unset($obj->$k);

            if (in_array($k, $this->private_properties))
             unset($obj->$k);

            if (is_array($v))
                foreach ($v as $item)
                    $this->_clean_obj($item);
            if (is_object($v))
                $this->_clean_obj($v);
        }
    }

    protected function _type(&$node) {
        $type = $this->NodeModel->get_type($node->type_id);
        if ($type)
            $node->type = $type;
    }

    protected function _object_data(&$node) {
        $type = $this->NodeModel->get_object_data($node->type->object, $node->id);
        if ($type) {
            foreach ($type as $k => $v) {
                //ESCLUDO NODE ID
                if ($k !== 'node_id' OR $k !== 'id')
                    $node->$k = $v;
            }
        }
    }

    protected function _object_tags(&$node) {
        $object_tags = $this->NodeModel->get_object_tags($node->type->object);
        if ($object_tags)
            $node->object_tags = $object_tags;
    }

    protected function _node_geopoints(&$node) {
        $node_geopoints = $this->NodeModel->get_node_geopoints($node->id);
        if ($node_geopoints) {
            
            if(sizeof($node_geopoints)>1){
                $node->geopoints = $node_geopoints;
            }else{
                $node->geopoints = $node_geopoints[0];
            }              
            
            //$node->geopoints = $node_geopoints;
            /*
              if (sizeof($node_geopoints) > 1) {
              $node->geopoints = $node_geopoints;
              } else {
              if (isset($node_geopoints[0]->route))
              $node->route = $node_geopoints[0]->route;
              if (isset($node_geopoints[0]->street_number))
              $node->street_number = $node_geopoints[0]->street_number;
              if (isset($node_geopoints[0]->locality))
              $node->locality = $node_geopoints[0]->locality;
              if (isset($node_geopoints[0]->administrative_area_level_3))
              $node->district = $node_geopoints[0]->administrative_area_level_3;
              if (isset($node_geopoints[0]->administrative_area_level_2))
              $node->town = $node_geopoints[0]->administrative_area_level_2;
              if (isset($node_geopoints[0]->administrative_area_level_2_label))
              $node->town_label = $node_geopoints[0]->administrative_area_level_2_label;
              if (isset($node_geopoints[0]->administrative_area_level_1))
              $node->region = $node_geopoints[0]->administrative_area_level_1;
              if (isset($node_geopoints[0]->country))
              $node->country = $node_geopoints[0]->country;
              }
             */
        }
    }

    protected function _node_tags(&$node) {
        if (isset($node->object_tags)) {
            foreach ($node->object_tags as $object_tag) {
                $tag_name = $object_tag->name;
                $tags = $this->NodeModel->get_tagsids_by_type($object_tag->id);
                if ($tags)
                    $node_tags = $this->NodeModel->get_node_tagsids($node->id, implode(',', $tags));
                if ($node_tags)
                    $node->$tag_name = $node_tags;
            }
        }
    }

    protected function _get_object_parents(&$node, $active = FALSE) {
        $object_parents = $this->NodeModel->get_object_parents($node->type->object);
        if ($object_parents) {
            foreach ($object_parents as $object_parent) {
                echo '_get_object_parents';
                echo '<pre>';
                print_r($object_parents);
                die;
            }
        }
    }

    protected function _get_node_parents(&$node, $active = FALSE) {

        $object_parents = $this->NodeModel->get_object_parents($node->type->object);
        if ($object_parents) {
            foreach ($object_parents as $object_parent) {
                $parent_table_name = $object_parent->parent_table;
                if (isset($object_parent->relation_table)) {
                    $found = $this->NodeModel->get_relation_parents($object_parent, $node->id, $active);
                    if ($found){
                        
                        //echo '$parent_table_name: '.$parent_table_name.' tot:'.sizeof($found).'<br />';
/*
                        if(sizeof($found)>1){
                            $node->$parent_table_name = $found;
                        }else{
                            $node->$parent_table_name = $found[0];
                        }                         
*/
                        $node->$parent_table_name = $found; 
                        
                    }
                }else {
                    $found = $this->NodeModel->get_direct_relation_parents($object_parent, $node->id, $active);
                    if ($found){
                        
                        //echo '$parent_table_name: '.$parent_table_name.' tot:'.sizeof($found).'<br />';
/*
                        if(sizeof($found)>1){
                            $node->$parent_table_name = $found;
                        }else{
                            $node->$parent_table_name = $found[0];
                        }                         
*/
                        $node->$parent_table_name = $found;                       
                    }
                }
            }
        }
    }

    protected function _get_node_children(&$node, $active = FALSE) {

        $object_children = $this->NodeModel->get_object_children($node->type->object);
        if ($object_children) {
            foreach ($object_children as $object_child) {
                $child_table_name = $object_child->child_table;
                if (isset($object_child->relation_table)) {
                    $found = $this->NodeModel->get_relation_children($object_child, $node->id, $active);
                    if ($found){   
                        
                        //echo '$child_table_name: '.$child_table_name.' tot:'.sizeof($found).'<br />';
/*                        
                        if(sizeof($found)>1){
                            $node->$child_table_name = $found;
                        }else{
                            $node->$child_table_name = $found[0];
                        }
*/                        
                        
                        $node->$child_table_name = $found;
                        
                    }
                }else {
                    $found = $this->NodeModel->get_direct_relation_children($object_child, $node->id, $active);
                    if ($found){
                        
                        //echo '$child_table_name: '.$child_table_name.' tot:'.sizeof($found).'<br />';
/*                        
                        if(sizeof($found)>1){
                            $node->$child_table_name = $found;
                        }else{
                            $node->$child_table_name = $found[0];
                        }
*/                        
                        
                        $node->$child_table_name = $found;
                    }
                }
            }
        }
    }

    protected function _get_roles(&$node) {
        $roles = $this->RoleModel->get_user_roles($node->id);
        /*
        if(sizeof($roles)>1){
            $node->roles = $roles;
        }else{
            $node->roles = $roles[0];
        }
        */
        $node->roles = $roles;
    }

    protected function _get_dates(&$node) {
        $this->load->model('Restaurant_Model', 'RestaurantModel');
        $dates = $this->RestaurantModel->get_dates($node->id);
        if ($dates)
            $node->dates = $dates;
    }
    
    protected function _get_event_dates(&$node) {
        $this->load->model('Event_Model', 'EventModel');
        $dates = $this->EventModel->get_dates($node->id);
        if ($dates) $node->dates = $dates;
    }    

    protected function _get_days(&$node) {
        $days = $this->RestaurantModel->get_days($node->id);
        if ($days)
            $node->days = $days;
    }

    protected function _get_event_days(&$node) {
        $this->load->model('Event_Model', 'EventModel');
        $days = $this->EventModel->get_days($node->id);
        if ($days)
            $node->days = $days;
    }    
    
    protected function _get_dish_types(&$node) {
        $this->load->model('Tag_Model', 'TagModel');
        $dish_types = $this->TagModel->get_node_tags_by_type(20);
        if ($dish_types)
            $node->dish_types = $dish_types;
    }

    protected function _get_restaurant_dishes(&$node) {
        $this->load->model('Restaurant_Model', 'RestaurantModel');
        $dishes = $this->RestaurantModel->get_restaurants_dishes($node->id);
        if ($dishes)
            $node->dishes = $dishes;
    }

    protected function _get_dish_restaurant(&$node) {
        $this->load->model('Dish_Model', 'DishModel');
        $restaurant_id = $this->DishModel->get_dish_restaurant($node->id);
        if ($restaurant_id) {
            $restaurant = $this->_get($restaurant_id);
            $node->restaurant = $restaurant;
        }
    }

    protected function _get_event_menus(&$node) {
        $this->load->model('Event_Model', 'EventModel');
        $menus = $this->EventModel->get_event_menus($node->id);
        if ($menus)
            $node->menus = $menus;
    }

    protected function _get_node_tags_chosen_options(&$node) {

        if (isset($node->object_tags)) {
            foreach ($node->object_tags as $object_tag) {
                $this->load->model('Tag_Model', 'TagModel');
                $this->viewdata['tags'][$object_tag->name]['id'] = $object_tag->id;
                $this->viewdata['tags'][$object_tag->name]['name'] = $object_tag->name;
                $this->viewdata['tags'][$object_tag->name]['description'] = $object_tag->description;
                $tags = $this->TagModel->get_tag_by_id($object_tag->id);
                if ($tags) {

                    foreach ($tags as $tag)
                        $this->viewdata['tags'][$object_tag->name]['options'][$tag->id . '-' . $tag->name] = $tag->name;
                    if (isset($node->id)) {
                        $node_tags = $this->TagModel->get_node_tags_by_id($node->id, $object_tag->id);
                        if ($node_tags)
                            foreach ($node_tags as $node_tag)
                                $this->viewdata['tags'][$object_tag->name]['selected'][] = $node_tag->node_id . '-' . $node_tag->name;
                    }
                }
            }
        }
    }

    protected function _set_node_tags_chosen_options(&$node) {

        if (isset($node->object_tags)) {
            foreach ($node->object_tags as $tag) {
                if (isset($this->postrequest->$tag_name)) {
                    $tags = $this->utils->chosenResult($this->postrequest->$tag_name);
                    $this->viewdata['tags'][$tag_name] = $this->utils->chosenOptions($tags);
                }
            }
        }
    }

    protected function _set_tags($nid) {

        $node = $this->_get($nid, TRUE);

        if (isset($node->object_tags)) {
            foreach ($node->object_tags as $tag) {
                $tag_name = $tag->name;
                if (isset($this->postrequest->$tag_name)) {
                    $this->TagModel->delete_node_tags_by_tagid($nid, $tag->id);
                    $tags = $this->utils->chosenResult($this->postrequest->$tag_name);
                    foreach ($tags as $t) {
                        $node_tag = array('node_id' => $nid, 'tag_id' => $t);
                        $this->TagModel->insert_node_nodes_tags(array('node_id' => $nid, 'tag_id' => $t));
                    }
                }
            }
        }
    }

    protected function _create($type_id, $uid = TRUE) {

        //if(!isset($uid)) $owner_id=NULL;
        //$owner_id = ($uid) ? $this->usersession->id : NULL;
        $new_node = array(
            'name' => $this->postrequest->name,
            'owner_id' => $owner_id = (!$uid) ? NULL : $uid,
            'type_id' => $type_id,
            'created' => $this->utils->dateLong(),
            'active' => 1
        );
        
        $new_node = array_filter($new_node);
        //$this->utils->dump($new_node);die;
        return $this->NodeModel->insert($new_node);
    }

    protected function _create_geopoint($address) {
        //CREA NODO
        $new_node = array(
            'name' => $address->formatted_address,
            'type_id' => 7,
            'created' => $this->utils->dateLong(),
            'latitude' => $address->latitude,
            'longitude' => $address->longitude,
            'address' => $address->formatted_address,
            'active' => 1
        );
        $new_node_id = $this->NodeModel->insert($new_node);
        //CREA NODO GEOPOINT
        $new_geopoint = $address;
        $new_geopoint->node_id = $new_node_id;
        $this->load->model('Geopoint_Model', 'GeopointModel');
        $this->GeopointModel->insert($new_geopoint);

        return $new_node_id;
    }

    protected function _set_geopoint($nid, $update = FALSE) {
        if (isset($this->postrequest->addressinfo) AND strlen($this->postrequest->addressinfo) > 0) {
            $new_address = $this->utils->formatGoogleAddress($this->postrequest->addressinfo);
            $geopoint_id = $this->_create_geopoint($new_address);
            $this->GeopointModel->delete_node_geopoint($nid);
            $this->GeopointModel->insert_node_geopoint(array('node_id' => $nid, 'geopoint_id' => $geopoint_id));
            $update_node_geoinfo = array(
                'id' => $nid,
                'latitude' => $new_address->latitude,
                'longitude' => $new_address->longitude,
                'address' => $new_address->formatted_address,
                'updated' => $this->utils->dateLong(),
            );
            $this->NodeModel->update($update_node_geoinfo);
        }
    }
    
    protected function _get_node_media(&$node){
        $media = $this->NodeModel->get_node_media($node->id);
        if($media){
            /*
            if(sizeof($media)>1){
                $node->media = $media;
            }else{
                $node->media = $media[0];
            }            
            */
            $node->media = $media;
        }
    }
    
    protected function _get_geopoint_nodes(&$node){
        $nodes = $this->GeopointModel->get_geopoint_nodes($node->id);
        if($nodes){
            
            if(sizeof($nodes)>1){
                $node->nodes = $nodes;
            }else{
                $node->nodes = $nodes[0];
            }            
            //$node->nodes = $nodes;
        }        
    }

    protected function _get($nid, $recursive = FALSE) {

        $nodeobj = $this->NodeModel->get_node($nid);

        if (!$nodeobj) {
            echo 'ko';
            die;
        }
        $this->_type($nodeobj);
        $this->_object_data($nodeobj);
        $this->_node_geopoints($nodeobj);
        if ($recursive) {
            $this->_object_tags($nodeobj);
            $this->_node_tags($nodeobj);
            $this->_get_node_children($nodeobj);
            $this->_get_node_parents($nodeobj);
            $this->_get_node_media($nodeobj);
            
            //GEOPOINT
            if ($nodeobj->type->id == 7) {
                //GET RELATED NODES
                $this->_get_geopoint_nodes($nodeobj);                
            //USER
            }else if ($nodeobj->type->id == 8) {

                $this->_get_roles($nodeobj);
                foreach ($nodeobj->roles as $role) {
                    //GET AGENT OWNER
                    if (in_array($role->id, array(4))) {
                        $this->load->model('Agent_Model', 'AgentModel');
                        $owners = $this->AgentModel->owners($nodeobj->id);
                        if ($owners)
                            $nodeobj->owners = $owners;
                    }
                    //GET OWNER AGENTS
                    if (in_array($role->id, array(3))) {
                        //echo 'owner';
                        $this->load->model('Agent_Model', 'AgentModel');
                        $agents = $this->AgentModel->agents($nodeobj->id);
                        if ($agents)
                            $nodeobj->agents = $agents;

                    }
                }
            }
            else if ($nodeobj->type->id == 1) {
                //RESTAURANT
                $this->_get_dates($nodeobj);
                $this->_get_days($nodeobj);
                $this->_get_restaurant_dishes($nodeobj);
            } else if ($nodeobj->type->id == 3) {
                //DISH
                //$this->_get_dish_types($nodeobj);
                $this->_get_dish_restaurant($nodeobj);
            //IF EVENT
            }else if ($nodeobj->type->id = 2) {
                $this->_get_event_menus($nodeobj);
                $this->_get_event_days($nodeobj);
                $this->_get_event_dates($nodeobj);
            }
        }
        $this->_clean_obj($nodeobj);
        
        //
        unset($nodeobj->node_id);
        //NEW OBJ
        $node = $nodeobj;
        $node->type = $nodeobj->type->object;
        return $node;
    }

    public function delete($nid) {
        $this->NodeModel->delete($nid);
        $this->session->set_flashdata('success', $this->lang->line('removal_success'));
        redirect($_SERVER['HTTP_REFERER'], 'refresh');
    }

    public function activate($nid) {
        $this->NodeModel->update(array('id' => $nid, 'active' => 1));
        $this->session->set_flashdata('success', $this->lang->line('activate_success'));
        redirect($_SERVER['HTTP_REFERER'], 'refresh');
    }

    public function deactivate($nid) {
        $this->NodeModel->update(array('id' => $nid, 'active' => 0));
        $this->session->set_flashdata('success', $this->lang->line('deactivate_success'));
        redirect($_SERVER['HTTP_REFERER'], 'refresh');
    }

    /* FILES/IMAGES */

    protected function _user_dirs($nid) {

        $user_dirs = $this->config->item('user_dirs_path');
        if (!file_exists($user_dirs))
            $this->utils->make_dir($user_dirs);

        $user_dirs_path = $this->config->item('user_dirs_path') . $nid;
        if (!file_exists($user_dirs_path))
            $this->utils->make_dir($user_dirs_path);
        foreach ($this->config->item('user_dirs') as $user_dir) {
            if (!file_exists($user_dirs_path . $user_dir))
                $this->utils->make_dir($user_dirs_path . $user_dir);
        }
    }

    public function _avatar($nid) {

        if (isset($this->filesrequest->userfile) AND strlen($this->filesrequest->userfile->name) > 0) {
            $avatar = $this->_image_upload($this->config->item('user_dirs_path') . $nid . '/avatar');
            $this->UserModel->update(array('node_id' => $nid, 'avatar' => $this->config->item('user_dirs_url') . '/' . $nid . '/avatar/' . $avatar));
        }
    }

    protected function _restaurant_dirs($nid) {

        $restaurant_dirs = $this->config->item('restaurant_dirs_path');
        if (!file_exists($restaurant_dirs))
            $this->utils->make_dir($restaurant_dirs);

        $restaurant_dirs_path = $this->config->item('restaurant_dirs_path') . $nid;
        if (!file_exists($restaurant_dirs_path))
            $this->utils->make_dir($restaurant_dirs_path);
        foreach ($this->config->item('restaurant_dirs') as $restaurant_dir) {
            if (!file_exists($restaurant_dirs_path . $restaurant_dir))
                $this->utils->make_dir($restaurant_dirs_path . $restaurant_dir);
        }
    }

    public function _logo($nid) {
        if (isset($this->filesrequest->userfile) AND strlen($this->filesrequest->userfile->name) > 0) {
            $logo = $this->_image_upload($this->config->item('restaurant_dirs_path') . $nid . '/logo');
            $this->RestaurantModel->update(array('node_id' => $nid, 'logo' => $this->config->item('restaurant_dirs_url') . '/' . $nid . '/logo/' . $logo));
        }
    }

    protected function _dish_dirs($nid) {

        $dish_dirs = $this->config->item('dish_dirs_path');
        if (!file_exists($dish_dirs))
            $this->utils->make_dir($dish_dirs);

        $dish_dirs_path = $this->config->item('dish_dirs_path') . $nid;
        if (!file_exists($dish_dirs_path))
            $this->utils->make_dir($dish_dirs_path);
        foreach ($this->config->item('dish_dirs') as $dish_dir) {
            if (!file_exists($dish_dirs_path . $dish_dir))
                $this->utils->make_dir($dish_dirs_path . $dish_dir);
        }
    }

    public function _dishlogo($nid) {

        if (isset($this->filesrequest->userfile) AND strlen($this->filesrequest->userfile->name) > 0) {
            $logo = $this->_image_upload($this->config->item('dish_dirs_path') . $nid . '/logo');
            $this->DishModel->update(array('node_id' => $nid, 'logo' => $this->config->item('dish_dirs_url') . '/' . $nid . '/logo/' . $logo));
        }
    }

    protected function _event_dirs($nid) {

        $event_dirs = $this->config->item('event_dirs_path');
        if (!file_exists($event_dirs))
            $this->utils->make_dir($event_dirs);

        $event_dirs_path = $this->config->item('event_dirs_path') . $nid;
        if (!file_exists($event_dirs_path))
            $this->utils->make_dir($event_dirs_path);
        foreach ($this->config->item('event_dirs') as $event_dir) {
            if (!file_exists($event_dirs_path . $event_dir))
                $this->utils->make_dir($event_dirs_path . $event_dir);
        }
    }

    public function _eventlogo($nid) {

        if (isset($this->filesrequest->userfile) AND strlen($this->filesrequest->userfile->name) > 0) {
            $logo = $this->_image_upload($this->config->item('event_dirs_path') . $nid . '/logo');
            $this->EventModel->update(array('node_id' => $nid, 'logo' => $this->config->item('event_dirs_url') . '/' . $nid . '/logo/' . $logo));
        }
    }

    protected function _image_upload($path, $resize = TRUE) {



        $this->load->config('image');

        $config['upload_path'] = $path; //. '/';
        $config['allowed_types'] = $this->config->item('allowed_formats'); // 'gif|jpg|png';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['encrypt_name'] = TRUE;
        $config['overwrite'] = FALSE;



        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            echo '1: '.$this->upload->display_errors();
            die;
            //$this->_set_error(array('error_code' => 'INTERNAL_SERVER_ERROR', 'error_msg' => strip_tags($this->upload->display_errors())));
        }
        $image = $this->upload->data();





        if ($resize) {

                list($width, $height, $type, $attr) = getimagesize($image['full_path']);
                if ($this->config->item('max_width') < $width && $width >= $height) {
                      $thumbwidth = $this->config->item('max_width');
                      $thumbheight = ($thumbwidth / $width) * $height;
                } elseif ($this->config->item('max_height') < $height && $height >= $width) {
                      $thumbheight = $this->config->item('max_height');
                      $thumbwidth = ($thumbheight /$height) * $width;
                }else{
                      $thumbheight = $height;
                      $thumbwidth = $width;
                }            
            
            //if ($width > $this->config->item('max_width') OR $height > $this->config->item('max_height') OR ($width * $height) > ($this->config->item('max_width') * $this->config->item('max_height'))) {

                $resize_config['image_library'] = 'gd2';
                $resize_config['source_image'] = $image['full_path'];
                $resize_config['create_thumb'] = FALSE;
                $resize_config['maintain_ratio'] = TRUE;
                $resize_config['width'] = $thumbwidth;//$this->config->item('max_width');
                $resize_config['height'] = $thumbheight;//$this->config->item('max_height');

                $this->load->library('image_lib', $resize_config);
                if (!$this->image_lib->resize()) {
                    echo '2: '.$this->image_lib->display_errors();
                    die;
                    //$this->_set_error(array('error_code' => 'INTERNAL_SERVER_ERROR', 'error_msg' => strip_tags($this->image_lib->display_errors())));
                }
            //}
        }

        return $image['file_name'];
    }
    
    protected function _set_gallery_image($nid,$gallery_path,$image,$baseurl){
        
            $gallery_image = $gallery_path.$image;
            $imageinfo = get_file_info($gallery_image);            
            //CREA NODO
            $new_nid = $this->_create(26);  
            $new_image=array(
                'node_id'=>$new_nid,
                'name' => $name = (isset($this->postrequest->name)) ? strip_tags($this->postrequest->name) : NULL,
                'desc' => $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc) : NULL,
                'type_id'=>26,
                'url'=>$baseurl.'/'.$nid.'/gallery/'.$image,
                'src'=>'/'.str_replace($_SERVER['DOCUMENT_ROOT'],'',$gallery_path).$imageinfo['name'],//$gallery_image,                
                'alt'=> $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc) : NULL,
                'ext'=>end(explode('.',$image)),
                'width'=>$this->config->item('max_width'),
                'height'=>$this->config->item('max_height'),
                'size'=>$imageinfo['size'],                
            );  
            $new_image = array_filter($new_image);  
            $this->load->model('Media_Model','MediaModel');
            $this->MediaModel->insert($new_image);
            $this->MediaModel->insert_node_media(array('node_id'=>$nid,'media_id'=>$new_nid));
        
    }    

    public function testmail() {

        $this->load->config('mail');
        require_once($_SERVER['DOCUMENT_ROOT'] . '/' . APPPATH . 'libraries/phpmailer/class.phpmailer.php');
        $this->phpmailer = new PHPMailer();
        $this->phpmailer->IsSMTP();
        $this->phpmailer->Host = $this->config->item('smtp_server');
        $this->phpmailer->SMTPDebug = false;
        $this->phpmailer->SMTPAuth = true;
        $this->phpmailer->Host = $this->config->item('smtp_server');
        $this->phpmailer->Port = $this->config->item('smtp_port');
        $this->phpmailer->Username = $this->config->item('smtp_user');
        $this->phpmailer->Password = $this->config->item('smtp_pwd');

        $this->phpmailer->SetFrom($this->config->item('smtp_sender_mail'), $this->config->item('smtp_sender_name'));
        $this->phpmailer->AddAddress('m.skuk@fingerlinks.org', 'm.skuk');

        $this->phpmailer->Subject = "Test Subject " . date('H:i:s', time());
        $this->phpmailer->AltBody = "Test AltBody";
        $this->phpmailer->MsgHTML('<h1>Test Body</h1>');

        if (!$this->phpmailer->Send()) {
            echo "Mailer Error: " . $this->phpmailer->ErrorInfo;
        } else {
            echo "Message sent!";
        }
    }   
    
    public function _send_mail($data){
            
            //$this->utils->dump($data);die;                
            $this->load->config('mail');
            require_once($_SERVER['DOCUMENT_ROOT'] . '/' . APPPATH . 'libraries/phpmailer/class.phpmailer.php');
            $this->phpmailer = new PHPMailer();
            $this->phpmailer->IsSMTP();
            $this->phpmailer->Host = $this->config->item('smtp_server');
            $this->phpmailer->SMTPDebug = false;
            $this->phpmailer->SMTPAuth = true;
            $this->phpmailer->Host = $this->config->item('smtp_server');
            $this->phpmailer->Port = $this->config->item('smtp_port');
            $this->phpmailer->Username = $this->config->item('smtp_user');
            $this->phpmailer->Password = $this->config->item('smtp_pwd');
            $this->phpmailer->SetFrom($this->config->item('smtp_sender_mail'), $this->config->item('smtp_sender_name'));
            $this->phpmailer->AddAddress($data['user']->usermail,$data['user']->username);
            $this->phpmailer->Subject = $data['subject'];
            $this->phpmailer->AltBody = $data['altbody'];
            $this->phpmailer->MsgHTML($data['htmlbody']);
            if($this->phpmailer->Send()) return TRUE;
            return FALSE;
            /*
            if (!$this->phpmailer->Send()) {
                echo "Mailer Error: " . $this->phpmailer->ErrorInfo;
            } else {
                echo "Message sent!";
            }         
            */
    }    
    
    protected function _secured(){
                
        if (!isset($this->httpheaders['App-Key']))
            $this->utils->jsonError(array('error_code' => 'ERROR', 'error_msg' => 'app_key_missing'));
        if (strcmp(trim($this->httpheaders['App-Key']), trim($this->config->item('app_key'))) !== 0)
            $this->utils->jsonError(array('error_code' => 'ERROR', 'error_msg' => 'app_key_invalid'));        
    }
    
}