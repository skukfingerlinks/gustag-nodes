<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main_Model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->debug = false;
        $this->dbname = $this->db->database;
        $this->dbprefix = $this->db->dbprefix;        
    }
    
    public function showSql($sql){
        
        if(preg_match("/DELETE/i", $sql) OR preg_match("/INSERT/i", $sql) OR preg_match("/update/i", $sql)) echo '<pre>'.$sql.'</pre>';
    }
    
    public function getRow($sql){
        $success = TRUE;
        $query = $this->db->query($sql);
        if ($this->debug) $this->showSql($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        }else{            
            return FALSE;         
        }
        
    }
    public function getRows($sql){
        $query = $this->db->query($sql);
        if ($this->debug) $this->showSql($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        }else{
            return FALSE;                     
        }
        
    }  
    public function countRows($sql){
        $query = $this->db->query($sql);
        if ($this->debug) $this->showSql($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return FALSE;         
            
        }        
    }  
    public function executeSql($sql){
        if ($this->db->query($sql)) return TRUE;
        if ($this->debug) $this->showSql($sql);
        return FALSE;         
    }
    
}