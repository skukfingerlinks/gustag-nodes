<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_Model extends Main_Model {
    
    public function __construct() {
        parent::__construct(); 
    }
    
    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'menus', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'menus', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'menus', array('node_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }    
    
    public function countall($uid=FALSE,$active=TRUE){
        $sql="
        SELECT COUNT(n.id) AS total 
        FROM ".$this->dbname.'.'.$this->dbprefix."menus m
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = m.node_id 
        WHERE 1=1 ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        $sql.="#".$this->router->fetch_class().' - '.$this->router->fetch_method();
        return $this->getRow($sql)->total;
    }    
    
    public function all($uid=FALSE,$active=TRUE,$limit=999,$offset=0){
        $sql="
        SELECT n . * 
        FROM ci_menus m
        JOIN ci_nodes n ON n.id = m.node_id 
        WHERE 1=1 ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        if(isset($limit) and isset($offset)) $sql.=" LIMIT $offset,$limit";
        $sql.="#".$this->router->fetch_class().' - '.$this->router->fetch_method();
        return $this->getRows($sql);
    }
    
    public function restaurant_countall($nid=FALSE,$active=TRUE){
        $sql="
        SELECT COUNT(n.id) AS total 
        FROM ".$this->dbname.'.'.$this->dbprefix."menus m
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = m.node_id 
        WHERE 1=1 ";
        if($nid) $sql.="AND m.restaurant_id =$nid ";
        if($active) $sql.="AND n.active=1 ";
        $sql.="#".$this->router->fetch_class().' - '.$this->router->fetch_method();
        return $this->getRow($sql)->total;
    }    
    
    public function restaurant_all($nid=FALSE,$active=TRUE,$limit=999,$offset=0){
        $sql="
        SELECT n . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."menus m
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = m.node_id 
        WHERE 1=1 ";
        if($nid) $sql.="AND m.restaurant_id =$nid ";
        if($active) $sql.="AND n.active=1 ";
        if(isset($limit) and isset($offset)) $sql.=" LIMIT $offset,$limit";
        $sql.="#".$this->router->fetch_class().' - '.$this->router->fetch_method();        
        return $this->getRows($sql);
    }
    
    public function event_countall($nid=FALSE,$active=TRUE){
        $sql="
        SELECT COUNT(n.id) AS total 
        FROM ".$this->dbname.'.'.$this->dbprefix."menus m
        JOIN ".$this->dbname.'.'.$this->dbprefix."events_menus em ON em.menu_id = m.node_id 
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = m.node_id 
        WHERE 1=1 ";
        if($nid) $sql.="AND em.event_id =$nid ";
        if($active) $sql.="AND n.active=1 ";
        $sql.="#".$this->router->fetch_class().' - '.$this->router->fetch_method();
        return $this->getRow($sql)->total;
    }    
    
    public function event_all($nid=FALSE,$active=TRUE,$limit=999,$offset=0){
        $sql="
        SELECT n . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."menus m
        JOIN ".$this->dbname.'.'.$this->dbprefix."events_menus em ON em.menu_id = m.node_id 
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = m.node_id 
        WHERE 1=1 ";
        if($nid) $sql.="AND em.event_id =$nid ";
        if($active) $sql.="AND n.active=1 ";
        if(isset($limit) and isset($offset)) $sql.=" LIMIT $offset,$limit";
        $sql.="#".$this->router->fetch_class().' - '.$this->router->fetch_method();
        return $this->getRows($sql);
    }    
    
    public function insert_days($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'menus_days', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function get_days($nid) {
        $sql="SELECT day FROM ".$this->dbname.'.'.$this->dbprefix."menus_days WHERE menu_id=$nid";
        return $this->getRows($sql);
    }    
    
    public function update_days($data) {
        
        $this->db->where('menu_id', $data['menu_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'menus_days', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_days($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'menus_days', array('menu_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    } 
    
    public function insert_dishes($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'menus_dishes', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update_dishes($data) {
        
        $this->db->where('menu_id', $data['menu_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'menus_dishes', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_dishes($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'menus_dishes', array('menu_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }    
    
}    