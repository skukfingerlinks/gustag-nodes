<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agent_Model extends Main_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        if ($this->db->insert($this->dbname . '.' . $this->dbprefix . 'owners_agents', $data)) {
            if ($this->debug)
                $this->showSql($this->db->last_query());
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname . '.' . $this->dbprefix . 'owners_agents', $data)) {
            if ($this->debug)
                $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function delete_agents($nid) {
        if ($this->db->delete($this->dbname . '.' . $this->dbprefix . 'owners_agents', array('node_id' => $nid))) {
            if ($this->debug)
                $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }
    public function delete_owners($nid) {
        if ($this->db->delete($this->dbname . '.' . $this->dbprefix . 'owners_agents', array('node_id' => $nid))) {
            if ($this->debug)
                $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }    

//    public function all() {
//        $sql = "SELECT oa.* FROM n_owners_agents oa";
//        echo '<pre>' . $sql . '</pre>';
//        return $this->getRows($sql);
//    }


    public function countall($uid=FALSE,$active=TRUE){

        $sql="
        SELECT COUNT(u.node_id) AS total 
        FROM ".$this->dbname.'.'.$this->dbprefix."users u
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = u.node_id
        JOIN ".$this->dbname.'.'.$this->dbprefix."owners_agents oa ON oa.owner_id = u.node_id
        WHERE oa.agent_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        return $this->getRow($sql)->total;
    }      
    
    public function all($uid=FALSE,$active=TRUE,$limit=999,$offset=0){

        $sql="
        SELECT u . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."users u
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = u.node_id
        JOIN ".$this->dbname.'.'.$this->dbprefix."owners_agents oa ON oa.owner_id = u.node_id
        WHERE oa.agent_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        if(isset($limit) and isset($offset)) $sql.=" LIMIT $offset,$limit";
        return $this->getRows($sql);
    }    
    
    
    public function owners($agent_nid = TRUE,$active=TRUE) {

        $sql="
        SELECT u . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."users u
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = u.node_id
        JOIN ".$this->dbname.'.'.$this->dbprefix."owners_agents oa ON oa.owner_id = u.node_id
        WHERE oa.agent_id =$agent_nid ";
        if($active) $sql.=" AND n.active =1";
        return $this->getRows($sql);
    }

    public function agents($owner_nid = TRUE,$active=TRUE) {

        $sql="
        SELECT u . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."users u
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = u.node_id
        JOIN ".$this->dbname.'.'.$this->dbprefix."owners_agents oa ON oa.agent_id = u.node_id
        WHERE oa.owner_id =$owner_nid ";
        if($active) $sql.=" AND n.active =1";
        //echo '<pre>'.$sql.'</pre>';
        return $this->getRows($sql);
    }    
    
    public function owner($nid) {

        $sql = "
        SELECT u.*
        FROM ".$this->dbname.'.'.$this->dbprefix."users u
        JOIN ".$this->dbname.'.'.$this->dbprefix."owners_agents oa ON oa.owner_id = u.node_id AND oa.agent_id =$nid";
        echo '<pre>' . $sql . '</pre>';
        return $this->getRows($sql);
    }

    public function agent($nid) {

        $sql = "
        SELECT u.*
        FROM ".$this->dbname.'.'.$this->dbprefix."users u
        JOIN ".$this->dbname.'.'.$this->dbprefix."owners_agents oa ON oa.agent_id = u.node_id AND oa.owner_id =$nid";
        echo '<pre>' . $sql . '</pre>';
        return $this->getRows($sql);
    }

    /*
      public function agents($nid){

      $sql="
      SELECT u . *
      FROM n_users u
      JOIN n_owners_agents oa ON oa.agent_id = u.node_id
      AND oa.agent_id =$nid
      ";
      echo '<pre>'.$sql.'</pre>';
      return $this->getRows($sql);
      }
     */

    public function user_roles($role) {
        $sql = "
        SELECT u.*
        FROM " . $this->dbname . '.' . $this->dbprefix . "users u
        JOIN " . $this->dbname . '.' . $this->dbprefix . "nodes n ON n.id = u.node_id 
        JOIN " . $this->dbname . '.' . $this->dbprefix . "users_roles ur ON ur.user_id=u.node_id AND ur.role_id=$role
        ORDER BY n.active";
        echo '<pre>'.$sql.'</pre>';die;
        return $this->getRows($sql);
    }

}

