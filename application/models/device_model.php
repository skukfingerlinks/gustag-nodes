<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Device_Model extends Main_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function isunique($col,$val){        
        $sql="SELECT COUNT(d.node_id) AS total FROM ".$this->dbname.'.'.$this->dbprefix."devices d WHERE $col='$val';";
        //echo '<pre>'.$sql.'</pre>';
        $total= $this->getRow($sql)->total;
        if($total<1) return TRUE;
        return FALSE;
    }
    
    public function getidby($col,$val){        
        $sql="SELECT d.node_id FROM ".$this->dbname.'.'.$this->dbprefix."devices d WHERE $col='$val';";
        //echo '<pre>'.$sql.'</pre>';
        return $this->getRow($sql)->node_id;;        
    }    
    
    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'devices', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'devices', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'devices', array('node_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }  
    
    public function get_user_devices($nid,$active=FALSE,$device_id=FALSE){
        
        $sql="
        SELECT d.* 
        FROM ".$this->dbname.'.'.$this->dbprefix."devices d ";
        if($active) $sql.=" JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = d.node_id AND n.active =1 ";
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."users_devices ud ON ud.device_id = d.node_id ";
        $sql.="WHERE ud.user_id =$nid ";
        if($device_id) $sql.=" AND d.device_id =  '$device_id' ";
        //echo '<pre>'.$sql.'</pre>';
        if($device_id) return $this->getRow($sql);
        return $this->getRows($sql);
    }
    
    
    public function insert_users_devices($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'users_devices', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update_users_devices($data) {
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'users_devices', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_users_devices($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'users_devices', array('node_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }    
    
}    