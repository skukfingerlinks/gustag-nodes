<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Event_Model extends Main_Model {
    
    public function __construct() {
        parent::__construct(); 
    }
    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'events', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'events', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'events', array('node_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }     
    public function countall($uid=FALSE,$active=FALSE){
        $sql="
        SELECT COUNT(n.id) AS total 
        FROM ".$this->dbname.'.'.$this->dbprefix."menus m
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = m.node_id 
        WHERE 1=1 ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        $sql.="#Event_Model:countall";
        return $this->getRow($sql)->total;
    }    
    
    public function all($uid=FALSE,$active=FALSE,$limit=999,$offset=0){
        $sql="
        SELECT n . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."menus m
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = m.node_id 
        WHERE 1=1 ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        if(isset($limit) and isset($offset)) $sql.=" LIMIT $offset,$limit";
        $sql.="#Event_Model:all";
        echo '<pre>'.$sql.'</pre>';
        return $this->getRows($sql);
    }    
    
    public function restaurant_countall($nid=FALSE,$active=FALSE){
        $sql="
        SELECT COUNT(n.id) AS total 
        FROM ".$this->dbname.'.'.$this->dbprefix."events e
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = e.node_id 
        WHERE 1=1 ";
        if($nid) $sql.="AND e.restaurant_id =$nid ";
        if($active) $sql.="AND n.active=1 ";
        $sql.="#Event_Model:restaurant_countall";
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRow($sql)->total;
    }    
    
    public function restaurant_all($nid=FALSE,$active=FALSE,$limit=999,$offset=0){
        $sql="
        SELECT n . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."events e
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = e.node_id 
        WHERE 1=1 ";
        if($nid) $sql.="AND e.restaurant_id =$nid ";
        if($active) $sql.="AND n.active=1 ";
        if(isset($limit) and isset($offset)) $sql.=" LIMIT $offset,$limit";
        $sql.="#Event_Model:restaurant_all";
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRows($sql);
    }    
    
    public function get_dishes($nid,$onlyids=FALSE){
        //SELECT d . * , md.menu_id, m.restaurant_id
        $sql="
        SELECT d. * 
        FROM ".$this->dbname.'.'.$this->dbprefix."dishes d
        JOIN ".$this->dbname.'.'.$this->dbprefix."menus_dishes md ON md.dish_id = d.node_id
        JOIN ".$this->dbname.'.'.$this->dbprefix."events_menus em ON em.menu_id = md.menu_id
        WHERE em.event_id =$nid           
        ";
        $dishes = $this->getRows($sql);
        if($onlyids){
            foreach($dishes as $dish) $res[]=$dish->node_id;
            return $res;
        }else{
            return $dishes;
        }
    }    
    
    public function check_date($nid,$start,$end) {

        $sql = "SELECT COUNT(event_id) AS total FROM ".$this->dbname.'.'.$this->dbprefix."events_dates WHERE event_id=$nid AND start=$start AND end=$end";
        $res = $this->getRow($sql)->total;
        if($res<1)return TRUE;
        return FALSE;
    }      
    
    public function get_dates($nid) {

        $res=array();
        $sql = "SELECT start,end FROM ".$this->dbname.'.'.$this->dbprefix."events_dates WHERE event_id=$nid";
        $dates = $this->getRows($sql);
        if($dates) foreach($dates as $date) $res[]=$date;
        return $res;
    }     
    
    public function insert_dates($data) {
        if ($this->db->insert($this->dbprefix.'events_dates', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function delete_dates($nid) {
        if ($this->db->delete($this->dbprefix.'events_dates', array('event_id' => $nid))) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function get_event_menus($nid,$active=FALSE) {
        
        $sql="
        SELECT m . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."menus m
        JOIN ".$this->dbname.'.'.$this->dbprefix."events_menus em ON em.menu_id = m.node_id AND em.event_id =$nid
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = m.node_id
        WHERE 1 =1 ";
        $sql.=" AND n.active =1 ";
        return $this->getRows($sql);
    }    
    
    public function insert_event_menus($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'events_menus', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update_event_menus($data) {
        
        $this->db->where('menu_id', $data['menu_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'events_menus', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_event_menus($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'events_menus', array('event_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }    
    
    public function get_days($nid) {

        $res=array();
        $sql = "SELECT day FROM ".$this->dbname.'.'.$this->dbprefix."events_days WHERE event_id=$nid";        
        $dates = $this->getRows($sql);
        if($dates) foreach($dates as $date) $res[]=$date->day;
        return $res;
    }     
    
    public function insert_days($data) {
        if ($this->db->insert($this->dbprefix.'events_days', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function delete_days($nid) {
        if ($this->db->delete($this->dbprefix.'events_days', array('event_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }     
    
    public function set_all_close_by_default(){
        $now=$this->utils->dateLong();        
        $sql="UPDATE ".$this->dbname.'.'.$this->dbprefix."nodes n SET active=0 WHERE n.type_id=2";
        //echo '<pre>'.$sql.'</pre>';
        $this->executeSql($sql);        
    }
    
    public function set_open(){
    
    $sql="
    UPDATE ".$this->dbname.'.'.$this->dbprefix."nodes n SET n.active=1 WHERE n.id IN 
    (
        SELECT DISTINCT(event_id) FROM ".$this->dbname.'.'.$this->dbprefix."events_dates ed WHERE (UNIX_TIMESTAMP(NOW())*1000) BETWEEN ed.start AND ed.end
    )            
    ";
    $this->executeSql($sql);
    }
    
}    