<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class _Menu_Model extends Main_Model {

    public function __construct() {
        parent::__construct();
    }

    public function set_menu($roles=FALSE) {

        $sql = "SELECT m. * , mr.role_id FROM ".$this->dbname.'.'.$this->dbprefix."_menu m ";
        $sql.=" JOIN ".$this->dbname.'.'.$this->dbprefix."_menu_roles mr ON mr.menu_id = m.id ";
        if($roles) $sql.=" AND mr.role_id IN (" . implode(',', $roles) . ") ";
        $sql.=" WHERE m.pid IS NULL AND m.active=1 ";
        $sql.=" GROUP BY m.id ";
        $sql.=" ORDER BY m.position #_Menu_Model:set_menu (roles)";            
        
        return $this->getRows($sql);
    }

    public function set_submenu($roles=array()) {

        if (isset($roles) AND sizeof($roles) > 0) {
            //$sql = "SELECT m. * , mr.role_id FROM ".$this->dbname.'.'.$this->dbprefix."_menu m JOIN ".$this->dbname.'.'.$this->dbprefix."_menu_roles mr ON mr.menu_id = m.id AND mr.role_id IS NULL OR mr.role_id IN (" . implode(',', $roles) . ") WHERE m.pid = $id AND m.active=1 GROUP BY m.id #_Menu_Model:set_submenu (roles)";
            $sql = "SELECT m. * , mr.role_id FROM ".$this->dbname.'.'.$this->dbprefix."_menu m JOIN ".$this->dbname.'.'.$this->dbprefix."_menu_roles mr ON mr.menu_id = m.id AND mr.role_id IN (" . implode(',', $roles) . ") WHERE m.pid = $id AND m.active=1 GROUP BY m.id ORDER BY m.position #_Menu_Model:set_submenu (roles)";
        } else {
            $sql = "SELECT m. * , mr.role_id FROM ".$this->dbname.'.'.$this->dbprefix."_menu m JOIN ".$this->dbname.'.'.$this->dbprefix."_menu_roles mr ON mr.menu_id = m.id AND mr.role_id IS NULL WHERE m.pid = $id AND m.active=1 GROUP BY m.id ORDER BY m.position #_Menu_Model:set_submenu (no roles)";
        }
        //echo '<pre>' . $sql . '</pre>';
        return $this->getRows($sql);
    }    
    
    
    public function set_roles_menu($roles) {

        if (isset($roles) AND sizeof($roles) > 0) {
            $sql = "SELECT m. * , mr.role_id FROM ".$this->dbname.'.'.$this->dbprefix."_menu m JOIN ".$this->dbname.'.'.$this->dbprefix."_menu_roles mr ON mr.menu_id = m.id AND mr.role_id IN (" . implode(',', $roles) . ") WHERE m.pid IS NULL AND m.active=1 GROUP BY m.id ORDER BY m.position #_Menu_Model:set_menu (roles)";            
        } else {
            $sql = "SELECT m. * , mr.role_id FROM ".$this->dbname.'.'.$this->dbprefix."_menu m JOIN ".$this->dbname.'.'.$this->dbprefix."_menu_roles mr ON mr.menu_id = m.id AND mr.role_id IS NULL WHERE m.pid IS NULL AND m.active=1 GROUP BY m.id ORDER BY m.position #_Menu_Model:set_menu (no roles)";
        }
        //echo '<pre>' . $sql . '</pre>';
        return $this->getRows($sql);
    }

    public function set_roles_submenu($id, $roles) {

        if (isset($roles) AND sizeof($roles) > 0) {
            //$sql = "SELECT m. * , mr.role_id FROM ".$this->dbname.'.'.$this->dbprefix."_menu m JOIN ".$this->dbname.'.'.$this->dbprefix."_menu_roles mr ON mr.menu_id = m.id AND mr.role_id IS NULL OR mr.role_id IN (" . implode(',', $roles) . ") WHERE m.pid = $id AND m.active=1 GROUP BY m.id #_Menu_Model:set_submenu (roles)";
            $sql = "SELECT m. * , mr.role_id FROM ".$this->dbname.'.'.$this->dbprefix."_menu m JOIN ".$this->dbname.'.'.$this->dbprefix."_menu_roles mr ON mr.menu_id = m.id AND mr.role_id IN (" . implode(',', $roles) . ") WHERE m.pid = $id AND m.active=1 GROUP BY m.id ORDER BY m.position #_Menu_Model:set_submenu (roles)";
        } else {
            $sql = "SELECT m. * , mr.role_id FROM ".$this->dbname.'.'.$this->dbprefix."_menu m JOIN ".$this->dbname.'.'.$this->dbprefix."_menu_roles mr ON mr.menu_id = m.id AND mr.role_id IS NULL WHERE m.pid = $id AND m.active=1 GROUP BY m.id ORDER BY m.position #_Menu_Model:set_submenu (no roles)";
        }
        //echo '<pre>' . $sql . '</pre>';
        return $this->getRows($sql);
    }
    
    public function role_menu($roles){
        $sql="SELECT m . * FROM ".$this->dbname.'.'.$this->dbprefix."_menu_roles _mr JOIN ".$this->dbname.'.'.$this->dbprefix."_menu m ON m.id = _mr.menu_id WHERE _mr.role_id IN (" . implode(',', $roles) . ") AND m.class IS NOT NULL ";
        
        return $this->getRows($sql);
    }

}

