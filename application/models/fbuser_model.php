<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fbuser_Model extends Main_Model {
    
    public function __construct() {
        parent::__construct();
    }

    public function isunique($col,$val){        
        $sql="SELECT COUNT(f.node_id) AS total FROM ".$this->dbname.'.'.$this->dbprefix."fbusers f WHERE $col='$val';";
        echo '<pre>'.$sql.'</pre>';
        $total= $this->getRow($sql)->total;
        if($total<1) return TRUE;
        return FALSE;
    }
    
    public function getidby($col,$val){        
        $sql="SELECT f.node_id FROM ".$this->dbname.'.'.$this->dbprefix."fbusers f WHERE $col='$val';";
        return $this->getRow($sql)->node_id;;        
    }    
    
    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'fbusers', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'fbusers', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'fbusers', array('node_id' => $nid))) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }     
    
    public function fblogin($fbtoken){
        $sql = "SELECT f.node_id FROM ".$this->dbname.'.'.$this->dbprefix."fbusers f JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id=f.node_id WHERE fbtoken='$fbtoken' AND n.active = 1";
        //echo '<pre>'.$sql.'</pre>';die;
        $res = $this->getRow($sql);
        if($res) return $this->getRow($sql)->node_id;
        
        return FALSE;
    }    
    
}    