<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tag_Model extends Main_Model {
    
    public function __construct() {
        parent::__construct(); 
    }
    
    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'tags', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'tags', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'tags', array('node_id' => $nid))) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }    
    
    public function insert_node_nodes_tags($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'nodes_tags', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function update_node_nodes_tags($data) {
        
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'nodes_tags', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function delete_node_nodes_tags($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'nodes_tags', array('node_id' => $nid))) {            
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }    
    
    public function delete_node_tags_by_tagid($nid,$tagid){
        $sql="DELETE FROM ".$this->dbname.'.'.$this->dbprefix."nodes_tags WHERE tag_id IN (SELECT node_id FROM ".$this->dbname.'.'.$this->dbprefix."tags WHERE type_id =$tagid) AND node_id =$nid";
        $this->executeSql($sql);
    }


    public function get_tag_by_id($type_id){
        $sql="
        SELECT n.* 
        FROM  ".$this->dbname.'.'.$this->dbprefix."nodes n
        WHERE n.type_id =$type_id
        AND n.active =1  
        #".$this->router->fetch_class().':'.$this->router->fetch_class()."";
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRows($sql);
    }    
    
    public function get_node_tags_by_id($nid,$type_id){
        $sql="
        SELECT t.node_id, t.name
        FROM ".$this->dbname.'.'.$this->dbprefix."tags t
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes_tags nt ON nt.tag_id = t.node_id
        WHERE t.type_id =$type_id
        AND nt.node_id =$nid 
        #".$this->router->fetch_class().':'.$this->router->fetch_class()."";
        $sql.="#".$this->router->fetch_class().' - '.$this->router->fetch_method();
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRows($sql);
    }
   
    public function get_node_tags_by_type($type_id){
        $sql="
        SELECT t.node_id,t.name
        FROM ".$this->dbname.'.'.$this->dbprefix."tags t
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = t.node_id
        WHERE t.type_id=$type_id
        AND n.active=1
        #".$this->router->fetch_class().':'.$this->router->fetch_class()."";
        $sql.="#".$this->router->fetch_class().' - '.$this->router->fetch_method();
        
        return $this->getRows($sql);
    }    
}    