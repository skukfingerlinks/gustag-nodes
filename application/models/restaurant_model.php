<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurant_Model extends Main_Model {
    
    public function __construct() {
        parent::__construct(); 
    }
    
    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'restaurants', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'restaurants', $data))return TRUE;        
        return FALSE;
    }

    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'restaurants', array('node_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }
    
    public function countall($uid=FALSE,$active=TRUE){
        $sql="
        SELECT COUNT(n.id) AS total 
        FROM ".$this->dbname.'.'.$this->dbprefix."restaurants r
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = r.node_id 
        WHERE 1=1 ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        return $this->getRow($sql)->total;
    }    
    
    public function all($uid=FALSE,$active=TRUE,$limit=999,$offset=0){
        $sql="
        SELECT n . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."restaurants r
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = r.node_id 
        WHERE 1=1 ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        $sql.="ORDER BY n.active ";
        if(isset($limit) and isset($offset)) $sql.=" LIMIT $offset,$limit";        
        return $this->getRows($sql);
    }
    
    public function user_restaurants($uid=FALSE,$active=FALSE){
        $sql="
        SELECT r . *,n.owner_id,n.active
        FROM ".$this->dbname.'.'.$this->dbprefix."restaurants r
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = r.node_id
        WHERE 1=1 ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        return $this->getRows($sql);
    }
    
    
    public function restaurant_dishes($nid){
        $sql="
        SELECT * 
        FROM ".$this->dbname.'.'.$this->dbprefix."nodes n
        JOIN ".$this->dbname.'.'.$this->dbprefix."restaurants_dishes rd ON rd.dish_id=n.id AND rd.restaurant_id=$nid
        WHERE n.active=1";   
        return $this->getRows($sql);
    }
    
    public function get_dishes($nid,$onlyids=FALSE){
        $sql="
        SELECT d .*,md.menu_id, m.restaurant_id
        FROM ".$this->dbname.'.'.$this->dbprefix."menus_dishes md
        JOIN ".$this->dbname.'.'.$this->dbprefix."menus m ON m.node_id = md.menu_id
        JOIN ".$this->dbname.'.'.$this->dbprefix."dishes d ON d.node_id = md.dish_id
        WHERE m.restaurant_id =$nid            
        ";
        echo '<pre>'.$sql.'</pre>';die;
        $dishes = $this->getRows($sql);
        if($onlyids){
            foreach($dishes as $dish) $res[]=$dish->node_id;
            return $res;
        }else{
            return $dishes;
        }
        //return $this->getRows($sql);
    }
    
    
    public function get_dates($nid) {

        $sql = "SELECT start,end FROM ".$this->dbname.'.'.$this->dbprefix."restaurants_closingdates WHERE restaurant_id=$nid";
        return $this->getRows($sql);
    }     
    
    public function check_date($nid,$start,$end) {

        $sql = "SELECT COUNT(restaurant_id) AS total FROM ".$this->dbname.'.'.$this->dbprefix."restaurants_closingdates WHERE restaurant_id=$nid AND start=$start AND end=$end ";
        $res = $this->getRow($sql)->total;
        if($res<1)return TRUE;
        return FALSE;
    }    
    
    public function insert_dates($data) {
        if ($this->db->insert($this->dbprefix.'restaurants_closingdates', $data)) {            
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function delete_dates($nid) {
        if ($this->db->delete($this->dbprefix.'restaurants_closingdates', array('restaurant_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }
  
    public function delete_date($nid,$start,$end){
        $sql="DELETE FROM ".$this->dbname.'.'.$this->dbprefix."restaurants_closingdates WHERE start=$start AND end=$end AND restaurant_id=$nid";
        if($this->executeSql($sql)) return TRUE;
        return FALSE;
    }
    
    
    public function get_days($nid) {

        $res=array();
        $sql = "SELECT day FROM ".$this->dbname.'.'.$this->dbprefix."restaurants_closingdays WHERE restaurant_id=$nid";        
        $dates = $this->getRows($sql);
        if($dates) foreach($dates as $date) $res[]=$date->day;
        return $res;
    }     
    
    public function insert_days($data) {
        if ($this->db->insert($this->dbprefix.'restaurants_closingdays', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function delete_days($nid) {
        if ($this->db->delete($this->dbprefix.'restaurants_closingdays', array('restaurant_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }    

    public function get_restaurants_dishes($nid,$active=TRUE) {
        $sql = "
        SELECT d.*,rd.rating
        FROM ".$this->dbname.'.'.$this->dbprefix."dishes d
        JOIN ".$this->dbname.'.'.$this->dbprefix."restaurants_dishes rd ON rd.dish_id = d.node_id
        AND rd.restaurant_id =$nid
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = d.node_id
        WHERE 1 =1 ";
        if($active) $sql.="AND n.active =1 ";
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRows($sql);

    }    
    
    public function insert_restaurants_dishes($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'restaurants_dishes', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function delete_restaurants_dishes($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'restaurants_dishes', array('restaurant_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }    
    
    public function is_closed_today($nid){
        $sql="SELECT COUNT(day) AS closed FROM ".$this->dbname.'.'.$this->dbprefix."restaurants_closingdays WHERE restaurant_id =$nid AND day = DAYOFWEEK(NOW())";
        echo '<pre>'.$sql.'</pre>';
        $res = $this->getRow($sql)->closed;
        if($res>1) return TRUE;
        return FALSE;
        //return $this->getRows($sql);
        
    }
    
    public function set_close_by_day(){
        $now=(time()*1000);
        $sql="UPDATE ".$this->dbname.'.'.$this->dbprefix."nodes n SET n.active=0 WHERE n.id IN (SELECT restaurant_id FROM ".$this->dbname.'.'.$this->dbprefix."restaurants_closingdates WHERE $now BETWEEN start AND end) AND n.active=1";        
        //echo '<pre>'.$sql.'</pre>';
        $this->executeSql($sql);
    }
    public function set_open_by_day(){
        $now=(time()*1000);
        $sql="UPDATE ".$this->dbname.'.'.$this->dbprefix."nodes n SET n.active=1 WHERE n.id IN (SELECT restaurant_id FROM ".$this->dbname.'.'.$this->dbprefix."restaurants_closingdates WHERE $now NOT BETWEEN start AND end) AND n.active=1";        
        //echo '<pre>'.$sql.'</pre>';
        $this->executeSql($sql);
    }    
    public function set_close_by_date(){
        $dayofweek=date('N',time());
        $sql="UPDATE ".$this->dbname.'.'.$this->dbprefix."nodes n SET n.active=0 WHERE n.id IN (SELECT restaurant_id FROM ".$this->dbname.'.'.$this->dbprefix."restaurants_closingdays WHERE day=$dayofweek) AND n.active=1";
        //echo '<pre>'.$sql.'</pre>';
        $this->executeSql($sql);
    }    
    
    public function set_open_by_date(){
        $dayofweek=date('N',time());
        $sql="UPDATE ".$this->dbname.'.'.$this->dbprefix."nodes n SET n.active=1 WHERE n.id IN (SELECT restaurant_id FROM ".$this->dbname.'.'.$this->dbprefix."restaurants_closingdays WHERE day=$dayofweek) AND n.active=1";
        //echo '<pre>'.$sql.'</pre>';
        $this->executeSql($sql);
    }    
    
}    