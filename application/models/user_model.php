<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_Model extends Main_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function isunique($col,$val){        
        $sql="SELECT COUNT(u.node_id) AS total FROM ".$this->dbname.'.'.$this->dbprefix."users u WHERE $col='$val';";
        $total= $this->getRow($sql)->total;
        if($total<1) return TRUE;
        return FALSE;
    }
    
    public function getidby($col,$val){        
        $sql="SELECT u.node_id FROM ".$this->dbname.'.'.$this->dbprefix."users u WHERE $col='$val';";
        return $this->getRow($sql)->node_id;;        
    }    
    
    public function getplainpwd($uid){
        $sql="SELECT u.plainpwd FROM ".$this->dbname.'.'.$this->dbprefix."users u WHERE u.node_id=$uid;";
        return $this->getRow($sql)->plainpwd;
    }
    
    
    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'users', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'users', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'users', array('node_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }    
    
    public function countall($uid=FALSE,$role=FALSE,$active=TRUE){
        $sql="
        SELECT COUNT( DISTINCT (n.id)) AS total 
        FROM ".$this->dbname.'.'.$this->dbprefix."users u
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = u.node_id ";
        if($role) $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."users_roles ur ON ur.user_id=u.node_id";
        $sql.=" WHERE 1=1 ";
        if($role) $sql.="AND ur.role_id=$role ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        //echo '<pre>'.$sql.'</pre>';
        return $this->getRow($sql)->total;
    }    
    
    public function all($uid=FALSE,$role=FALSE,$active=TRUE,$limit=999,$offset=0){
        
        $sql="
        SELECT n . * ,u.*
        FROM ".$this->dbname.'.'.$this->dbprefix."users u
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = u.node_id ";
        if($role) $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."users_roles ur ON ur.user_id=u.node_id";
        $sql.=" WHERE 1=1 ";
        if($role) $sql.="AND ur.role_id=$role ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        $sql.=" GROUP BY n.id ";
        if(isset($limit) and isset($offset)) $sql.=" LIMIT $offset,$limit";
        
        //echo '<pre>'.$sql.'</pre>';
        return $this->getRows($sql);
    }    
    
    public function login($usermail,$userpwd){
        $sql = "SELECT u.node_id FROM ".$this->dbname.'.'.$this->dbprefix."users u JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id=u.node_id WHERE usermail='$usermail' AND userpwd='$userpwd' AND n.active = 1";
        $res = $this->getRow($sql);
        if($res) return $this->getRow($sql)->node_id;
        
        return FALSE;
    }
    
    public function mobilelogin($usertoken){
        $sql = "SELECT u.node_id FROM ".$this->dbname.'.'.$this->dbprefix."users u JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id=u.node_id WHERE user_token='$usertoken' AND n.active = 1";
        $res = $this->getRow($sql);
        if($res) return $this->getRow($sql)->node_id;
        
        return FALSE;
    }    
    
    
    
}    