<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Node_Model extends Main_Model {
    
    public function __construct() {
        parent::__construct(); 
    }

    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'nodes', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        
        $this->db->where('id', $data['id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'nodes', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'nodes', array('id' => $nid))) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }    
    
    public function get_type($type){
        $sql="SELECT _t.* FROM ".$this->dbname.'.'.$this->dbprefix."_types _t WHERE _t.id =$type";
        $type = $this->getRow($sql); 
        return $type;         
    }    

    public function get_nodes(){
        $sql="SELECT n.id,n.name,n.active,n.type_id FROM ".$this->dbname.'.'.$this->dbprefix."nodes n ";
        $nodes = $this->getRows($sql);
        if($nodes) return $nodes;        
        return FALSE;
    }    
    
    public function get_node($nid){
        $sql="SELECT n.* FROM ".$this->dbname.'.'.$this->dbprefix."nodes n WHERE id=$nid ";
        $node = $this->getRow($sql);
        if($node) return $node;        
        return FALSE;
    }
    
    public function get_object_data($tbl,$nid){
        $sql="SELECT * FROM ".$this->dbname.'.'.$this->dbprefix.$tbl." WHERE node_id=$nid ";
        $node = $this->getRow($sql);
        if($node) return $node;        
        return FALSE;        
    }


    public function get_object_tags($object){
        $sql="SELECT _t.* FROM ".$this->dbname.'.'.$this->dbprefix."_types _t WHERE _t.object =  '$object' AND _t.name != _t.object";
        $node = $this->getRows($sql);
        return $node;        
    }    
    
    public function get_tags_by_type($type){
        $sql="SELECT node_id AS id FROM ".$this->dbname.'.'.$this->dbprefix."tags WHERE type_id =$type";
        $tags_ids = $this->getRows($sql);
        return $tags_ids;        
    }     
    
    public function get_tagsids_by_type($type_id){
        $result=array();
        $sql="SELECT node_id AS id FROM ".$this->dbname.'.'.$this->dbprefix."tags WHERE type_id =$type_id";
        $tags_ids = $this->getRows($sql);
        if($tags_ids) foreach($tags_ids as $tag_id) $result[]=$tag_id->id;
        return $result;        
    }     
    
    public function get_node_tagsids($nid,$tag_ids){
        $result=array();
        $sql="SELECT n.* FROM ".$this->dbname.'.'.$this->dbprefix."nodes n WHERE id IN (SELECT nt.tag_id AS id FROM ".$this->dbname.'.'.$this->dbprefix."nodes_tags nt WHERE nt.node_id =$nid AND nt.tag_id IN ($tag_ids))";
        $node_tags_ids = $this->getRows($sql);
        return $node_tags_ids;        
    }    
    
    public function get_node_geopoints($nid){
        $sql="SELECT g.* FROM ".$this->dbname.'.'.$this->dbprefix."geopoints g JOIN ".$this->dbname.'.'.$this->dbprefix."nodes_geopoints ng ON ng.geopoint_id=g.node_id WHERE ng.node_id = $nid";
        $node = $this->getRows($sql);
        return $node;        
    }   
    
    public function get_object_parents($object){
        $sql= "SELECT * FROM ".$this->dbname.'.'.$this->dbprefix."_hmvc WHERE child_table='$object' AND active=1";
        $parent_objects = $this->getRows($sql);
        return $parent_objects;
    }
    
    public function get_object_children($object){
        $sql= "SELECT * FROM ".$this->dbname.'.'.$this->dbprefix."_hmvc WHERE parent_table='$object' AND active=1";
        $parent_objects = $this->getRows($sql);
        return $parent_objects;
    }   
    
    public function get_relation_children($tbl,$nid,$active=FALSE){
        

        $sql = "SELECT ".$this->dbname.'.'.$this->dbprefix."" . $tbl->child_table . ".*,".$this->dbprefix."nodes.type_id,".$this->dbprefix."nodes.active,".$this->dbprefix."_types.name as type ";
        $sql.="FROM ".$this->dbname.'.'.$this->dbprefix."" . $tbl->child_table . " ";
        if(isset($tbl->private_key)){
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."nodes ON ".$this->dbprefix."nodes.id=" . $this->dbprefix.$tbl->child_table . "." . $tbl->private_key . " ";
        }else{
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."nodes ON ".$this->dbprefix."nodes.id=" . $this->dbprefix.$tbl->child_table . "." . $tbl->primary_key . " ";
        }        
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."_types ON ".$this->dbprefix."_types.id=".$this->dbprefix."nodes.type_id ";
        if(isset($tbl->private_key)){
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."" . $tbl->relation_table . " ON " . $this->dbprefix.$tbl->relation_table . "." . $tbl->child_key . "=" . $this->dbprefix.$tbl->child_table . "." . $tbl->private_key . " ";
        }else{
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."" . $tbl->relation_table . " ON " . $this->dbprefix.$tbl->relation_table . "." . $tbl->child_key . "=" . $this->dbprefix.$tbl->child_table . "." . $tbl->primary_key . " ";
        }        
        $sql.="WHERE ".$this->dbname.'.'.$this->dbprefix."" . $tbl->relation_table . "." . $tbl->parent_key . "=$nid ";
        //if($active) $sql.="JOIN nodes n ON n.id=WHERE ".$this->dbname.'.'.$this->dbprefix."" . $tbl->relation_table . "." . $tbl->parent_key . "=$nid ";
        $sql.="# get_relation_children $tbl->child_table -- $tbl->parent_table"; 
        
        //echo '<pre>'.$sql.'</pre>';
        return $this->getRows($sql);

    }
     
    public function get_direct_relation_children($tbl,$nid,$active=FALSE){
        
        if (isset($tbl->private_key)) {
        $sql = "SELECT ".$this->dbname.'.'.$this->dbprefix."" . $tbl->child_table . ".*,".$this->dbprefix."nodes.type_id,".$this->dbprefix."nodes.active,".$this->dbprefix."_types.name as type  ";
        } else {
        $sql = "SELECT ".$this->dbname.'.'.$this->dbprefix."" . $tbl->child_table . ".*,".$this->dbprefix."nodes.type_id,".$this->dbprefix."nodes.active,".$this->dbprefix."_types.name as type  ";
        }                    
        $sql.="FROM ".$this->dbname.'.'.$this->dbprefix."" . $tbl->child_table . " ";
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."nodes ON ".$this->dbprefix."nodes.id=" . $this->dbprefix.$tbl->child_table . "." . $tbl->primary_key . " ";
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."_types ON ".$this->dbprefix."_types.id=".$this->dbprefix."nodes.type_id ";
        if (isset($tbl->parent_key)) {
        $sql.="WHERE ".$this->dbname.'.'.$this->dbprefix."" . $tbl->child_table . "." . $tbl->parent_key . "=$nid ";
        } else {
        $sql.="WHERE ".$this->dbname.'.'.$this->dbprefix."" . $tbl->child_table . "." . $tbl->primary_key . "=$nid ";
        }        
        $sql.="# get_direct_relation_children ".$this->dbname.'.'.$this->dbprefix."$tbl->child_table -- ".$this->dbname.'.'.$this->dbprefix."$tbl->parent_table"; 
        //echo '<pre>'.$sql.'</pre>';
        return $this->getRows($sql);
    }
    
    public function get_relation_parents($tbl,$nid,$active=FALSE){
        
        $sql = "SELECT ".$this->dbname.'.'.$this->dbprefix."" . $tbl->parent_table . ".*,".$this->dbprefix."nodes.type_id,".$this->dbprefix."nodes.active,".$this->dbprefix."_types.name as type  ";
        $sql.="FROM ".$this->dbname.'.'.$this->dbprefix."" . $tbl->parent_table . " ";
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."" . $tbl->relation_table . " ON " . $this->dbprefix.$tbl->relation_table . "." . $tbl->parent_key . " = " . $this->dbname.".".$this->dbprefix.$tbl->parent_table . ".node_id ";
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."nodes ON ".$this->dbprefix."nodes.id=" . $this->dbprefix.$tbl->parent_table . "." . $tbl->primary_key . " ";
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."_types ON ".$this->dbprefix."_types.id=".$this->dbprefix."nodes.type_id ";                    
        $sql.= "WHERE ".$this->dbname.'.'.$this->dbprefix."" . $tbl->relation_table . "." . $tbl->child_key . " =" . $nid . " ";
        $sql.="# get_relation_parents ".$this->dbname.'.'.$this->dbprefix."$tbl->child_table -- ".$this->dbname.'.'.$this->dbprefix."$tbl->parent_table"; 
        return $this->getRows($sql);
        
    }
    public function get_direct_relation_parents($tbl,$nid,$active=FALSE){
          
        
        $sql = "SELECT ".$this->dbname.'.'.$this->dbprefix."" . $tbl->parent_table . ".*,".$this->dbprefix."nodes.type_id,".$this->dbprefix."nodes.active,".$this->dbprefix."_types.name as type  ";
        $sql .="FROM ".$this->dbname.'.'.$this->dbprefix."" . $tbl->parent_table . " ";
        $sql .="JOIN ".$this->dbname.'.'.$this->dbprefix."" . $tbl->child_table . " ON " . $this->dbprefix.$tbl->child_table . "." . $tbl->parent_key . " = " . $this->dbprefix.$tbl->parent_table . "." . $tbl->primary_key . " ";
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."nodes ON ".$this->dbprefix."nodes.id=" . $this->dbprefix.$tbl->parent_table . "." . $tbl->primary_key . " ";
        $sql.="JOIN ".$this->dbname.'.'.$this->dbprefix."_types ON ".$this->dbprefix."_types.id=".$this->dbprefix."nodes.type_id ";                                        
        $sql .="WHERE ".$this->dbname.'.'.$this->dbprefix."" . $tbl->child_table . "." . $tbl->primary_key . " =$nid "; 
        $sql.="# get_direct_relation_parents $tbl->child_table -- $tbl->parent_table"; 
        return $this->getRows($sql);
    }
    
    /* QRCODE */
    
    public function get_nid_from_qrcode($qrcode){
        $sql="SELECT n.id FROM ".$this->dbname.'.'.$this->dbprefix."nodes n WHERE qrcodeurl ='$qrcode'";
        $nid = $this->getRow($sql)->id; 
        return $nid;         
    }     
    public function get_qrcode_from_nid($nid){
        $sql="SELECT n.qrcodeurl FROM ".$this->dbname.'.'.$this->dbprefix."nodes n WHERE id ='$nid'";
        $qrcodeurl = $this->getRow($sql)->qrcodeurl; 
        return $qrcodeurl;         
    }         
    
    public function get_node_media($nid,$active=FALSE){
        $sql="
        SELECT m.*,n.owner_id,_t.name AS media_type
        FROM ".$this->dbname.'.'.$this->dbprefix."media m
        JOIN ".$this->dbname.'.'.$this->dbprefix."_types _t ON _t.id = m.type_id             
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = m.node_id        
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes_media nm ON nm.media_id = m.node_id           
        AND nm.node_id =$nid
        WHERE 1 =1 ";
        if($active) $sql.="AND n.active =1 ";        
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRows($sql); 
    }
    
    public function get_node_tags($nid,$active=TRUE){
        $sql="
        SELECT n . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."nodes n
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes_tags nt ON nt.tag_id = n.id
        AND nt.node_id =$nid ";
        if($active) $sql.=" WHERE n.active =1 ";        
        return $this->getRows($sql);         
    }
    
}    
