<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Geopoint_Model extends Main_Model {

    
    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'geopoints', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'geopoints', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'geopoints', array('node_id' => $nid))) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    } 
    
    public function insert_node_geopoint($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'nodes_geopoints', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update_node_geopoint($data) {
        
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'nodes_geopoints', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function delete_node_geopoint($node_id) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'nodes_geopoints', array('node_id' => $node_id))) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }    
    
    public function get_user_geopoints($uid){
        $sql="SELECT g. * FROM ".$this->dbname.'.'.$this->dbprefix."geopoints g JOIN ".$this->dbname.'.'.$this->dbprefix."nodes_geopoints ng ON ng.geopoint_id = g.node_id WHERE ng.node_id =$uid";
        return $this->getRows($sql);
    }
    
    public function locations($active=FALSE){
        $sql="SELECT g.node_id,g.administrative_area_level_3 AS provincia,g.administrative_area_level_2_label AS label,g.administrative_area_level_1 AS regione,g.country AS nazione FROM ".$this->dbname.'.'.$this->dbprefix."geopoints g JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id=g.node_id JOIN ".$this->dbname.'.'.$this->dbprefix."nodes_geopoints ng ON ng.geopoint_id=g.node_id GROUP BY g.administrative_area_level_3  ORDER BY g.administrative_area_level_3";// WHERE ng.node_id=$nid;
        if($active) $sql.=" AND n.active=1";
        return $this->getRows($sql);       
    }    
    
    public function get_agent_locations(){
        $sql="
        SELECT g.node_id,g.administrative_area_level_3 AS provincia,g.administrative_area_level_2_label AS label,g.administrative_area_level_1 AS regione,g.country AS nazione
        FROM ".$this->dbname.'.'.$this->dbprefix."geopoints g 
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id=g.node_id 
        WHERE n.active=1
        GROUP BY g.administrative_area_level_3
        ORDER BY g.administrative_area_level_3            
        ";
        return $this->getRows($sql);       
    }
    
    
    public function countall($active=TRUE){
        $sql="
        SELECT COUNT(n.id) AS total 
        FROM ".$this->dbname.'.'.$this->dbprefix."geopoints g
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = g.node_id ";
        $sql.=" WHERE 1=1 ";
        if($active) $sql.="AND n.active=1 ";
        //echo '<pre>'.$sql.'</pre>';die;        
        return $this->getRow($sql)->total;
    }    
    
    public function all($active=TRUE,$limit=999,$offset=0){
        
        $sql="
        SELECT n . * ,g.*
        FROM ".$this->dbname.'.'.$this->dbprefix."geopoints g
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = g.node_id ";
        $sql.=" WHERE 1=1 ";
        if($active) $sql.="AND n.active=1 ";
        $sql.="ORDER BY g.formatted_address ";
        if(isset($limit) and isset($offset)) $sql.=" LIMIT $offset,$limit";
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRows($sql);
    }    
    
    public function get_geopoint_nodes($nid,$active=FALSE){
        $sql="
        SELECT n. *,_t.name AS type
        FROM ".$this->dbname.'.'.$this->dbprefix."nodes n
        JOIN ".$this->dbname.'.'.$this->dbprefix."_types _t ON _t.id = n.type_id
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes_geopoints ng ON ng.node_id = n.id
        AND ng.geopoint_id =$nid
        WHERE 1 =1 ";
        if($active) $sql.="AND n.active=1 ";
        return $this->getRows($sql);

    }
    
    
}    