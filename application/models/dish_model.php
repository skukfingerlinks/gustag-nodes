<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dish_Model extends Main_Model {
    
    public function __construct() {
        parent::__construct(); 
    }
    
    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'dishes', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'dishes', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }   
    
    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'dishes', array('node_id' => $nid))) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }    
    
    public function restaurant_countall($nid=FALSE,$active=FALSE){

        $sql="
        SELECT COUNT( DISTINCT n.id ) AS total
        FROM ".$this->dbname.'.'.$this->dbprefix."dishes d
        JOIN ".$this->dbname.'.'.$this->dbprefix."restaurants_dishes rd ON rd.dish_id = d.node_id
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = d.node_id
        WHERE 1 =1
        AND rd.restaurant_id =$nid ";
        if($active) $sql.="AND n.active=1 ";
        //$sql.="GROUP BY n.id ";
        $sql.="#Dish_Model:restaurant_countall";
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRow($sql)->total;
    }    
    
    public function restaurant_all($nid=FALSE,$active=FALSE,$limit=999,$offset=0){
        $sql="
        SELECT n . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."dishes d
        JOIN ".$this->dbname.'.'.$this->dbprefix."restaurants_dishes rd ON rd.dish_id = d.node_id
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = d.node_id
        WHERE 1 =1
        AND rd.restaurant_id =$nid ";
        if($active) $sql.="AND n.active=1 ";
        $sql.="GROUP BY n.id ";
        $sql.="#Dish_Model:restaurant_all";
        return $this->getRows($sql);
    }    
    
    public function countall($uid=FALSE,$active=TRUE){
        $sql="
        SELECT COUNT(n.id) AS total 
        FROM ".$this->dbname.'.'.$this->dbprefix."dishes d
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = d.node_id 
        WHERE 1=1 ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        $sql.="#Dish_Model:countall";
        return $this->getRow($sql)->total;
    }    
    
    public function all($uid=FALSE,$active=TRUE,$limit=999,$offset=0){
        $sql="
        SELECT n . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."dishes d
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = d.node_id 
        WHERE 1=1 ";
        if($uid) $sql.="AND n.owner_id =$uid ";
        if($active) $sql.="AND n.active=1 ";
        if(isset($limit) and isset($offset)) $sql.=" LIMIT $offset,$limit";
        $sql.="#Dish_Model:all";
        return $this->getRows($sql);
    }    
    
    public function get_dish_events($nid){
        $sql="
        SELECT
        ed.event_id
        FROM ".$this->dbname.'.'.$this->dbprefix."events_menus em
        WHERE ed.menu_id IN
        (
            SELECT md.menu_id
            FROM ".$this->dbname.'.'.$this->dbprefix."menus_dishes md
            WHERE md.dish_id =$nid
        )            
        ";
        $sql.="#Dish_Model:get_dish_events";
        $res = array();
        foreach($this->getRows($sql) as $item) $res[]=$item->event_id;
        if(sizeof($res)>0) return $res;
        return FALSE;
    }    
    
    public function get_dish_restaurants($nid){
        
        $sql="
        SELECT d.restaurant_id
        FROM ".$this->dbname.'.'.$this->dbprefix."menus m
        JOIN ".$this->dbname.'.'.$this->dbprefix."menus_dishes md
        WHERE md.dish_id =$nid           
        ";
        $sql.="#Dish_Model:get_dish_restaurants";
        $res = array();
        foreach($this->getRows($sql) as $item) $res[]=$item->restaurant_id;
        if(sizeof($res)>0) return $res;
        return FALSE;
    }   

    public function get_dishes_types($type_id){
        
        $sql="
        SELECT d . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."nodes n
        JOIN ".$this->dbname.'.'.$this->dbprefix."dishes d ON d.node_id = n.id AND d.dish_type =$type_id ";
        $sql.="#Dish_Model:get_dishes_types";
        return $this->getRows($sql);
    }    
    
    public function get_dishes_by_type($type_id,$uid=FALSE){
        
        $sql="
        SELECT d . * 
        FROM ".$this->dbname.'.'.$this->dbprefix."nodes n
        JOIN ".$this->dbname.'.'.$this->dbprefix."dishes d ON d.node_id = n.id AND d.dish_type =$type_id ";
        if($uid) $sql.="WHERE n.owner_id IS NULL OR n.owner_id =$uid AND n.active =1 ";
        $sql.="#Dish_Model:get_dishes_by_type";
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRows($sql);
    }     
    
    public function get_dish_restaurant($nid) {

        $sql = "SELECT restaurant_id FROM ".$this->dbname.'.'.$this->dbprefix."restaurants_dishes  WHERE dish_id=$nid";        
        return $this->getRow($sql)->restaurant_id;
    }      
    
    public function get_restaurant_dishes($nid){
        $sql="SELECT * FROM ".$this->dbname.'.'.$this->dbprefix."dishes d JOIN ".$this->dbname.'.'.$this->dbprefix."restaurants_dishes rd ON rd.dish_id = d.node_id AND rd.restaurant_id=$nid GROUP BY d.node_id";
        return $this->getRows($sql);
    }
    
}    