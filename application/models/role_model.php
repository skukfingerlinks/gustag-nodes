<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role_Model extends Main_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get($id) {    
        $sql = "SELECT m. * FROM ".$this->dbname.'.'.$this->dbprefix."_menu m JOIN ".$this->dbname.'.'.$this->dbprefix."_menu_roles mr ON mr.menu_id = m.id AND mr.role_id IN (" . implode(',', $roles) . ") WHERE m.pid IS NULL AND m.active=1 GROUP BY m.id ORDER BY m.position #_Menu_Model:set_menu (roles)";            
        return $this->getRow($sql);         
    }  
    public function user_roles($limit=FALSE) {
        $sql="SELECT * FROM ".$this->dbname.'.'.$this->dbprefix."_roles ";
        if($limit) $sql.=" WHERE id > $limit";
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRows($sql);         
    } 
    
    public function get_user_roles($uid){
        $sql="SELECT r . * FROM ".$this->dbname.'.'.$this->dbprefix."_roles r JOIN ".$this->dbname.'.'.$this->dbprefix."users_roles ur ON ur.role_id = r.id WHERE ur.user_id =$uid";
        return $this->getRows($sql);
    }

    public function insert_user_roles($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'users_roles', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update_user_roles($data) {
        $this->db->where('node_id', $data['node_id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'users_roles', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_user_roles($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'users_roles', array('user_id' => $nid))) {
            return TRUE;
        }
        return FALSE;
    }    
}    