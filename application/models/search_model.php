<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search_Model extends Main_Model {

    public function __construct() {
        parent::__construct();
        $this->db_prefix = $this->dbname.'.'.$this->dbprefix;
    }

    public function search_by_position($params, $onlyids = FALSE) {

        $sql = "
        SELECT n.id, n.name, _ty.object, ( 3959 * ACOS( COS( RADIANS( $params->latitude ) ) * COS( RADIANS( n.latitude ) ) * COS( RADIANS( n.longitude ) - RADIANS( $params->longitude ) ) + SIN( RADIANS( $params->latitude ) ) * SIN( RADIANS( n.latitude ) ) ) ) AS distance
        FROM ".$this->dbname.'.'.$this->dbprefix."nodes n
        JOIN ".$this->dbname.'.'.$this->dbprefix."_types _ty ON _ty.id = n.type_id
        WHERE n.active =1 ";
        if (isset($params->objects_id))
            $sql.=" AND n.type_id IN (" . implode(',', $params->objects_id) . ") "; //AND n.type_id IN ( 1, 2 ) 
        $sql.=" HAVING distance <= $params->distance ";
        $sql.="ORDER BY distance ";
        echo '<pre>'.$sql.'</pre>';
        $res = $this->getRows($sql);
        if ($onlyids) {
            foreach ($res as $node)
                $res[] = $node->node_id;
            return $res;
        } else {
            return $res;
        }
    }

    public function search_by_tag($word, $ids, $onlyids = FALSE) {

        $sql = "
        SELECT n.id, n.name, _t.object, _t.name AS type 
        FROM ".$this->dbname.'.'.$this->dbprefix."nodes_tags nt
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id=nt.node_id
        JOIN ".$this->dbname.'.'.$this->dbprefix."_types _t ON _t.id=n.type_id
        WHERE nt.tag_id
        IN (
            SELECT t.node_id
            FROM  ".$this->dbname.'.'.$this->dbprefix."tags t
            WHERE  t.name LIKE  '%aperto%'
        )
        AND n.id IN(" . implode(',', $ids) . ")
        AND n.active=1
        AND _t.object=_t.name
        GROUP BY n.id  
        #search_by_tag: $word
";
//echo '<pre>'.$sql.'</pre>';        

        /*
          $sql="
          SELECT t.node_id ,_t.object,_t.name AS type
          FROM ".$this->dbname.'.'.$this->dbprefix."tags t
          JOIN ".$this->dbname.'.'.$this->dbprefix."nodes_tags nt ON nt.tag_id=t.node_id
          JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id=t.node_id
          JOIN ".$this->dbname.'.'.$this->dbprefix."_types _t ON _t.id = n.type_id
          WHERE t.name LIKE '%$word%'
          AND nt.node_id IN (".implode(',',$ids).")
          AND n.active =1
          AND _t.object=_t.name #search_by_tag: $word";
         */
        $res = $this->getRows($sql);
        if ($onlyids) {
            foreach ($res as $node)
                $res[] = $node->node_id;
            return $res;
        } else {
            return $res;
        }
    }

    public function search_by_name($word, $ids, $onlyids = FALSE) {

        $sql = "
        SELECT n.id, n.name, _t.object, _t.name AS type 
        FROM ".$this->dbname.'.'.$this->dbprefix."nodes n
        JOIN ".$this->dbname.'.'.$this->dbprefix."_types _t ON _t.id = n.type_id
        WHERE _t.object = _t.name
        AND n.name LIKE  '%$word%' OR n.address LIKE  '%$word%'
        AND n.active =1  
        # search_by_name: $word";


//        $sql="
//        SELECT n.id, n.name, _t.object, _t.name AS type 
//        FROM ".$this->dbname.'.'.$this->dbprefix."nodes n
//        JOIN ".$this->dbname.'.'.$this->dbprefix."_types _t ON _t.id = n.type_id
//        WHERE n.id IN (".implode(',',$ids).") 
//        AND _t.object = _t.name
//        AND n.name LIKE  '%$word%' OR n.address LIKE  '%$word%'
//        AND n.active =1  
//        # search_by_name: $word";
        /*
          $sql="
          SELECT n.id,n.name,_t.object,_t.name AS type
          FROM ".$this->dbname.'.'.$this->dbprefix."nodes n
          JOIN ".$this->dbname.'.'.$this->dbprefix."_types _t ON _t.id = n.type_id
          WHERE n.name LIKE  '%$word%'
          OR n.address LIKE  '%$word%'
          AND n.id IN (".implode(',',$ids).")
          AND n.active =1
          AND _t.object=_t.name # search_by_name: $word";
          echo '<pre>'.$sql.'</pre>';die;
         */

        echo '<pre>' . $sql . '</pre>';
        $res = $this->getRows($sql);
        if ($onlyids) {
            foreach ($res as $node)
                $res[] = $node->node_id;
            return $res;
        } else {
            return $res;
        }
    }

    public function get_nodes_by_name($word,$searchable_object) {
        $sql="
        SELECT n.*,_t.name AS type
        FROM ".$this->dbname.'.'.$this->dbprefix."nodes n
        JOIN ".$this->dbname.'.'.$this->dbprefix."_types _t ON _t.id=n.type_id
        WHERE n.type_id
        IN (".implode(',',$searchable_object).") 
        AND n.name LIKE  '%$word%'
        AND n.active =1";           

        //$sql = "SELECT n.* FROM ".$this->dbname.'.'.$this->dbprefix."nodes n WHERE n.type_id IN (".implode(',',$searchable_object).") AND n.name LIKE  '%$word%' OR n.address LIKE  '%$word%' AND n.active =1";
        //echo '<pre>' . $sql . '</pre>';
        return $this->getRows($sql);
    }

    public function get_restaurant_event_dishes($restaurant_id) {
        $sql = "
        SELECT md.dish_id
        FROM ".$this->dbname.'.'.$this->dbprefix."menus_dishes md
        WHERE md.menu_id
        IN (
            SELECT em.menu_id
            FROM ".$this->dbname.'.'.$this->dbprefix."events_menus em
            WHERE em.event_id
            IN (
                SELECT node_id
                FROM  ".$this->dbname.'.'.$this->dbprefix."events
                WHERE  `restaurant_id` =$restaurant_id
            )
        )            
        ";
        echo '<pre>' . $sql . '</pre>';
        return $this->getRows($sql);
    }
    
    public function get_restaurant_dishes($restaurant_id){
        $sql="SELECT md.dish_id FROM  n_menus_dishes md WHERE md.menu_id IN (SELECT node_id FROM  `n_menus` WHERE  `restaurant_id` =$restaurant_id)";
        echo '<pre>' . $sql . '</pre>';
        return $this->getRows($sql);
    }
    
    public function tags_found_by_name($nid,$word){
                $sql="
                SELECT n.*, _t.name AS type
                FROM ".$this->db_prefix."nodes n
                JOIN ".$this->dbname.'.'.$this->dbprefix."_types _t ON _t.id = n.type_id
                JOIN ".$this->db_prefix."nodes_tags nt ON nt.tag_id =n.id AND nt.node_id=$nid
                AND n.name LIKE '%$word%'
                AND n.active=1";
                echo '<pre>' . $sql . '</pre>'; 
                return $this->getRows($sql);
    }

}

