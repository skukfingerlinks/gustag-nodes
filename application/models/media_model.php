<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Media_Model extends Main_Model {
    
    public function __construct() {
        parent::__construct(); 
    }

    public function insert($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'media', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update($data) {
        
        $this->db->where('id', $data['id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'media', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function delete($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'media', array('node_id' => $nid))) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }
    
    public function insert_node_media($data) {
        if ($this->db->insert($this->dbname.'.'.$this->dbprefix.'nodes_media', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update_node_media($data) {
        
        $this->db->where('id', $data['id']);
        if ($this->db->update($this->dbname.'.'.$this->dbprefix.'nodes_media', $data)) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }

    public function delete_node_media($nid) {
        if ($this->db->delete($this->dbname.'.'.$this->dbprefix.'nodes_media', array('node_id' => $nid))) {
            if ($this->debug) $this->showSql($this->db->last_query());
            return TRUE;
        }
        return FALSE;
    }    
    
    public function get_node_media($nid,$active=FALSE){
        $sql="
        SELECT m.*,n.owner_id,_t.name AS media_type
        FROM ".$this->dbname.'.'.$this->dbprefix."media m
        JOIN ".$this->dbname.'.'.$this->dbprefix."_types _t ON _t.id = m.type_id             
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes n ON n.id = m.node_id        
        JOIN ".$this->dbname.'.'.$this->dbprefix."nodes_media nm ON nm.media_id = m.node_id           
        AND nm.node_id =$nid
        WHERE 1 =1 ";
        if($active) $sql.="AND n.active =1 ";        
        //echo '<pre>'.$sql.'</pre>';die;
        return $this->getRows($sql); 
    }    
}    