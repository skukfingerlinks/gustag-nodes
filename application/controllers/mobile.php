<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mobile extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_Model','UserModel');
        $this->load->model('Fbuser_Model','FbuserModel');
        $this->load->model('Device_Model','DeviceModel');
    }

    public function fbregister() {

        $user = $this->facebook->getUser();
        if (!$user) {
            redirect($this->facebook->getLoginUrl(array('scope' => $this->config->item('fbscopes'), 'display' => 'touch', 'redirect_uri' => site_url('/mobile/fbregister'))));
        } else {

            $fbdata = $this->fbapiCall('/me');
            $fbdata->avatar = 'http://graph.facebook.com/' . $fbdata->id . '/picture?type=large';
            $this->facebook->setExtendedAccessToken();
            $fbdata->access_token = $this->facebook->getAccessToken();
            $fbdata->fbuser = TRUE;
            
            if (!$this->UserModel->isunique('usermail', $fbdata->email)){
                $nid = $this->UserModel->getidby('usermail', $fbdata->email);
                $this->_fbuserupdate($nid,$obj);
            }else if(!$this->UserModel->isunique('username', $fbdata->username)){
                $nid = $this->UserModel->getidby('usermail', $fbdata->email);
                $this->_fbuserupdate($nid,$obj);
            }else{
                $this->_fbregister($fbdata);
            }            
            
        }
    }
   
    
    protected function _fbregister($obj){

        
        if ($obj->fbuser) {
            $obj->usermail = $obj->email;
            unset($obj->email);
            $obj->sex = $obj->gender;
            unset($obj->gender);
        }    
        
        //CREA NODO
        $nid = $this->_create(8,FALSE);
        
        $user = array(
            'node_id' => $nid, //*
            'username' => $obj->username, //*
            'usermail' => $obj->usermail, //*
            'plainpwd' => $userpwd = ($obj->fbuser) ? NULL : $obj->userpwd, //*
            'userpwd' => $userpwd = ($obj->fbuser) ? NULL : $this->utils->encrypt($obj->userpwd), //*
            'user_token' => $user_token = ($obj->fbuser) ? $this->utils->encrypt($nid . $obj->access_token . $this->utils->dateLong()) : $this->encrypt($nid . $obj->userpwd . $this->utils->dateLong()), //*
            'first_name' => $first_name = (isset($obj->first_name)) ? $obj->first_name : NULL,
            'last_name' => $last_name = (isset($obj->last_name)) ? $obj->last_name : NULL,
            'avatar' => $avatar = (isset($obj->avatar)) ? $obj->avatar : NULL,
            'birthday' => $birthday = (isset($obj->birthday)) ? $this->utils->dateFormat($obj->birthday) : NULL,
            'sex' => $sex = (isset($obj->sex)) ? $obj->sex : NULL,
            'phone' => $sex = (isset($obj->phone)) ? $obj->phone : NULL,
            'status' => $sex = (isset($obj->status)) ? $obj->status : NULL,
            'fbuser' => $fbuser = ($obj->fbuser) ? 1 : 0, //*
            'fbregistration' => $fbregistration = ($obj->fbuser) ? 1 : 0, //*
        );
        
        $user = array_filter($user);           
        $this->UserModel->insert($user); 
        //AGGIORNA NODO
        $this->NodeModel->update(array('id' => $nid, 'name' => $obj->username));
        //ASSEGNO RUOLO UTENTE
        $this->RoleModel->insert_user_roles(array('user_id' => $nid,'role_id' => 5));        
        
        //AGGIORNO L'AVATAR
        $this->_user_dirs($nid);
        
        $this->_fbuserregister($nid,$obj);
        
        $this->registration_mail($nid);
        
        $this->get($nid);  


    }
    
    protected function _fbuserregister($nid,$obj){
        
        if ($obj->fbuser) {
            $obj->usermail = $obj->email;
            unset($obj->email);
            $obj->sex = $obj->gender;
            unset($obj->gender);
        }         
        
        $fbuser = array(
            'node_id' => $nid, //*
            'fbid' => $obj->id,
            'fbusername' => $fbusername = (isset($obj->username)) ? $obj->username : NULL,
            'fbfirstname' => $fbfirstname = (isset($obj->first_name)) ? $obj->first_name : NULL,
            'fblastname' => $fblastname = (isset($obj->last_name)) ? $obj->last_name : NULL,
            'fbgender' => $fbgender = (isset($obj->sex)) ? $obj->sex : NULL,
            'fbbirthday' => $fbbirthday = (isset($obj->birthday)) ? $this->utils->dateFormat($obj->birthday) : NULL,
            'fblink' => $fblink = (isset($obj->link)) ? $obj->link : NULL,
            'fbemail' => $fbemail = (isset($obj->usermail)) ? $obj->usermail : NULL,
            'fbavatar' => $fbavatar = (isset($obj->avatar)) ? $obj->avatar : NULL,
            'fbtoken' => $fbtoken = (isset($obj->access_token)) ? $obj->access_token : NULL,
        );        
        
        $fbuser = array_filter($fbuser);           
        $this->FbuserModel->insert($fbuser);
        $this->get($nid);
        
    }

    protected function _fbuserupdate($nid,$obj){
        
        if ($obj->fbuser) {
            $obj->usermail = $obj->email;
            unset($obj->email);
            $obj->sex = $obj->gender;
            unset($obj->gender);
        }         
        
        $fbuser = array(
            'node_id' => $nid, //*
            'fbid' => $obj->id,
            'fbusername' => $fbusername = (isset($obj->username)) ? $obj->username : NULL,
            'fbfirstname' => $fbfirstname = (isset($obj->first_name)) ? $obj->first_name : NULL,
            'fblastname' => $fblastname = (isset($obj->last_name)) ? $obj->last_name : NULL,
            'fbgender' => $fbgender = (isset($obj->sex)) ? $obj->sex : NULL,
            'fbbirthday' => $fbbirthday = (isset($obj->birthday)) ? $this->utils->dateFormat($obj->birthday) : NULL,
            'fblink' => $fblink = (isset($obj->link)) ? $obj->link : NULL,
            'fbemail' => $fbemail = (isset($obj->usermail)) ? $obj->usermail : NULL,
            'fbavatar' => $fbavatar = (isset($obj->avatar)) ? $obj->avatar : NULL,
            'fbtoken' => $fbtoken = (isset($obj->access_token)) ? $obj->access_token : NULL,
        );        
        
        $fbuser = array_filter($fbuser);           
        $this->FbuserModel->update($fbuser);  
        //$this->UserModel->update(array('node_id' => $nid, 'avatar'=>$obj->avatar));
        $this->get($nid);
        
    }    

    public function registration_mail($nid){
        
            $user = $this->_get($nid,TRUE);
            $user->plainpwd = $this->UserModel->getplainpwd($user->id);            
            $data['user']=$user;            
            $this->load->library('parser');
            $data['subject']='Benvenuto in Gustag!';
            $data['altbody'] = $this->parser->parse('mails/registration_txt', $data,TRUE);
            $data['htmlbody'] = $this->parser->parse('mails/registration', $data,TRUE);  
            if($this->_send_mail($data)) $this->UserModel->update(array('node_id'=>$nid,'registration_mail'=>1));
            
    }    
    
    public function fblogin(){
        $this->_secured();
        $nid = $this->FbuserModel->fblogin($this->httpheaders['Fbtoken']);
        if (!$nid) $this->utils->jsonError(array('error_code' => 'ERROR', 'error_msg' => 'login_failed'));
        $this->_update_user_device($nid);
        $this->get($nid);

    }
    
    public function login(){
   
        $this->_secured();
        $nid = $this->UserModel->mobilelogin($this->httpheaders['User-Token']);
        if (!$nid) $this->utils->jsonError(array('error_code' => 'ERROR', 'error_msg' => 'login_failed'));
        $this->_update_user_device($nid);
        $this->get($nid);
    }
    
    public function register(){
        
        $this->_secured();        
        if (!$this->UserModel->isunique('usermail', $this->postrequest->usermail)){
            $this->utils->jsonError(array('error_code' => 'ERROR', 'error_msg' => 'usermail_not_unique'));
        }
        if (!$this->UserModel->isunique('username', $this->postrequest->username)){
            $this->utils->jsonError(array('error_code' => 'ERROR', 'error_msg' => 'username_not_unique'));
        }        
        //CREA NODO
        $nid = $this->_create(8,FALSE);
        //CREA DEVICE
        $user = array(
            'node_id' => $nid, //*
            'username' => $this->postrequest->username, //*
            'usermail' => $this->postrequest->usermail, //*
            'plainpwd' => $this->postrequest->userpwd, //*
            'userpwd' => $this->utils->encrypt($this->postrequest->userpwd), //*
            'user_token' => $this->utils->encrypt($nid . $obj->userpwd . $this->utils->dateLong()), //*
            'first_name' => NULL,
            'last_name' => NULL,
            'avatar' => NULL,
            'birthday' => NULL,
            'sex' => NULL,
            'phone' => NULL,
            'status' => NULL,
            'fbuser' => 0, //*
            'fbregistration' =>0, //*
        );        
        $user = array_filter($user);
        $this->UserModel->insert($user); 
        //AGGIORNA NODO
        $this->NodeModel->update(array('id' => $nid, 'name' => $this->postrequest->username));
        //ASSEGNO RUOLO UTENTE
        $this->RoleModel->insert_user_roles(array('user_id' => $nid,'role_id' => 5));                
        //AGGIORNO L'AVATAR
        $this->_user_dirs($nid);
        $this->_avatar($nid);
        $this->_set_user_device($nid);
        $this->registration_mail($nid);        
        $this->get($nid);
    }
    
    public function testdevice(){
        
        $nid=1;
        $this->load->model('Device_Model','DeviceModel');
        //GET DEVICE
        echo '$user_device<br />';
        $user_device = $this->DeviceModel->get_user_devices(2198,FALSE,$this->httpheaders['Device-Id']);
        $this->utils->dump($user_device);
        //GET ACTIVE DEVICE
        echo '$user_active_devices<br />';
        $user_active_devices = $this->DeviceModel->get_user_devices(2198,TRUE);
        $this->utils->dump($user_active_devices);
        //GET ALL DEVICES
        echo '$user_devices<br />';
        $user_devices = $this->DeviceModel->get_user_devices(2198);
        $this->utils->dump($user_devices);
        
        die;
        
        $device = array(
          'node_id'=>$nid,
          'device_id'=>$this->httpheaders['Device-Id'],
          'device_type'=>$this->httpheaders['Device-Type'],
          'push_notification_token'=>$pushtoken = (isset($this->httpheaders['Device-Token']) AND strlen($this->httpheaders['Device-Token'])>0) ? $this->httpheaders['Device-Token'] : NULL,
        );        
        $this->utils->dump($device);die;
        $this->utils->dump($this->httpheaders);die;
        
        $this->load->model('Device_Model','DeviceModel');
        if($this->DeviceModel->isunique('device_id',$this->httpheaders['Device-Id'])){
            echo 'nessuno ha questo telefono. inserisci ed associa a $nid<br />';
        }else{
            echo 'qualcuno ha lo stesso telefono?<br />';
        }
        echo "SELECT COUNT(d.node_id) AS total FROM n_devices d WHERE device_id='".$this->httpheaders['Device-Id']."';";
        die;
        $this->utils->dump($this->httpheaders);
        //$this->_set_user_device(1);
    }
    
    protected function _set_user_device($uid){
        
        if (!isset($this->httpheaders['Device-Id']) OR strlen($this->httpheaders['Device-Token'])<1){
            $this->utils->jsonError(array('error_code' => 'ERROR', 'error_msg' => 'device_id_missing'));
        }        
        if (!isset($this->httpheaders['Device-Type']) OR strlen($this->httpheaders['Device-Type'])<1){
            $this->utils->jsonError(array('error_code' => 'ERROR', 'error_msg' => 'device_type_missing'));
        }                
        $nid = $this->_create(9,$uid);
        $device = array(
          'node_id'=>$nid,
          'device_id'=>$this->httpheaders['Device-Id'],
          'device_type'=>$this->httpheaders['Device-Type'],
          'push_notification_token'=>$pushnotificationtoken = (isset($this->httpheaders['Device-Token'])) ? $this->httpheaders['Device-Token'] : NULL,
        );
        $this->DeviceModel->insert($device);        
        $this->DeviceModel->insert_users_devices(array('user_id'=>$uid,'device_id'=>$nid));
    }

    
    public function logout(){
        $this->_secured();
        $nid = $this->UserModel->mobilelogin($this->httpheaders['User-Token']);
        if (!$nid) $this->utils->jsonError(array('error_code' => 'ERROR', 'error_msg' => 'login_failed'));
        $this->_set_device_inactive($nid);     
        $this->get($nid);
    }

    protected function _update_user_device($nid){                
        
        //IL DEVICE E' REGISTRATO?        
        //SI. LO AGGIORNO E LO ATTIVO
        if (!$this->DeviceModel->isunique('device_id',$this->httpheaders['Device-Id'])){
                $user_device = $this->DeviceModel->get_user_devices($nid,FALSE,$this->httpheaders['Device-Id']);
                $this->NodeModel->update(array('id'=>$user_device->node_id,'active'=>1));
        //NO. LO REGISTRO E LO ATTIVO
        }else{
            $this->_set_user_device($nid);
        }        
    }
    
    protected function _set_device_inactive($nid){
        $user_device = $this->DeviceModel->get_user_devices($nid,FALSE,$this->httpheaders['Device-Id']);
        $this->NodeModel->update(array('id'=>$user_device->node_id,'active'=>0));        
    }

    public function get($nid){
        $user = $this->_get($nid,TRUE); 
        $private = array('userpwd','plainpwd','fbregistration','mobile_registration','registration_mail','push_notification_token');
        foreach($user as $k=>$v){
            if(is_array($v)){
                foreach($v as $a=>$b) if(in_array($k,$private)) unset($v->$a);
            }else{
                if(in_array($k,$private)) unset($user->$k);
            }
            
        }
        $this->utils->jsonResponse($user);die;        
    }
    
}    