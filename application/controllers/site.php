<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Site extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->viewdata['showuserinfo']=FALSE;
        unset($this->viewdata['showuserinfo']);
    }
    
    public function index() {
        $this->_set_Jquery();
        $this->show('site/index');
    }    
    
    public function logout() {
        $this->_logout();
        redirect('/login');
    }      
    
    public function login() {
        
        $this->form_validation->set_rules('usermail', 'Usermail', 'trim|required|xss_clean');
        $this->form_validation->set_rules('userpwd', 'Password', 'trim|required|xss_clean');        
        if ($this->form_validation->run() == FALSE) {
        } else {         
            /* DO LOGIN GET USER */
            $nid = $this->UserModel->login(trim($this->postrequest->usermail),$this->utils->encrypt(trim($this->postrequest->userpwd)));                        
            if(!$nid) {echo 'handle error';die;}
            $this->_login($nid);
            redirect('/welcome');
        }        
        $this->_set_Jquery();
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/validation/login.js');
        $this->show('site/login');
    }     
    
    public function test() {
        $this->utils->dump($this->usersession);die;
    }     
}

