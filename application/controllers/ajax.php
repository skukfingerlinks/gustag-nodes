<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax extends Main_Controller {
    
    public function deleteCalendarDate(){
        
        $nid = $this->postrequest->node_id;
        $date = $this->postrequest->date;
        $start = (strtotime($date.' 00:00:01')*1000);
        $end = (strtotime($date.' 23:59:59')*1000);
        
        $this->load->model('Restaurant_Model','RestaurantModel');
        //if($this->RestaurantModel->check_date($nid,$start,$end)){
            if($this->RestaurantModel->delete_date($nid,$start,$end)){
                echo 'rimosso';
            }else{
                echo 'fallito!';
            }
        //}else{
            //echo 'non presente!';
        //}
        
        //echo "DELETE FROM ci_restaurants_closingdates WHERE start=$start AND end=$end AND restaurant_id=$nid";
        
    }
    public function setCalendarDate(){
        
        $nid = $this->postrequest->node_id;
        $date = $this->postrequest->date;
        $start = (strtotime($date.' 00:00:01')*1000);
        $end = (strtotime($date.' 23:59:59')*1000);
        
        $this->load->model('Restaurant_Model','RestaurantModel');
        //if($this->RestaurantModel->check_date($nid,$start,$end)){
            $calendar_date=array('restaurant_id'=>$nid,'start'=>$start,'end'=>$end);
            if($this->RestaurantModel->insert_dates($calendar_date)){
                echo 'inserito';
            }else{
                echo 'fallito!';
            }
        //}else{
            //echo 'non presente!';
        //}
        
        //echo "DELETE FROM ci_restaurants_closingdates WHERE start=$start AND end=$end AND restaurant_id=$nid";
        
    }    
    
    public function resetCalendarDate(){
        $nid = $this->postrequest->node_id;
        $this->RestaurantModel->delete_dates($nid);
        $this->session->set_flashdata('success',$this->lang->line('dates_delete_success'));
        redirect('/restaurants/calendar/'.$nid,'refresh');            
        
    }
    
    public function deleteEventDate(){
        
        $nid = $this->postrequest->node_id;
        $date = $this->postrequest->date;
        $start = (strtotime($date.' 00:00:01')*1000);
        $end = (strtotime($date.' 23:59:59')*1000);
        
        $this->load->model('Event_Model','EventModel');
        if($this->EventModel->check_date($nid,$start,$end)){
            $this->EventModel->delete_date($nid,$start,$end);
        }
        
    }
    public function setEventDate(){
        
        $nid = $this->postrequest->node_id;
        $date = $this->postrequest->date;
        $start = (strtotime($date.' 00:00:01')*1000);
        $end = (strtotime($date.' 23:59:59')*1000);

        $this->load->model('Event_Model','EventModel');
        if($this->EventModel->check_date($nid,$start,$end)){
            $event_date=array('event_id'=>$nid,'start'=>$start,'end'=>$end);
            $this->EventModel->insert_dates($event_date);
        }
        
    }    
    
    public function resetEventDate(){
        $nid = $this->postrequest->node_id;
        $this->EventModel->delete_dates($nid);
        $this->session->set_flashdata('success',$this->lang->line('dates_delete_success'));
        redirect('/events/calendar/'.$nid,'refresh');            
        
    } 
    
}
