<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->_is_logged();
        if (!$this->usersession->is_owner)
            $this->_logout();
        $this->load->model('Event_Model', 'EventModel');
    }

    public function index($offset = 0) {
        
        if ($this->usersession->is_admin) {
            $count = $this->EventModel->countall(FALSE, FALSE);
        } else {
            $count = $this->EventModel->countall($this->usersession->id, FALSE);
        }        
        if ($count) {
            if ($this->usersession->is_admin) {
                $events = $this->EventModel->all(FALSE, FALSE, $this->config->item('items_per_page'), $offset);
            } else {
                $events = $this->EventModel->all($this->usersession->id, FALSE, $this->config->item('items_per_page'), $offset);
            }
            foreach ($events as $event) $found[] = $this->_get($event->id, TRUE);
            $this->_paginate('/events/index', $count, $found, $this->config->item('items_per_page'));
        }        
        $this->viewdata['html']['h1'] = 'Eventi';
        $this->viewdata['html']['items_type'] = 'events';

        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_GMap();
        $this->_set_JqueryTablesorter();
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->_set_custom_js('/assets/js/custom/dashboardmap.js');

        //$this->utils->dump($this->viewdata);

        $this->show('events/index');        
        
    }

    public function restaurant($nid, $offset = 0) {
        
        $restaurant = $this->_get($nid,TRUE);
        $this->viewdata['restaurant'] = $restaurant;
        $this->viewdata['owner'] = $restaurant->users[0];

        $count = $this->EventModel->restaurant_countall($nid, FALSE);
        $events = $this->EventModel->restaurant_all($nid, FALSE, 4, $offset);
        
        
        
        foreach ($events as $event) $found[] = $this->_get($event->id, TRUE);     
        
        //$this->utils->dump($found);die;
        $this->_paginate('/events/restaurant/'.$nid, $count, $found,4);
        
        
        
        $this->viewdata['html']['h1'] = 'Eventi';
        $this->viewdata['html']['items_type'] = 'events';
        $this->viewdata['restaurant'] = $restaurant;

        $this->_set_Jquery();
        $this->show('events/restaurant');
    }

    public function create($nid) {
        $restaurant = $this->_get($nid, TRUE);

        if (!$restaurant->dishes) {
            $this->session->set_flashdata('error', $this->lang->line('missing_restaurant_dishes'));
            redirect('dishes/create/' . $nid, 'refresh');
        }

        $this->viewdata['restaurant'] = $restaurant;

        $event = new stdClass;
        $event->type->object = 'events';
        $this->_object_tags($event);
        $this->_get_node_tags_chosen_options($event);
        
        //$this->utils->dump($restaurant);die;
        
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else {   
            
            //$this->utils->dump($this->postrequest->menus);die;
                   
            //VERIFICA MENU
            if(!isset($this->postrequest->menus) OR sizeof($this->postrequest->menus)<1){
                $this->session->set_flashdata('error',$this->lang->line('missing_event_menu'));
                redirect('events/create/'.$nid,'refresh');                 
            }            
            
            //CREA NODO
            $new_nid = $this->_create(2);
            //CREA EVENTO
            $new_event=array(
                'node_id'=>$new_nid,     
                //'user_id'=>$this->usersession->id,
                'restaurant_id'=>$nid,            
                'name'=>$this->postrequest->name,
                'desc'=> $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc): NULL,
                'email'=> $email = (isset($this->postrequest->email)) ? strip_tags($this->postrequest->email): NULL,
                'phone'=> $phone = (isset($this->postrequest->phone)) ? strip_tags($this->postrequest->phone): NULL,
                'website'=> $website = (isset($this->postrequest->website)) ? strip_tags($this->postrequest->website): NULL,
                'seats'=> $seats = (isset($this->postrequest->seats)) ? strip_tags($this->postrequest->seats): NULL,
                'logo'=> $this->config->item('event_default_logo'),
            );        
            $new_event=array_filter($new_event);
            $this->EventModel->insert($new_event);            
            //AGGIORNA NODO
            $this->NodeModel->update(array('id'=>$new_nid,'name'=>$this->postrequest->name));
            //ASSOCIA TAGS
            $this->_set_tags($new_nid);
            //ASSOCIA MENU
            if(isset($this->postrequest->menus) AND sizeof($this->postrequest->menus)>0){
                $event_menus = $this->utils->chosenResult($this->postrequest->menus);
                foreach($event_menus as $event_menu) $this->EventModel->insert_event_menus(array('event_id'=>$new_nid,'menu_id'=>$event_menu));
            }            
            //CREA/AGGIORNA GEOPOINT
            if(isset($this->postrequest->addressinfo) AND strlen($this->postrequest->addressinfo)>0){  
                $this->_set_geopoint($new_nid);
            }else{
                $update_node_geoinfo=array(
                    'id'=>$new_nid,
                    'latitude'=>$restaurant->latitude,
                    'longitude'=>$restaurant->longitude,
                    'address'=>$restaurant->address,
                    'updated'=>$this->utils->dateLong(),
                );     
                $this->NodeModel->update($update_node_geoinfo);                 
            }            
            //CARICA LOGO
            $this->_event_dirs($new_nid);
            $this->_eventlogo($new_nid); 
            
            //DEFINISCI GIORNI DI CHIUSURA
            $this->_set_days($new_nid);            
            
            //REDIRECT
            $this->session->set_flashdata('success',$this->lang->line('create_success'));
            redirect('events/restaurant/'.$nid,'refresh');             
        } 
        $this->load->model('Menu_Model','MenuModel');
        $menus = $this->MenuModel->restaurant_all($event->restaurant_id);
        if($menus) $this->viewdata['restaurant_menus'] = $this->utils->chosenOptions($menus);
        
        
        
        foreach ($this->week_days as $k => $v)
            $this->viewdata['week_days'][$k . '-' . $v] = $v;
        $this->viewdata['data'] = $event;
        $this->viewdata['owner'] = $restaurant->users[0];
        $this->_set_GMap();
        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_JqueryGeocomplete();
        $this->_set_custom_js('/assets/js/jquery.geocomplete/init.js');
        
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_event.js');


        $this->show('events/create');

        //$this->utils->dump($this->viewdata['dishes']);die;
        //$this->utils->dump($event);die;
    }

    public function edit($nid) {

        $event = $this->_get($nid,TRUE);        
        $this->_get_node_tags_chosen_options($event);
        $restaurant = $this->_get($event->restaurant_id,TRUE);
        
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else {               
            //AGGIORNA EVENTO
            $update_event=array(
                'node_id'=>$nid,     
                //'user_id'=>$this->usersession->id,
                'restaurant_id'=>$event->restaurant_id,            
                'name'=>$this->postrequest->name,
                'desc'=> $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc): NULL,
                'email'=> $email = (isset($this->postrequest->email)) ? strip_tags($this->postrequest->email): NULL,
                'phone'=> $phone = (isset($this->postrequest->phone)) ? strip_tags($this->postrequest->phone): NULL,
                'website'=> $website = (isset($this->postrequest->website)) ? strip_tags($this->postrequest->website): NULL,
                'seats'=> $seats = (isset($this->postrequest->seats)) ? strip_tags($this->postrequest->seats): NULL,
            );        
            $update_event=array_filter($update_event);
            $this->EventModel->update($update_event);             
            //AGGIORNA NODO
            $this->NodeModel->update(array('id'=>$nid,'name'=>$this->postrequest->name));            
            //ASSOCIA TAGS
            $this->_set_tags($nid);
            //RESETTA ASSOCIA MENU
            if(isset($this->postrequest->menus) AND sizeof($this->postrequest->menus)>0){
                $this->EventModel->delete_event_menus($nid);
                $event_menus = $this->utils->chosenResult($this->postrequest->menus);
                foreach($event_menus as $event_menu) $this->EventModel->insert_event_menus(array('event_id'=>$nid,'menu_id'=>$event_menu));
            }             
            //AGGIORNA GEOPOINT
            if(isset($this->postrequest->addressinfo) AND strlen($this->postrequest->addressinfo)>0) $this->_set_geopoint($nid);
            //AGGIORNA LOGO
            $this->_event_dirs($nid);
            $this->_eventlogo($nid);            
            
            //DEFINISCI GIORNI DI CHIUSURA
            $this->_set_days($nid);              
            
            //REDIRECT
            $this->session->set_flashdata('success',$this->lang->line('create_success'));
            redirect('events/edit/'.$nid,'refresh');             
        }        
        
        
        $this->viewdata['data'] = $event;    
        $restaurant = $this->_get($event->restaurant_id,TRUE);
        $this->viewdata['restaurant'] = $restaurant;  
        $this->viewdata['owner'] = $restaurant->users[0];
        
        foreach ($this->week_days as $k => $v) $this->viewdata['week_days'][$k . '-' . $v] = $v;
        foreach($event->days as $day) $this->viewdata['event_days'][]=$day.'-'.$this->week_days[$day];        
        
        $this->load->model('Menu_Model','MenuModel');
        $menus = $this->MenuModel->restaurant_all($event->restaurant_id);
        if($menus) $this->viewdata['restaurant_menus'] = $this->utils->chosenOptions($menus);        
        
        if(isset($event->menus)) foreach($event->menus as $event_menu) $this->viewdata['event_menus'][]=$event_menu->node_id.'-'.$event_menu->name;                
        $this->_set_GMap();
        $this->_set_Jquery();        
        $this->_set_JqueryGeocomplete();
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->_set_custom_js('/assets/js/jquery.geocomplete/init.js');        
        $this->_set_custom_js('/assets/js/custom/calendar.js');
        $this->_set_ChosenJS();        
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_event.js');
        $this->load->model('Menu_Model','MenuModel');
        
        //$this->utils->dump($this->viewdata['event_menus']);die;
        
        $this->show('events/edit');         
        /*
        $this->utils->dump($this->viewdata);
        $this->utils->dump($event);
        die;
        
        
        $this->viewdata['data'] = $event;    
        $restaurant = $this->_get($event->restaurant_id,TRUE);
        $this->viewdata['restaurant'] = $restaurant;         
        
        $this->load->model('Menu_Model','MenuModel');
        $menus = $this->MenuModel->restaurant_all($event->restaurant_id);
        if($menus) $this->viewdata['restaurant_menus'] = $this->utils->chosenOptions($menus);        
        if(isset($event->menus)) foreach($event->menus as $event_menu) $this->viewdata['event_menus'][]=$event_menu->node_id.'-'.$event_menu->name;        
        
        
        
        //$this->utils->dump($this->viewdata);die;
        
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else {   

            $this->utils->dump($this->postrequest);die;
            
            $edit_event=array(
                'node_id'=>$nid, 
                'user_id'=>$this->usersession->id,
                'restaurant_id'=>$event->restaurant_id,            
                'name'=>$this->postrequest->name,
                'desc'=> $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc): NULL,
                'email'=> $email = (isset($this->postrequest->email)) ? strip_tags($this->postrequest->email): NULL,
                'phone'=> $phone = (isset($this->postrequest->phone)) ? strip_tags($this->postrequest->phone): NULL,
                'website'=> $website = (isset($this->postrequest->website)) ? strip_tags($this->postrequest->website): NULL,
                'seats'=> $seats = (isset($this->postrequest->seats)) ? strip_tags($this->postrequest->seats): NULL,                
            );        
            $edit_event=array_filter($edit_event);
            $this->EventModel->update($edit_event);
            $this->NodeModel->update(array('id'=>$nid,'name'=>$this->postrequest->name,'updated'=>$this->utils->dateLong()));
            
            $this->_set_tags($nid);
            
            if(isset($this->postrequest->addressinfo) AND strlen($this->postrequest->addressinfo)>0){  
                $this->_set_geopoint($nid);
            }else{
                $update_node_geoinfo=array(
                    'id'=>$nid,
                    'latitude'=>$restaurant->latitude,
                    'longitude'=>$restaurant->longitude,
                    'address'=>$restaurant->address,
                    'updated'=>$this->utils->dateLong(),
                );     
                $this->NodeModel->update($update_node_geoinfo);                 
            }
            
            if(isset($this->postrequest->menus) AND sizeof($this->postrequest->menus)>0){
                $this->EventModel->delete_event_menus($nid);
                $event_menus = $this->utils->chosenResult($this->postrequest->menus);
                foreach($event_menus as $event_menu) $this->EventModel->insert_event_menus(array('event_id'=>$new_nid,'menu_id'=>$event_menu));
            }            
            
            $this->_event_dirs($new_nid);
            $this->_eventlogo($new_nid);             
            
            $this->session->set_flashdata('success',$this->lang->line('create_success'));
            redirect('events/edit/'.$nid,'refresh'); 
        }        
        
        
        $this->viewdata['data'] = $event;    
        $restaurant = $this->_get($event->restaurant_id,TRUE);
        
        $this->load->model('Menu_Model','MenuModel');
        $menus = $this->MenuModel->restaurant_all($event->restaurant_id);
        if($menus) $this->viewdata['restaurant_menus'] = $this->utils->chosenOptions($menus);
        
        if(isset($event->menus)) foreach($event->menus as $event_menu) $this->viewdata['event_menus'][]=$event_menu->node_id.'-'.$event_menu->name;
        
        $this->utils->dump($this->viewdata);die;
        
        $this->viewdata['restaurant'] = $restaurant;        
        $this->_set_GMap();
        $this->_set_Jquery();        
        $this->_set_JqueryGeocomplete();
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->_set_custom_js('/assets/js/jquery.geocomplete/init.js');        
        $this->_set_custom_js('/assets/js/custom/calendar.js');
        $this->_set_ChosenJS();        
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_event.js');
        $this->load->model('Menu_Model','MenuModel');
        
        $this->show('events/edit');          
        */ 
    } 
    
    public function calendar($nid){
        
        $event = $this->_get($nid,TRUE);        
        $this->_get_node_tags_chosen_options($event);
        $restaurant = $this->_get($event->restaurant_id,TRUE);
        //$this->utils->dump($event);die;
        
        $year = ($this->uri->segment(4) === FALSE) ? date('Y') : $this->uri->segment(4);
        $month = ($this->uri->segment(5) === FALSE) ? date('m') : $this->uri->segment(5);
        $yearmonth=$year.'/'.$month;        
        
        $this->calendar_options = array (
        'show_next_prev'  => TRUE,
        'next_prev_url'   => site_url('events/calendar/'.$nid.'/'),
        'day_type'        => 'long',
        'template'=>
        '
        {table_open}
            <table class="calendar">
        {/table_open}
	{week_day_cell}
            <th class="day_header">{week_day}</th>
        {/week_day_cell}
	{cal_cell_content}
            <a href="#" title="'.$yearmonth.'/{day}" class="pickevent assigned">{day}</a>
        {/cal_cell_content}
	{cal_cell_content_today}
            <a href="#" title="'.$yearmonth.'/{day}" class="pickevent assigned today">{day}</a>
        {/cal_cell_content_today}
	{cal_cell_no_content}
            <a href="#" title="'.$yearmonth.'/{day}" class="pickevent">{day}</a>
        {/cal_cell_no_content}
	{cal_cell_no_content_today}
            <div class="today"><a href="#" title="'.$yearmonth.'/{day}" class="pickevent">{day}</a></div>
        {/cal_cell_no_content_today}            
        '
        );
        
        $open_days=array();
        if(isset($event->dates)){
            foreach($event->dates as $date){              
                $open_year = date('Y',$date->start/1000);
                $open_month = date('m',$date->start/1000);
                $open_day = ltrim(date('d',$date->start/1000), '0');            
                if($year==$open_year AND $month==$open_month) $open_days[$open_day]='Chiuso';
            }
            $this->viewdata['open_days']=sizeof($event->dates);
        }
        
        
        
        $startDate = date("$year-$month-01");        
        foreach($this->utils->thismonthdates($startDate,'+ 3 month') as $monthday){            
            if(in_array((int)$monthday->weekday,$event->days)){
                $start = date('Y-m-d 00:00:00',strtotime($monthday->date));
                $end = date('Y-m-d 23:59:59',strtotime($monthday->date));
                $startlong=(strtotime($start)*1000);
                $endlong=(strtotime($end)*1000);
                $date_open = array(
                    'event_id'=>$nid,
                    'start'=>$startlong,
                    'end'=>$endlong,
                );
                if($this->EventModel->check_date($nid,$startlong,$endlong)) $this->EventModel->insert_dates($date_open);
                if((int)$monthday->month==(int)$month) $open_days[(int)$monthday->day]='Chiuso';
            }
        }        
        $this->viewdata['data'] = $event;    
        $this->viewdata['restaurant'] = $restaurant;  
        $this->viewdata['owner'] = $restaurant->users[0];
        $this->viewdata['nid']=$nid;
        $this->load->library('calendar',$this->calendar_options);
        $this->viewdata['calendar'] = $this->calendar->generate($year, $month, $open_days);    
        
        //$this->utils->dump($this->viewdata['calendar']);die;
        
        $this->_set_Jquery();
        $this->_set_GMap();
        $this->_set_custom_js('/assets/js/custom/showMap.js');        
        $this->_set_custom_js('/assets/js/custom/event_calendar.js');        
        
        $this->show('events/calendar');
        
        //$this->utils->dump($event);
    }      
    public function gallery($nid) {
        
        $event = $this->_get($nid,TRUE);        
        $this->_get_node_tags_chosen_options($event);
        $restaurant = $this->_get($event->restaurant_id,TRUE);
        
        $this->form_validation->set_rules('userfile', 'Immagine', '');
        if ($this->form_validation->run() == FALSE) {            
        } else {            
            if (isset($this->filesrequest->userfile) AND strlen($this->filesrequest->userfile->name) > 0) {
                $this->_event_dirs($nid);
                $gallery_path=$this->config->item('event_dirs_path') . $nid . '/gallery/';
                $image = $this->_image_upload($gallery_path);  
                $this->_set_gallery_image($nid,$gallery_path,$image,$this->config->item('event_dirs_url'));
                $this->session->set_flashdata('success',$this->lang->line('upload_success'));
                redirect('/events/gallery/'.$nid,'refresh');
            }else{
                $this->session->set_flashdata('error',$this->lang->line('missing_upload_image'));
                redirect('/events/gallery/'.$nid,'refresh');
            }
        }        
        
        
        
        if(!$this->usersession->is_admin) if($this->usersession->id!==$event->owner_id) $this->_logout ();
        $this->viewdata['data'] = $event;    
        $this->viewdata['restaurant'] = $restaurant;  
        $this->viewdata['owner'] = $restaurant->users[0];
        $this->viewdata['owner'] = $event->users[0];
        $this->_set_Jquery();       
        $this->show('events/gallery'); 
        
        
    }     
    public function delete($nid){  
        $this->NodeModel->delete($nid);
        $this->session->set_flashdata('success',$this->lang->line('removal_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }    
    
    public function activate($nid){  
        $this->NodeModel->update(array('id'=>$nid,'active'=>1));
        $this->session->set_flashdata('success',$this->lang->line('activate_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }      

    public function deactivate($nid){   
        $this->NodeModel->update(array('id'=>$nid,'active'=>0));
        $this->session->set_flashdata('success',$this->lang->line('deactivate_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }    

    protected function _set_days($nid, $update = FALSE) {
        if (isset($this->postrequest->event_days)) {
            $days = $this->utils->chosenResult($this->postrequest->event_days);
            $this->EventModel->delete_days($nid);
            foreach ($days as $day)
                $this->EventModel->insert_days(array('event_id' => $nid, 'day' => $day));
        }
    }   
    
    public function deleteimage($nid){  
        
        $image = $this->_get($nid);
        unlink($image->src);
        $this->NodeModel->delete($image->id);
        $this->session->set_flashdata('success',$this->lang->line('removal_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }     
    
    public function removedates($nid){
        $this->EventModel->delete_dates($nid);
        $this->session->set_flashdata('success',$this->lang->line('dates_delete_success'));
        redirect('/events/calendar/'.$nid,'refresh');            
    }     
    
}

