<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->_is_logged();                
        $this->load->model('Agent_Model','AgentModel');
        
        $this->_set_custom_css('/assets/css/agent.css');
    }    
    
    protected function checkRole(){
        if(!$this->usersession->is_agent) $this->_logout();
    }


    public function index() {

        $this->checkRole();
        $count = $this->AgentModel->countall($this->usersession->id,FALSE);
        $users = $this->AgentModel->all($this->usersession->id,FALSE, $this->config->item('items_per_page'), $offset);        
        foreach ($users as $user) $found[] = $this->_get($user->node_id, TRUE);
        $this->_paginate('/agent/owners', $count, $found, $this->config->item('items_per_page'));
        $this->viewdata['html']['h1'] = $this->lang->line('users');
        $this->viewdata['html']['items_type'] = 'owners';
        $this->viewdata['html']['role'] = 'owner';
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        $this->show('/agent/owners');       
    }

    public function create() {
        $this->checkRole();
        $nid = $this->usersession->id;
        $user = $this->_get($nid,TRUE);    
        $locations = $this->GeopointModel->locations();
        
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|is_unique['.$this->dbprefix.'users.username]');
        $this->form_validation->set_rules('usermail', 'Mail', 'trim|required|xss_clean|is_unique['.$this->dbprefix.'users.usermail]');
        $this->form_validation->set_rules('phone', 'Telefono', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else {
            /*
            if(!isset($this->postrequest->user_locations) OR sizeof($this->postrequest->user_locations)<1){               
                $this->viewdata['input']=$this->postrequest;
                //$this->show('/agent/create');  
                $this->session->set_flashdata('error', $this->lang->line('missing_user_locations'));
                redirect('/agent/create/');//, 'refresh'
            }
            */
            //CREA NODO
            $nid = $this->_create(8);
            //CREO L'UTENTE
            $new_user = array(
                'node_id' => $nid,
                'username' => $this->postrequest->username,
                'usermail' => $this->postrequest->usermail,
                'plainpwd' => $this->postrequest->plainpwd,
                'userpwd' => $this->utils->encrypt($this->postrequest->plainpwd),
                'first_name' => $first_name = (isset($this->postrequest->first_name)) ? $this->postrequest->first_name : NULL,
                'last_name' => $last_name = (isset($this->postrequest->last_name)) ? $this->postrequest->last_name : NULL,
                'status' => $status = (isset($this->postrequest->status)) ? strip_tags($this->postrequest->status) : NULL,
                'phone' => $phone = (isset($this->postrequest->phone)) ? $this->postrequest->phone : NULL,
                'avatar'=>$this->config->item('user_default_logo')
            );
            $new_user = array_filter($new_user);           
            $this->UserModel->insert($new_user); 
            //AGGIORNA NODO
            $this->NodeModel->update(array('id' => $nid, 'name' => $this->postrequest->username));
            //ASSEGNO RUOLO OWNER
            $this->RoleModel->insert_user_roles(array('user_id' => $nid,'role_id' => 3));
            //ASSEGNO RUOLO UTENTE
            $this->RoleModel->insert_user_roles(array('user_id' => $nid,'role_id' => 5));
            /*
            //AGENTE: AGGIORNO LE PROVINCIE
            if (isset($this->postrequest->user_locations)) {
                $this->GeopointModel->delete_node_geopoint($nid);
                foreach ($this->utils->chosenResult($this->postrequest->user_locations) as $location) $this->GeopointModel->insert_node_geopoint(array('node_id' => $nid,'geopoint_id' => $location));
            }            
            */
            
            //AGGIORNO L'AVATAR
            $this->_user_dirs($nid);
            $this->_avatar($nid);            
            
            $this->AgentModel->insert(array('owner_id'=>$nid,'agent_id'=>$this->usersession->id));
            
            $this->session->set_flashdata('success', $this->lang->line('create_success'));
            redirect('/agent/');//, 'refresh'            
            
            //CREA NODO
            
            //CREA OWNER
            
            //AGGIORNA NODO
            
            //ASSOCIA PROVINCE
            
            //ASSOCIA AGENTE
            
            
            $this->utils->dump($this->postrequest);die;
        }        
        
        
        $this->viewdata['locations'] = $this->utils->chosenOptions($locations, 'node_id', 'provincia');        
        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_owner.js');
        $this->show('/agent/create');       
    }
    
    public function all() {                 
        $all = $this->AgentModel->all();
        $this->utils->dump($all);
    }    
    
    public function agents($offset = 0) {

        $this->checkRole();
        if(!$this->usersession->is_agent) $this->_logout();
        $count = $this->UserModel->countall(FALSE, 4, FALSE);
        $users = $this->UserModel->all(FALSE, 4, FALSE, $this->config->item('items_per_page'), $offset);
        foreach ($users as $user)
            $found[] = $this->_get($user->node_id, TRUE);

        $this->_paginate('/users/agents', $count, $found, $this->config->item('items_per_page'));
        $this->viewdata['html']['h1'] = $this->lang->line('agents');
        $this->viewdata['html']['items_type'] = 'agents';
        $this->viewdata['html']['relate'] = 'owners';
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        $this->show('agent/index');
    }
    
    public function owners($offset = 0) {

        $this->checkRole();
        $count = $this->AgentModel->countall($this->usersession->id,FALSE);
        $users = $this->AgentModel->all($this->usersession->id,FALSE, $this->config->item('items_per_page'), $offset);        
        foreach ($users as $user) $found[] = $this->_get($user->node_id, TRUE);
        $this->_paginate('/agent/owners', $count, $found, $this->config->item('items_per_page'));
        $this->viewdata['html']['h1'] = $this->lang->line('users');
        $this->viewdata['html']['items_type'] = 'owners';
        $this->viewdata['html']['role'] = 'owner';
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        $this->show('/agent/owners');   
        
    }    
    
    public function relate_agents($nid=FALSE){
        
        if(!$nid) $nid = $this->usersession->id;
        //TROVA INFO AGENTE COMPRESI RISTORATORI
        $agent = $this->_get($nid,TRUE);
        $agent_owners = $this->AgentModel->owners($nid);
        if($agent_owners){
            $agent->agent_owners = $agent_owners;
            $this->viewdata['agent_owners'] = $this->utils->chosenOptions($agent->agent_owners,'node_id','username');
        }
        $this->viewdata['agent'] = $agent;
        $owners =  $this->UserModel->all(FALSE,3,FALSE);
        if($owners) $this->viewdata['all_owners'] = $this->utils->chosenOptions($owners,'node_id','username');
        
        
        $this->utils->dump($this->viewdata);        
        //TROVA TUTTI I RISTORATORI ATTIVI
        
        
        $agent = $this->_get($nid,TRUE);
        $agent_owners = $this->AgentModel->owners($nid);
        if($agent_owners) $agent->owners = $agent_owners;
        $this->utils->dump($agent);
        $owners =$this->AgentModel->user_roles(3);
        $this->utils->dump($owners);
        echo "SELECT ";die;
        $agents = $this->UserModel->all(FALSE,4,FALSE,FALSE,FALSE);
        $this->viewdata['agents'] = $this->chosenOption($agents,'id','username');
        $this->viewdata['agents'] = $this->chosenOption($agents,'id','username');
        $this->utils->dump($this->viewdata['agents']);
        
    }
    public function relateowner($nid){
        
    }
    
}
