<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Geopoints extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->_is_logged();        
        if(!$this->usersession->is_admin) $this->_logout();
    }    
    
    public function index($offset=0) {
        
        $count = $this->GeopointModel->countall(FALSE);
        $geopoints = $this->GeopointModel->all(FALSE,$this->config->item('items_per_page'), $offset);
        foreach ($geopoints as $geopoint) $found[] = $this->_get($geopoint->node_id, TRUE);
        //$this->utils->dump($found);die;
        $this->_paginate('/geopoints/index', $count, $found, $this->config->item('items_per_page'));
        $this->viewdata['html']['h1'] = $this->lang->line('geopoints');
        $this->viewdata['html']['items_type'] = 'geopoints';
        $this->viewdata['html']['item'] = 'geopoint';
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        $this->show('geopoints/index');        

    }
    
    public function create(){
  
        $this->form_validation->set_rules('address', 'Indirizzo', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else {   
            
            if (!isset($this->postrequest->addressinfo) OR strlen($this->postrequest->addressinfo) < 1){
                $this->session->set_flashdata('error',$this->lang->line('missing_geopoint_address'));
                redirect('geopoints/create','refresh');                                
            }
            
            $new_address = $this->utils->formatGoogleAddress($this->postrequest->addressinfo);  
            $geopoint_id = $this->_create_geopoint($new_address);
            $update_node_geoinfo = array(
                'id' => $geopoint_id,
                'latitude' => $new_address->latitude,
                'longitude' => $new_address->longitude,
                'address' => $new_address->formatted_address,
                'updated' => $this->utils->dateLong(),
            );
            $this->NodeModel->update($update_node_geoinfo);              
            $this->session->set_flashdata('success',$this->lang->line('create_success'));
            redirect('geopoints/index','refresh');                                
        }        
        
        $this->_set_GMap();
        $this->_set_Jquery();
        $this->_set_JqueryGeocomplete();
        $this->_set_custom_js('/assets/js/jquery.geocomplete/init.js');        
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->_set_custom_js('/assets/js/custom/edit_restaurant.js');
        $this->show('geopoints/create');
        
    }
    
    public function edit($nid){
        $geopoint = $this->_get($nid);
        $this->viewdata['data'] = $geopoint;
        $this->_set_GMap();
        $this->_set_Jquery();
        $this->_set_JqueryGeocomplete();
        $this->_set_custom_js('/assets/js/jquery.geocomplete/init.js');        
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->show('geopoints/edit');
        
        //$this->utils->dump($geopoint);die;
    }
    
    public function delete($nid){  
                
        $this->remove($nid);
        $this->NodeModel->delete($nid);
        $this->session->set_flashdata('success',$this->lang->line('removal_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }    
    
    public function activate($nid){  
        $this->NodeModel->update(array('id'=>$nid,'active'=>1));
        $this->session->set_flashdata('success',$this->lang->line('activate_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }      

    public function deactivate($nid){   
        $this->NodeModel->update(array('id'=>$nid,'active'=>0));
        $this->session->set_flashdata('success',$this->lang->line('deactivate_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }     

}
