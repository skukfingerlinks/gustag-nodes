<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nodes extends Main_Controller {

    public function __construct() {
        parent::__construct();
    }   
    
    public function activate($nid){  
        $this->NodeModel->update(array('id'=>$nid,'active'=>1));
        $this->session->set_flashdata('success',$this->lang->line('activate_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }      

    public function deactivate($nid){   
        $this->NodeModel->update(array('id'=>$nid,'active'=>0));
        $this->session->set_flashdata('success',$this->lang->line('deactivate_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }    
    
    public function index(){        
  
        if($this->is_mobile){
            echo 'mobile!';
        }else{
            echo 'no mobile!';
        }

               
    }    
    
    public function get($nid) {
        $node = $this->_get($nid,TRUE);
        unset($node->object_tags);
        $this->utils->dump($node);die;
    }  
    
    public function jsonget($nid) {
        $node = $this->_get($nid,TRUE);
        unset($node->object_tags);
        $this->utils->jsonResponse($node);die;
    }     
}

