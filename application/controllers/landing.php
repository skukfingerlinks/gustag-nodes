<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Landing extends Main_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('User_Model','UserModel');
    }
    
    public function index(){
                
        if(isset($this->postrequest->submit)){            
            
            $this->form_validation->set_rules('usermail', 'Usermail', 'trim|required|xss_clean|is_unique['.$this->dbprefix.'users.usermail]');
            if ($this->form_validation->run() == FALSE) {                  
                    $error=  $this->postrequest->usermail;//validation_errors();
                    $this->session->set_flashdata('error',$error);
                    redirect('/landing/error');
            } else {
                
                $explode = explode('@',$this->postrequest->usermail);
                $pwd = $this->utils->generateRandomString();
                $nid = $this->_create(8);
                $new_user = array(
                    'node_id' => $nid,
                    'username' => $explode[0],
                    'usermail' => $this->postrequest->usermail,
                    'plainpwd' => $pwd,
                    'userpwd' => $this->utils->encrypt($pwd),
                    'avatar'=>'/assets/img/default-avatar.png'
                );     
                $this->UserModel->insert($new_user);
                $this->NodeModel->update(array('id'=>$nid,'name'=>$explode[0]));
                
                $this->RoleModel->insert_user_roles(array('user_id'=>$nid,'role_id'=>5));                                
                if(isset($this->postrequest->owner) AND $this->postrequest->owner=='on') $this->RoleModel->insert_user_roles(array('user_id'=>$nid,'role_id'=>3));
                $this->_user_dirs($nid);      
                
                $this->registration_mail($nid);
                
                $this->session->set_userdata('uid',$nid);
                redirect('/landing/thankyou');
            }                    
        }
        $this->load->view('landing/index'); 
        
    } 

    public function agents(){
        
        if(isset($this->postrequest->submit)){
            
            //$this->utils->dump($this->postrequest);die;
            $this->form_validation->set_rules('first_name', 'Nome', '');            
            $this->form_validation->set_rules('last_name', 'Cognome', '');            
            $this->form_validation->set_rules('age', 'Et&agrave;', 'trim|numeric|greater_than[18]');            
            $this->form_validation->set_rules('phone', 'Telefono','trim|numeric|min_length[6]');            
            $this->form_validation->set_rules('usermail', 'Usermail', 'trim|required|xss_clean|is_unique['.$this->dbprefix.'users.usermail]');
            if ($this->form_validation->run() == FALSE) {                  
                    $error=  $this->postrequest->usermail;//validation_errors();
                    $this->session->set_flashdata('error',$error);
                    redirect('/landing/error');
            } else {

                $this->session->set_userdata('post_values',$this->postrequest);
                
                if(!isset($this->postrequest->user_locations) OR sizeof($this->postrequest->user_locations)<1){
                    $this->session->set_flashdata('error',$this->lang->line('missing_user_locations'));
                    redirect('/landing/agents','refresh'); 
                }
                
                $explode = explode('@',$this->postrequest->usermail);
                $pwd = $this->utils->generateRandomString();
                $nid = $this->_create(8);
                $new_user = array(
                    'node_id' => $nid,
                    'username' => $explode[0],
                    'usermail' => $this->postrequest->usermail,
                    'plainpwd' => $pwd,
                    'userpwd' => $this->utils->encrypt($pwd),
                    'age' => $age = (isset($this->postrequest->age)) ? $this->postrequest->age:  NULL,
                    'phone' => $phone = (isset($this->postrequest->phone)) ? $this->postrequest->phone:  NULL,
                    'first_name' => $first_name = (isset($this->postrequest->first_name)) ? $this->postrequest->first_name:  NULL,
                    'last_name' => $last_name = (isset($this->postrequest->last_name)) ? $this->postrequest->last_name:  NULL,
                    'avatar'=>'/assets/img/default-avatar.png'
                );     
    
                $this->UserModel->insert($new_user);
                $this->NodeModel->update(array('id'=>$nid,'name'=>$explode[0]));                
                $this->RoleModel->insert_user_roles(array('user_id'=>$nid,'role_id'=>5));                                
                $this->RoleModel->insert_user_roles(array('user_id'=>$nid,'role_id'=>4));                                
                $this->_user_dirs($nid);     
                
                $this->registration_mail($nid);
                
                $this->session->set_userdata('uid',$nid);
                redirect('/landing/thankyou');                
                
                /*
                $explode = explode('@',$this->postrequest->usermail);
                $pwd = $this->utils->generateRandomString();

                $nid = $this->_create(8);
                $new_user = array(
                    'node_id' => $nid,
                    'username' => $explode[0],
                    'usermail' => $this->postrequest->usermail,
                    'plainpwd' => $pwd,
                    'userpwd' => $this->utils->encrypt($pwd),
                    'age' => $age = (isset($this->postrequest->age)) ? $this->postrequest->age:  NULL,
                    'phone' => $phone = (isset($this->postrequest->phone)) ? $this->postrequest->phone:  NULL,
                    'first_name' => $first_name = (isset($this->postrequest->first_name)) ? $this->postrequest->first_name:  NULL,
                    'last_name' => $last_name = (isset($this->postrequest->last_name)) ? $this->postrequest->last_name:  NULL,
                    'avatar'=>'/assets/img/default-avatar.png'
                );     
                $this->NodeModel->update(array('id'=>$nid,'name'=>$explode[0]));
                $this->UserModel->insert($new_user);
                $this->RoleModel->insert_user_roles(array('user_id'=>$nid,'role_id'=>4));                                
                foreach($this->utils->chosenResult($this->postrequest->user_locations) as $user_location) $this->GeopointModel->insert_node_geopoint(array('node_id'=>$nid,'geopoint_id'=>$user_location));
                $this->_user_dirs($nid);   
                $this->session->set_userdata('uid',$nid);
                redirect('/landing/thankyou');   
                */
            }        
        }
        
        $locations = $this->GeopointModel->get_agent_locations();
        $data['locations'] = $this->utils->chosenOptions($locations, 'node_id', 'provincia');            
        
        $this->load->view('landing/agents',$data); 
        
    }     
    
    public function thankyou(){
        
        if(!$this->session->userdata('uid')) redirect('/landing/index');
        $nid = $this->session->userdata('uid');
        $user = $this->_get($nid,TRUE);
        $this->_get_roles($user);
        $this->_get_geopoints($user);
        $data['user'] = $user;   

        
        $this->load->view('landing/thankyou',$data); 
    }   
    
    public function login(){
        
         
        $this->form_validation->set_rules('usermail', 'Usermail', 'trim|required|xss_clean');
        $this->form_validation->set_rules('userpwd', 'Password', 'trim|required|xss_clean');        
        if ($this->form_validation->run() == FALSE) {
        } else {       
            $uid = $this->UserModel->login(trim($this->postrequest->usermail),$this->utils->encrypt(trim($this->postrequest->userpwd)));                                    
            if(!$uid){ 
                
                $this->session->set_flashdata('error',$this->lang->line('login_failed'));
                redirect('/login','refresh');                
                
            }else{
                $this->_login($uid);
                redirect('/welcome');                
            }

        } 
        
        $this->load->view('landing/login');
    }
    
    public function error(){
        $data['usermail'] = $this->session->flashdata('error');
        $this->load->view('landing/error',$data); 
    }  
    
    public function test(){
        $this->viewdata['html']['global_css'][] = array('src' => '/assets/css/landing/landing.css', 'media' => 'all');
        $this->_set_Jquery();
        $this->_set_custom_js('/assets/js/landing/landing.js');
        $this->show('landing/test'); 
    }      
    
    public function _get_roles(&$user) {
        $roles = $this->RoleModel->get_user_roles($user->id);
        $user->roles = $roles;
    }

    public function _get_geopoints(&$user) {
        $geopoints = $this->GeopointModel->get_user_geopoints($user->id);
        if($geopoints) $user->geopoints = $geopoints;            
    }
    
    public function registration_mail($nid){
        
            $user = $this->_get($nid,TRUE);
            $user->plainpwd = $this->UserModel->getplainpwd($user->id);            
            $data['user']=$user;            
            $this->load->library('parser');
            $data['subject']='Benvenuto in Gustag!';
            $data['altbody'] = $this->parser->parse('mails/registration_txt', $data,TRUE);
            $data['htmlbody'] = $this->parser->parse('mails/registration', $data,TRUE);  
            if($this->_send_mail($data)) $this->UserModel->update(array('node_id'=>$nid,'registration_mail'=>1));
            
    }    
   
}
