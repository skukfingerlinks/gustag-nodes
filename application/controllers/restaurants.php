<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Restaurants extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->_is_logged();
        if (!$this->usersession->is_owner)
            $this->_logout();
        $this->load->model('Restaurant_Model', 'RestaurantModel');
        
        
        $this->_set_custom_css('/assets/css/restaurant.css');
    }

    public function index($offset = 0) {

        if($this->usersession->is_admin){
            $count = $this->RestaurantModel->countall(FALSE, FALSE);
        }else{
            $count = $this->RestaurantModel->countall($this->usersession->id, FALSE);
        }
        if ($count) {            
            if($this->usersession->is_admin){
                $restaurants = $this->RestaurantModel->all(FALSE, FALSE, $this->config->item('items_per_page'), $offset);
            }else{
                $restaurants = $this->RestaurantModel->all($this->usersession->id, FALSE, $this->config->item('items_per_page'), $offset);
            }
            foreach ($restaurants as $restaurant) $found[] = $this->_get($restaurant->id, TRUE);
            $this->_paginate('/restaurants/index', $count, $found, $this->config->item('items_per_page'));
        }
        
        $this->viewdata['html']['h1'] = lang('restaurants');
        $this->viewdata['html']['items_type'] = 'restaurants';
        
        
        $this->_set_GMap();
        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_JqueryGeocomplete();
        $this->_set_custom_js('/assets/js/jquery.geocomplete/init.js');
        $this->_set_JqueryTablesorter();
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_restaurant.js');        
        $this->show('restaurants/index');
        
    }

    public function create() {

        $restaurant = new stdClass;
        $restaurant->type->object = 'restaurants';
        $this->_object_tags($restaurant);
        $this->_get_node_tags_chosen_options($restaurant);


        /* FORM */
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|xss_clean');
        $this->form_validation->set_rules('desc', 'Descrizione', 'trim|required|xss_clean');
        $this->form_validation->set_rules('addressinfo', 'Indirizzo', 'trim|required|xss_clean');
        $this->form_validation->set_rules('phone', 'Telefono', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else {

            //VERIFICA INDIRIZZO
            if(!isset($this->postrequest->addressinfo) OR strlen($this->postrequest->addressinfo)<1){
                $this->session->set_flashdata('error',$this->lang->line('missing_restaurant_address'));
                redirect('restaurants/create','refresh');                 
            }
            //CREA NODO
            $nid = $this->_create(1);
            //CREA RISTORANTE
            $new_restaurant = array(
                'node_id' => $nid,
                'user_id' => $this->usersession->id,
                'name' => $this->postrequest->name,
                'desc' => $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc) : NULL,
                'email' => $email = (isset($this->postrequest->email)) ? $this->postrequest->email : NULL,
                'phone' => $phone = (isset($this->postrequest->phone)) ? $this->postrequest->phone : NULL,
                'website' => $website = (isset($this->postrequest->website)) ? $this->postrequest->website : NULL,
                'seats' => $seats = (isset($this->postrequest->seats)) ? $this->postrequest->seats : NULL,
                'logo'=> $this->config->item('restaurant_default_logo'),
            );
            $new_restaurant = array_filter($new_restaurant);
            $this->RestaurantModel->insert($new_restaurant);
            //AGGIORNA NODO
            $this->NodeModel->update(array('id' => $nid, 'name' => $this->postrequest->name));
            //DEFINISCI GIORNI DI CHIUSURA
            $this->_set_days($nid);
            //DEFINISCI TAGS
            $this->_set_tags($nid);
            //DEFINISCI GEOPOINT
            $this->_set_geopoint($nid);
            //DEFINISCI DIRECTORIES/LOGO RISTORANTE
            $this->_restaurant_dirs($nid);
            $this->_logo($nid);
            //REDIRIGI A CALENDAR
            $this->session->set_flashdata('success',$this->lang->line('create_success'));
            //redirect('restaurants/edit/'.$nid,'refresh');
            redirect('/restaurants','refresh');
        }
        /* END FORM */
        
        foreach ($this->week_days as $k => $v) $this->viewdata['week_days'][$k . '-' . $v] = $v;
        $this->viewdata['data'] = $restaurant;
        $this->_set_GMap();
        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_JqueryGeocomplete();
        $this->_set_custom_js('/assets/js/jquery.geocomplete/init.js');
        
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_restaurant.js');

        $this->show('restaurants/create');
    }

    public function edit($nid) {
        
        $restaurant = $this->_get($nid, TRUE);
        $owners = $this->UserModel->all(FALSE, 3, FALSE);
        foreach($owners as $owner) $this->viewdata['owners'][$owner->node_id]=$owner->username;
        $this->viewdata['owner'][$restaurant->users[0]->node_id]=$restaurant->users[0]->username;
        //$this->utils->dump($this->viewdata);die;
        
                
        $this->viewdata['owner'][]=$restaurant->users[0]->node_id.'-'.$restaurant->users[0]->username;
        
        $owners = $this->UserModel->all(FALSE, 3, FALSE);        
        $this->viewdata['owners']=$this->utils->chosenOptions($owners,'id','name');
        
        
        
        //$this->_object_tags($restaurant);
        //$this->_get_node_tags_chosen_options($restaurant);  

        
        if(!$this->usersession->is_admin) if($this->usersession->id!==$restaurant->owner_id) $this->_logout ();
        
        
        
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|xss_clean');
        $this->form_validation->set_rules('desc', 'Descrizione', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {            
        } else {
            
            $new_owner = explode('-',$this->postrequest->owner);
            
            $edit_restaurant = array(
                'node_id' => $nid,
                'name' => $this->postrequest->name,
                'desc' => $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc) : NULL,
                'email' => $email = (isset($this->postrequest->email)) ? $this->postrequest->email : NULL,
                'phone' => $phone = (isset($this->postrequest->phone)) ? $this->postrequest->phone : NULL,
                'website' => $website = (isset($this->postrequest->website)) ? $this->postrequest->website : NULL,
                'seats' => $seats = (isset($this->postrequest->seats)) ? $this->postrequest->seats : NULL,
            );
            
            $edit_restaurant = array_filter($edit_restaurant);
            $this->RestaurantModel->update($edit_restaurant);
            $this->NodeModel->update(array('id' => $nid, 'name' => $this->postrequest->name,'owner_id'=>$new_owner[0]));


            $this->_set_days($nid);
            $this->_set_tags($nid);
            $this->_set_geopoint($nid);

            $this->_restaurant_dirs($nid);
            $this->_logo($nid);
            
            $this->session->set_flashdata('success',$this->lang->line('update_success'));
            redirect('restaurants/edit/'.$nid,'refresh');            
        }        
        
        foreach ($this->week_days as $k => $v) $this->viewdata['week_days'][$k . '-' . $v] = $v;
        foreach($restaurant->days as $day) $this->viewdata['closed_days'][]=$day.'-'.$this->week_days[$day];
        $this->viewdata['data'] = $restaurant;
        $this->viewdata['owner'] = $restaurant->users[0];
        
        
        
        $this->_set_GMap();
        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_JqueryGeocomplete();
        $this->_set_custom_js('/assets/js/jquery.geocomplete/init.js');
        $this->_set_JqueryTablesorter();
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_restaurant.js');        
        
        $this->show('restaurants/edit'); 
        
        //$this->utils->dump($restaurant);
    }

    public function calendar($nid){
        
        $restaurant = $this->_get($nid, TRUE);
        
        //$this->_object_tags($restaurant);
        //$this->_get_node_tags_chosen_options($restaurant);          
        
        if(!$this->usersession->is_admin) if($this->usersession->id!==$restaurant->owner_id) $this->_logout ();
        
        $year = ($this->uri->segment(4) === FALSE) ? date('Y') : $this->uri->segment(4);
        $month = ($this->uri->segment(5) === FALSE) ? date('m') : $this->uri->segment(5);
        $yearmonth=$year.'/'.$month;        
        
        $this->calendar_options = array (
        'show_next_prev'  => TRUE,
        'next_prev_url'   => site_url('restaurants/calendar/'.$nid.'/'),
        'day_type'        => 'long',
        'template'=>
        '
        {table_open}
            <table class="calendar">
        {/table_open}
	{week_day_cell}
            <th class="day_header">{week_day}</th>
        {/week_day_cell}
	{cal_cell_content}
            <a href="#" title="'.$yearmonth.'/{day}" class="pick closed">{day}</a>
        {/cal_cell_content}
	{cal_cell_content_today}
            <a href="#" title="'.$yearmonth.'/{day}" class="pick closed today">{day}</a>
        {/cal_cell_content_today}
	{cal_cell_no_content}
            <a href="#" title="'.$yearmonth.'/{day}" class="pick">{day}</a>
        {/cal_cell_no_content}
	{cal_cell_no_content_today}
            <div class="today"><a href="#" title="'.$yearmonth.'/{day}" class="pick">{day}</a></div>
        {/cal_cell_no_content_today}            
        '
        );
        
        $closed_days=array();
        if(isset($restaurant->dates)){
            foreach($restaurant->dates as $date){              
                $closed_year = date('Y',$date->start/1000);
                $closed_month = date('m',$date->start/1000);
                $closed_day = ltrim(date('d',$date->start/1000), '0');            
                if($year==$closed_year AND $month==$closed_month) $closed_days[$closed_day]='Chiuso';
            }
            $this->viewdata['closed_days']=sizeof($restaurant->dates);
        }
        $startDate = date("$year-$month-01");        
        foreach($this->utils->thismonthdates($startDate,'+ 3 month') as $monthday){            
            if(in_array((int)$monthday->weekday,$restaurant->days)){
                $start = date('Y-m-d 00:00:00',strtotime($monthday->date));
                $end = date('Y-m-d 23:59:59',strtotime($monthday->date));
                $startlong=(strtotime($start)*1000);
                $endlong=(strtotime($end)*1000);
                $date_closed = array(
                    'restaurant_id'=>$nid,
                    'start'=>$startlong,
                    'end'=>$endlong,
                );
                if($this->RestaurantModel->check_date($nid,$startlong,$endlong)) $this->RestaurantModel->insert_dates($date_closed);
                if((int)$monthday->month==(int)$month) $closed_days[(int)$monthday->day]='Chiuso';
            }
        }        
        $this->viewdata['data']=$restaurant;
        $this->viewdata['owner'] = $restaurant->users[0];
        $this->viewdata['nid']=$nid;
        $this->load->library('calendar',$this->calendar_options);
        $this->viewdata['calendar'] = $this->calendar->generate($year, $month, $closed_days);           
        $this->_set_Jquery();
        $this->_set_GMap();
        $this->_set_custom_js('/assets/js/custom/showMap.js');        
        $this->_set_custom_js('/assets/js/custom/calendar.js');        
        
        $this->show('restaurants/calendar');
        
        //$this->utils->dump($restaurant);
    }
    
    public function gallery($nid) {
        
        $restaurant = $this->_get($nid, TRUE);
        
        $this->form_validation->set_rules('userfile', 'Immagine', '');
        if ($this->form_validation->run() == FALSE) {            
        } else {            
            if (isset($this->filesrequest->userfile) AND strlen($this->filesrequest->userfile->name) > 0) {
                $this->_restaurant_dirs($nid);
                $gallery_path=$this->config->item('restaurant_dirs_path') . $nid . '/gallery/';
                $image = $this->_image_upload($gallery_path);  
                $this->_set_gallery_image($nid,$gallery_path,$image,$this->config->item('restaurant_dirs_url'));
                $this->session->set_flashdata('success',$this->lang->line('upload_success'));
                redirect('/restaurants/gallery/'.$nid,'refresh');
            }else{
                $this->session->set_flashdata('error',$this->lang->line('missing_upload_image'));
                redirect('/restaurants/gallery/'.$nid,'refresh');
            }
        }        
        
        
        
        if(!$this->usersession->is_admin) if($this->usersession->id!==$restaurant->owner_id) $this->_logout ();
        $this->viewdata['data'] = $restaurant;
        
        //$this->utils->dump($restaurant->media);die;
        
        $this->viewdata['owner'] = $restaurant->users[0];
        $this->_set_Jquery();       
        $this->show('restaurants/gallery'); 
        
        
    }    
    
    protected function _set_days($nid, $update = FALSE) {
        if (isset($this->postrequest->closing_days)) {
            $days = $this->utils->chosenResult($this->postrequest->closing_days);
            $this->RestaurantModel->delete_days($nid);
            foreach ($days as $day)
                $this->RestaurantModel->insert_days(array('restaurant_id' => $nid, 'day' => $day));
        }
    }
    
    public function removedates($nid){
        $this->RestaurantModel->delete_dates($nid);
        $this->session->set_flashdata('success',$this->lang->line('dates_delete_success'));
        redirect('/restaurants/calendar/'.$nid,'refresh');            
    }     
    
    public function remove($nid){        
        $dir = $this->config->item('restaurant_dirs_path').$nid;
        if(file_exists($dir)) $this->utils->rrmdir($dir);
    }
    
    public function delete($nid){                  
        $this->remove($nid);
        $this->NodeModel->delete($nid);
        $this->session->set_flashdata('success',$this->lang->line('removal_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }    
    
    public function activate($nid){  
        $this->NodeModel->update(array('id'=>$nid,'active'=>1));
        $this->session->set_flashdata('success',$this->lang->line('activate_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }      

    public function deactivate($nid){   
        $this->NodeModel->update(array('id'=>$nid,'active'=>0));
        $this->session->set_flashdata('success',$this->lang->line('deactivate_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }     
    public function week(){
        $this->viewdata['showuserinfo']=FALSE;
        unset($this->viewdata['showuserinfo']);        
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        $this->show('restaurants/week');
    }    
    
    public function deleteimage($nid){  
        
        $image = $this->_get($nid);
        unlink($image->src);
        $this->NodeModel->delete($image->id);
        $this->session->set_flashdata('success',$this->lang->line('removal_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }    


}
