<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Owner extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->_is_logged();        
        if(!$this->usersession->is_owner) $this->_logout();
    }    
   
    
    public function index() {
        $this->utils->dump($this->usersession);
        die;
    }

}
