<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menus extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->_is_logged();
        if (!$this->usersession->is_owner)
            $this->_logout();
        $this->load->model('Menu_Model', 'MenuModel');
    }

    public function index($offset = 0) {

        if ($this->usersession->is_admin) {
            $count = $this->MenuModel->countall(FALSE, FALSE);
        } else {
            $count = $this->MenuModel->countall($this->usersession->id, FALSE);
        }
        if ($count) {
            if ($this->usersession->is_admin) {
                $menus = $this->MenuModel->all(FALSE, FALSE, $this->config->item('items_per_page'), $offset);
            } else {
                $menus = $this->MenuModel->all($this->usersession->id, FALSE, $this->config->item('items_per_page'), $offset);
            }
            foreach ($menus as $menu)
                $found[] = $this->_get($menu->id, TRUE);
            $this->_paginate('/menus/index', $count, $found, $this->config->item('items_per_page'));
        }
        $this->viewdata['html']['h1'] = 'Men&ugrave;';
        $this->viewdata['html']['items_type'] = 'menus';

        //$this->utils->dump($this->viewdata);die;

        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        $this->show('menus/index');
    }

    public function restaurant($nid, $offset = 0) {

        $restaurant = $this->_get($nid, TRUE);

        
        
        if (!$restaurant->dishes) {
            $this->session->set_flashdata('error', $this->lang->line('missing_restaurant_dishes'));
            redirect('dishes/create/' . $nid, 'refresh');
        }


        $this->viewdata['restaurant'] = $restaurant;
        $this->viewdata['owner'] = $restaurant->users[0];
        $found = array();
        $count = $this->MenuModel->restaurant_countall($nid, FALSE);
        $menus = $this->MenuModel->restaurant_all($nid, FALSE, $this->config->item('items_per_page'), $offset);
        foreach ($menus as $menu)
            $found[] = $this->_get($menu->id, TRUE);
        if ($found)
            //$this->utils->dump($found);die;
            $this->_paginate('/menus/index', $count, $found, $this->config->item('items_per_page'));

        $this->viewdata['html']['h1'] = 'Men&ugrave;';
        $this->viewdata['html']['items_type'] = 'menus';

        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_GMap();
        $this->_set_JqueryTablesorter();
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->_set_custom_js('/assets/js/custom/dashboardmap.js');

        //$this->utils->dump($this->viewdata);die;

        $this->show('menus/restaurant');
    }

    public function create($nid) {
        $restaurant = $this->_get($nid, TRUE);

        if (!$restaurant->dishes) {
            $this->session->set_flashdata('error', $this->lang->line('missing_restaurant_dishes'));
            redirect('dishes/create/' . $nid, 'refresh');
        }

        $this->viewdata['restaurant'] = $restaurant;
        $this->viewdata['owner'] = $restaurant->users[0];

        $menu = new stdClass;
        $menu->type->object = 'menus';
        $this->_object_tags($menu);
        $this->_get_node_tags_chosen_options($menu);
        $this->_get_dishes($menu);

        $this->form_validation->set_rules('name', 'Nome', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {

        } else {
            
            //$this->utils->dump($this->postrequest);die;

            //VERIFICA TYPE MENU
            if(!isset($this->postrequest->menu_type) OR sizeof($this->postrequest->menu_type)<1){
                $this->session->set_flashdata('error',$this->lang->line('missing_menu_type'));
                redirect('menus/create/' . $nid, 'refresh');
            }             
            
            //VERIFICA TIME MENU
            if(!isset($this->postrequest->menu_time) OR sizeof($this->postrequest->menu_time)<1){
                $this->session->set_flashdata('error',$this->lang->line('missing_menu_time'));
                redirect('menus/create/' . $nid, 'refresh');
            }             
            
            //VERIFICA PIATTI
            if(!isset($this->postrequest->dishes) OR sizeof($this->postrequest->dishes)<1){
                $this->session->set_flashdata('error',$this->lang->line('missing_menu_dishes'));
                redirect('menus/create/' . $nid, 'refresh');
            }             
            
            //VERIFICA ACTIVE DAYS
            if(!isset($this->postrequest->active_days) OR sizeof($this->postrequest->active_days)<1){
                $this->session->set_flashdata('error',$this->lang->line('missing_menu_active_days'));
                redirect('menus/create/' . $nid, 'refresh');
            }           
            
            
            $new_nid = $this->_create(4);
            $new_menu = array(
                'node_id' => $new_nid,
                'restaurant_id' => $nid,
                'name' => $this->postrequest->name,
                'desc' => $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc) : NULL,
            );
            $new_menu = array_filter($new_menu);
            $this->MenuModel->insert($new_menu);

            //$this->utils->dump($this->postrequest);

            $active_days = $this->utils->chosenResult($this->postrequest->active_days);
            foreach ($active_days as $day)
                $this->MenuModel->insert_days(array('menu_id' => $new_nid, 'day' => $day));

            $this->_set_tags($new_nid);


            if (isset($this->postrequest->dishes) AND sizeof($this->postrequest->dishes) > 0) {
                $dishes = $this->utils->chosenResult($this->postrequest->dishes);
                foreach ($dishes as $dish) {
                    $this->MenuModel->insert_dishes(array('menu_id' => $new_nid, 'dish_id' => $dish));
                    $this->load->model('Restaurant_Model', 'RestaurantModel');
                    $this->RestaurantModel->insert_restaurants_dishes(array('restaurant_id' => $nid, 'dish_id' => $dish));
                }
            }

            $this->session->set_flashdata('success', $this->lang->line('create_success'));
            redirect('menus/restaurant/'.$nid,'refresh');
        }

        foreach ($this->week_days as $k => $v)
            $this->viewdata['week_days'][$k . '-' . $v] = $v;
        $this->viewdata['data'] = $menu;
        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_menu.js');

        //$this->utils->dump($this->viewdata);die;

        $this->show('menus/create');

        //$this->utils->dump($this->viewdata['dishes']);die;
        //$this->utils->dump($menu);die;
    }

    public function edit($nid) {


        $menu = $this->_get($nid, TRUE);
        $this->_object_tags($menu);
        $this->_get_node_tags_chosen_options($menu);

        $restaurant = $this->_get($menu->restaurant_id, TRUE);

        if (!$this->usersession->is_admin)
            if ($this->usersession->id !== $restaurant->owner_id)
                $this->_logout();

        $this->viewdata['restaurant'] = $restaurant;
        $this->viewdata['owner'] = $restaurant->users[0];

        $days = $this->MenuModel->get_days($menu->id);
        if ($days)
            $menu->days = $days;

        $this->form_validation->set_rules('name', 'Nome', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {

        } else {

            //$this->utils->dump($this->postrequest);die;
            
            
         
            
            

            //VERIFICA TYPE MENU
            if(!isset($this->postrequest->menu_type) OR sizeof($this->postrequest->menu_type)<1){
                $this->session->set_flashdata('error',$this->lang->line('missing_menu_type'));
                redirect('menus/edit/' . $nid, 'refresh');
            }             
            
            //VERIFICA TIME MENU
            if(!isset($this->postrequest->menu_time) OR sizeof($this->postrequest->menu_time)<1){
                $this->session->set_flashdata('error',$this->lang->line('missing_menu_time'));
                redirect('menus/edit/' . $nid, 'refresh');
            }             
            
            //VERIFICA PIATTI
            if(!isset($this->postrequest->dishes) OR sizeof($this->postrequest->dishes)<1){
                $this->session->set_flashdata('error',$this->lang->line('missing_menu_dishes'));
                redirect('menus/edit/' . $nid, 'refresh');
            }             
            
            //VERIFICA ACTIVE DAYS
            if(!isset($this->postrequest->active_days) OR sizeof($this->postrequest->active_days)<1){
                $this->session->set_flashdata('error',$this->lang->line('missing_menu_active_days'));
                redirect('menus/edit/' . $nid, 'refresh');
            }            
            
            $edit_menu = array(
                'node_id' => $nid,
                'name' => $this->postrequest->name,
                'desc' => $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc) : NULL,
            );
            $edit_menu = array_filter($edit_menu);
            $this->MenuModel->update($edit_menu);
            $this->NodeModel->update(array('id' => $nid, 'name' => $this->postrequest->name, 'updated' => $this->utils->dateLong()));

            $active_days = $this->utils->chosenResult($this->postrequest->active_days);
            $this->MenuModel->delete_days($nid);
            foreach ($active_days as $day)
                $this->MenuModel->insert_days(array('menu_id' => $nid, 'day' => $day));

            $this->_set_tags($nid);

            $dishes = $this->utils->chosenResult($this->postrequest->dishes);
            $this->MenuModel->delete_dishes($nid);
            foreach ($dishes as $dish)
                $this->MenuModel->insert_dishes(array('menu_id' => $nid, 'dish_id' => $dish));
            $this->session->set_flashdata('success', $this->lang->line('update_success'));
            redirect('menus/edit/' . $nid, 'refresh');
        }

        $this->viewdata['data'] = $menu;

        foreach ($this->week_days as $k => $v)
            $this->viewdata['week_days'][$k . '-' . $v] = $v;
        foreach ($menu->days as $day)
            $this->viewdata['menu_days'][] = $day->day . '-' . $this->week_days[$day->day];
        $this->viewdata['menu_time_selected'] = $this->utils->chosenOptions($menu->menu_time);
        $this->viewdata['menu_type_selected'] = $this->utils->chosenOptions($menu->menu_type);
        $dishtypes = $this->TagModel->get_tag_by_id(20);
        foreach ($dishtypes as $dishtype) {
            $this->load->model('Dish_Model', 'DishModel');
            $dishes = $this->DishModel->get_dishes_by_type($dishtype->id, $this->usersession->id);
            if ($dishes)
                $this->viewdata['dishes'][$dishtype->name] = $this->utils->chosenOptions($dishes, 'node_id');
        }
        foreach ($menu->dishes as $dish)
            $this->viewdata['menu_dishes'][] = $dish->node_id . '-' . $dish->name;

        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_menu.js');
        $this->show('menus/edit');

        //$this->utils->dump($this->viewdata);die;
        //$this->utils->dump($menu);
        //die;
    }

    function _get_dishes(&$node) {

        $dishtypes = $this->TagModel->get_tag_by_id(20);
        foreach ($dishtypes as $dishtype) {
            $this->load->model('Dish_Model', 'DishModel');
            $dishes = $this->DishModel->get_dishes_by_type($dishtype->id, $this->usersession->id);
            if ($dishes)
                $this->viewdata['dishes'][$dishtype->name] = $this->utils->chosenOptions($dishes, 'node_id');
        }
    }
    
    public function delete($nid){  
        $this->NodeModel->delete($nid);
        $this->session->set_flashdata('success',$this->lang->line('removal_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }    
    
    public function activate($nid){  
        $this->NodeModel->update(array('id'=>$nid,'active'=>1));
        $this->session->set_flashdata('success',$this->lang->line('activate_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }      

    public function deactivate($nid){   
        $this->NodeModel->update(array('id'=>$nid,'active'=>0));
        $this->session->set_flashdata('success',$this->lang->line('deactivate_success'));
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }    

}

