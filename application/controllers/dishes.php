<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dishes extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->_is_logged();
        if (!$this->usersession->is_owner)
            $this->_logout();
        $this->load->model('Dish_Model','DishModel');
    }   
   
    
    public function index($offset = 0) {

        if($this->usersession->is_admin){
            $count = $this->DishModel->countall(FALSE, TRUE);
        }else{
            $count = $this->DishModel->countall($this->usersession->id, TRUE);
        }
        if ($count) {            
            if($this->usersession->is_admin){
                $dishes = $this->DishModel->all(FALSE, TRUE, $this->config->item('items_per_page'), $offset);
            }else{
                $dishes = $this->DishModel->all($this->usersession->id, TRUE, $this->config->item('items_per_page'), $offset);
            }
            foreach ($dishes as $dish)
                $found[] = $this->_get($dish->id, TRUE);
            $this->_paginate('/dishes/index', $count, $found, $this->config->item('items_per_page'));
        }
        $this->viewdata['html']['h1'] = 'Piatti';
        $this->viewdata['html']['items_type'] = 'dishes';
        //$this->utils->dump($this->viewdata);die;
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        $this->show('dishes/index');
    }
    
    public function restaurant($nid,$offset=0){
        
        
        $this->session->set_userdata('actual_restaurant',$nid);
        
        $restaurant = $this->_get($nid,TRUE);
        $this->viewdata['restaurant'] = $restaurant;        
        $found=array();
        $count=$this->DishModel->restaurant_countall($nid,TRUE);        
        $dishes=$this->DishModel->restaurant_all($nid,TRUE,$this->config->item('items_per_page'),$offset);
        foreach($dishes as $dish) $found[] = $this->_get($dish->id,TRUE); 
        
        //$this->utils->dump($found);die;
        
        if($found) $this->_paginate('/dishes/index',$count,$found,$this->config->item('items_per_page'));            
        //$this->utils->dump($found);die;        
        $this->viewdata['html']['h1'] = 'Piatti';
        $this->viewdata['html']['items_type'] = 'dishes';
        $this->viewdata['owner'] = $restaurant->users[0];
        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_GMap();
        $this->_set_JqueryTablesorter();
        $this->_set_custom_js('/assets/js/custom/showMap.js');
        $this->_set_custom_js('/assets/js/custom/dashboardmap.js');  
        
               
        $this->show('dishes/restaurant'); 
                
    }   
    
    public function create($nid){
        
        $restaurant = $this->_get($nid,TRUE);
        if(!$this->usersession->is_admin) if($this->usersession->id!==$restaurant->owner_id) $this->_logout ();
        
        $dish = new stdClass;
        $dish->type->object = 'dishes';
        $this->_object_tags($dish);
        $this->_get_node_tags_chosen_options($dish);
        $this->_get_dish_types($dish);
        
        
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else {   
            
            
            //DEFINISCO SE IL PIATTO E' PRIVATO O PUBBLICO E CREO NODO TIPO DISH
            //$private = (isset($this->postrequest->owner) AND $this->postrequest->owner='private') ? $this->usersession->id : NULL;            
            $private = (isset($this->postrequest->owner) AND $this->postrequest->owner='private') ? $this->usersession->id : FALSE;            
            if($this->usersession->is_admin) $private=NULL;
            //CREO NODO
            $new_nid = $this->_create(3,$private);            
            //DEFINISCO IL TIPO PIATTO $this->postrequest->dish_type
            $dish_type = $this->utils->chosenResult($this->postrequest->dish_type);            
            //CREO PIATTO
            $new_dish=array(
                'node_id'=>$new_nid,     
                //'user_id'=>$private,  
                'dish_type'=>$dish_type[0],
                'name'=>$this->postrequest->name,
                'desc'=> $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc): NULL,
                'logo'=> $this->config->item('dish_default_logo'),
            );        
            $new_dish=array_filter($new_dish);
            $this->DishModel->insert($new_dish);    
            //ASSOCIO TAGS
            $this->_set_tags($new_nid);            
            //ASSOCIO PIATTO RISTORANTE
            $this->load->model('Restaurant_Model','RestaurantModel');
            $this->RestaurantModel->insert_restaurants_dishes(array('restaurant_id'=>$nid,'dish_id'=>$new_nid));
            //CREO DIRECTORY/LOGO PIATTO
            $this->_dish_dirs($new_nid);
            $this->_dishlogo($new_nid);             
            //REDIRIGO
            $this->session->set_flashdata('success',$this->lang->line('create_success'));
            redirect('dishes/restaurant/'.$nid,'refresh');
            
        }        
        
        foreach($dish->dish_types as $dish_type) $this->viewdata['types_dish'][$dish_type->node_id]=$dish_type->name;    
        $dish_types = $this->TagModel->get_tag_by_id(20);
        $this->viewdata['dish_types'] = $this->utils->chosenOptions($dish_types);         
        $this->viewdata['data'] = $dish;        
        $this->viewdata['restaurant'] = $restaurant;      
        $this->viewdata['owner'] = $restaurant->users[0];
        $this->_set_Jquery();
        $this->_set_custom_js('/assets/js/custom/calendar.js');
        $this->_set_ChosenJS();       
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_dish.js');
        $this->_set_custom_js('/assets/js/custom/tooltip.js');
        $this->show('dishes/create');        
        

    }
    
public function edit($nid) {
        
        
        $restaurant = $this->_get($this->session->userdata('actual_restaurant'),TRUE);
        //$this->utils->dump($restaurant);
        $dish = $this->_get($nid,TRUE);
        $this->_object_tags($dish);
        $this->_get_node_tags_chosen_options($dish);
        $this->_get_dish_types($dish);
        
        
        //$this->utils->dump($dish);die;
        
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else { 
            
            //DEFINISCO SE IL PIATTO E' PRIVATO O PUBBLICO
            $private = (isset($this->postrequest->owner) AND $this->postrequest->owner='private') ? $this->usersession->id : NULL;    
            if($this->usersession->is_admin) $private=NULL;
            if(!is_null($private)) $this->NodeModel->update(array('id'=>$nid,'owner_id'=>$private));
            
            //CREO IL DISH
            $update_dish=array(
                'node_id'=>$nid,     
                //'user_id'=>$private,  
                'dish_type'=>$this->postrequest->dish_type,
                'name'=>$this->postrequest->name,
                'desc'=> $desc = (isset($this->postrequest->desc)) ? strip_tags($this->postrequest->desc): NULL,
            );        
            $update_dish=array_filter($update_dish);
            $this->DishModel->update($update_dish);
            $this->NodeModel->update(array('id'=>$nid,'name'=>$this->postrequest->name,'updated'=>$this->utils->dateLong()));
            $this->_set_tags($nid);          
            
            $this->_dish_dirs($nid);
            $this->_dishlogo($nid);             
            
            $this->session->set_flashdata('success',$this->lang->line('update_success'));
            redirect('dishes/edit/'.$nid,'refresh');            
            
            //$this->utils->dump($this->postrequest);die;
        }

        $dish_types = $this->TagModel->get_tag_by_id(20);
        $this->viewdata['type_of_dish'] = $dish->dish_type[0]->id;
        foreach($dish->dish_types as $dish_type) $this->viewdata['dish_type'][$dish_type->node_id]=$dish_type->name;
        $this->viewdata['data'] = $dish;        
        $this->viewdata['restaurant'] = $restaurant; 
        $this->viewdata['owner'] = $restaurant->users[0];
        
        
        
        $this->_set_Jquery();
        $this->_set_ChosenJS();        
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_dish.js');
        
        
        $this->show('dishes/edit');        
        
        
        //
        
    }        

}
