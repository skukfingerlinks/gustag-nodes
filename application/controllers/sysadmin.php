<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sysadmin extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->_is_logged();        
        if(!$this->usersession->is_sysadmin) $this->_logout();
    }    
   
    
    public function index() {
        $this->utils->dump($this->usersession);
        die;
    }

}
