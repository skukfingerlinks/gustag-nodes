<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends Main_Controller {

    public function __construct() {
        parent::__construct();
        $this->_is_logged();
        $this->_set_custom_css('/assets/css/users.css');
        $this->viewdata['controller_help']=$this->lang->line('users_help');
    }
    
    public function index($offset = 0) {

        if(!$this->usersession->is_admin) $this->_logout();
        
        $count = $this->UserModel->countall(FALSE, 5, FALSE);
        $users = $this->UserModel->all(FALSE, 5, FALSE, $this->config->item('items_per_page'), $offset);
        foreach ($users as $user)
            $found[] = $this->_get($user->node_id, TRUE);
        
        $this->_paginate('/users/index', $count, $found, $this->config->item('items_per_page'));
        $this->viewdata['html']['h1'] = $this->lang->line('users');
        $this->viewdata['html']['items_type'] = 'users';
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        //$this->utils->dump($this->viewdata['html']['found']);  die;
        $this->show('users/index');
    }

    public function admins($offset = 0) {

        if(!$this->usersession->is_admin) $this->_logout();
        
        $count = $this->UserModel->countall(FALSE, 2, FALSE);
        $users = $this->UserModel->all(FALSE, 2, FALSE, $this->config->item('items_per_page'), $offset);
        foreach ($users as $user)
            $found[] = $this->_get($user->node_id, TRUE);

        $this->_paginate('/users/admins', $count, $found, $this->config->item('items_per_page'));
        $this->viewdata['html']['h1'] = $this->lang->line('admins');
        $this->viewdata['html']['items_type'] = 'admins';
        $this->viewdata['html']['role'] = 'admin';
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        //$this->utils->dump($this->viewdata['html']['found']);  die;
        $this->show('users/index');
    }

    public function owners($offset = 0) {

        if(!$this->usersession->is_admin) $this->_logout();
        
        $count = $this->UserModel->countall(FALSE, 3, FALSE);
        $users = $this->UserModel->all(FALSE, 3, FALSE, $this->config->item('items_per_page'), $offset);
        foreach ($users as $user)
            $found[] = $this->_get($user->node_id, TRUE);

        $this->_paginate('/users/owners', $count, $found, $this->config->item('items_per_page'));
        $this->viewdata['html']['h1'] = $this->lang->line('owners');
        $this->viewdata['html']['items_type'] = 'owners';
        $this->viewdata['html']['role'] = 'owner';
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        //$this->utils->dump($this->viewdata);
        $this->show('users/index');
    }

    public function agents($offset = 0) {

        if(!$this->usersession->is_admin) $this->_logout();
        
        $count = $this->UserModel->countall(FALSE, 4, FALSE);
        $users = $this->UserModel->all(FALSE, 4, FALSE, $this->config->item('items_per_page'), $offset);
        foreach ($users as $user)
            $found[] = $this->_get($user->node_id, TRUE);
        //$this->utils->dump($found);

        $this->_paginate('/users/agents', $count, $found, $this->config->item('items_per_page'));
        $this->viewdata['html']['h1'] = $this->lang->line('agents');
        $this->viewdata['html']['items_type'] = 'agents';
        $this->viewdata['html']['role'] = 'agent';
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        $this->show('users/index');
    }

    public function users($offset = 0) {

        echo 'users';die;
        
        if(!$this->usersession->is_admin) $this->_logout();
        
        $count = $this->UserModel->countall(FALSE, 5, FALSE);
        $users = $this->UserModel->all(FALSE, 5, FALSE, $this->config->item('items_per_page'), $offset);
        
        $this->utils->dump($users);die;
        
        foreach ($users as $user)
            $found[] = $this->_get($user->node_id, TRUE);

        $this->_paginate('/users/users', $count, $found, $this->config->item('items_per_page'));
        $this->viewdata['html']['h1'] = $this->lang->line('users');
        $this->viewdata['html']['items_type'] = 'users';
        $this->viewdata['html']['role'] = 'user';
        $this->_set_Jquery();
        $this->_set_JqueryTablesorter();
        $this->show('users/index');
    }

    public function profile() {

        $nid = $this->usersession->id;
        $user = $this->_get($nid, TRUE);

        if (isset($user->geopoints)) {
            foreach ($user->geopoints as $geopoint)
                $this->viewdata['user_geopoints'][] = $geopoint->node_id . '-' . $geopoint->administrative_area_level_3;
            $locations = $this->GeopointModel->locations();
            $this->viewdata['locations'] = $this->utils->chosenOptions($locations, 'node_id', 'provincia');
        }

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('usermail', 'Usermail', 'trim|required|xss_clean');
        $this->form_validation->set_rules('plainpwd', 'Password', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else {

            if ($this->postrequest->username !== $user->username AND !$this->UserModel->isunique('username', $this->postrequest->username)) {
                $this->session->set_flashdata('error', $this->lang->line('username_not_unique'));
                redirect('/users/profile', 'refresh');
            }
            if ($this->postrequest->usermail !== $user->usermail AND !$this->UserModel->isunique('usermail', $this->postrequest->usermail)) {
                $this->session->set_flashdata('error', $this->lang->line('usermail_not_unique'));
                redirect('/users/profile', 'refresh');
            }


            //CREA UTENTE
            $update_user = array(
                'node_id' => $nid,
                'username' => $this->postrequest->username,
                'usermail' => $this->postrequest->usermail,
                'plainpwd' => $this->postrequest->plainpwd,
                'userpwd' => $this->utils->encrypt($this->postrequest->plainpwd),
                'first_name' => $first_name = (isset($this->postrequest->first_name)) ? $this->postrequest->first_name : NULL,
                'last_name' => $last_name = (isset($this->postrequest->last_name)) ? $this->postrequest->last_name : NULL,
                'status' => $status = (isset($this->postrequest->status)) ? strip_tags($this->postrequest->status) : NULL,
                'phone' => $phone = (isset($this->postrequest->phone)) ? $this->postrequest->phone : NULL,
            );

            $update_user = array_filter($update_user);
            $this->UserModel->update($update_user);

            $this->NodeModel->update(array('id' => $nid, 'name' => $this->postrequest->username));

            $this->RoleModel->delete_user_roles($nid);
            foreach ($this->postrequest->user_roles as $role) {
                $user_role = array(
                    'user_id' => $nid,
                    'role_id' => $role
                );
                $this->RoleModel->insert_user_roles($user_role);
            }

            $this->_user_dirs($nid);
            $this->_avatar($nid);

            if (isset($this->postrequest->user_locations)) {
                $this->GeopointModel->delete_node_geopoint($nid);
                foreach ($this->utils->chosenResult($this->postrequest->user_locations) as $location) {
                    $user_geopoint = array(
                        'node_id' => $nid,
                        'geopoint_id' => $location,
                    );
                    $this->GeopointModel->insert_node_geopoint($user_geopoint);
                }
            }

            $this->session->set_flashdata('success', $this->lang->line('update_success'));
            redirect('/users/profile', 'refresh');
        }

        $this->viewdata['data'] = $user;
        $user_roles = $this->RoleModel->user_roles();
        $this->viewdata['user_roles'] = $this->utils->chosenOptions($user_roles, 'id', 'description');
        foreach ($user->roles as $role)
            $this->viewdata['roles_user'][] = $role->id . '-' . $role->description;

        //$this->utils->dump($this->viewdata);die;

        $this->viewdata['plainpwd'] = $this->UserModel->getplainpwd($user->id);
        
        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_user.js');
        $this->show('/users/profile');
    }

    public function edit($nid) {

        if(!$this->usersession->is_admin) $this->_logout();

        $user = $this->_get($nid, TRUE);
        //$this->utils->dump($user);die;
        //$this->utils->jsonResponse($user);die;

        foreach ($user->restaurants as $restaurant) $this->viewdata['user_restaurants'][]=$restaurant->node_id.'-'.$restaurant->name;
        $this->load->model('Restaurant_Model','RestaurantModel');
        $all_restaurants = $this->RestaurantModel->user_restaurants(FALSE,FALSE);
        foreach ($all_restaurants as $restaurants) $this->_clean_obj($restaurants);
        $this->viewdata['all_restaurants'] = $this->utils->chosenOptions($all_restaurants,'node_id','name');

        //$this->utils->dump($this->viewdata['user_restaurants']);
        if($restaurants) $user->restaurants = $restaurants;

        
        
        
        //$this->utils->dump($user);die;

        foreach($user->roles as $role){
            //AGENT
            if (in_array($role->id, array(4))){

                $this->viewdata['is_agent']=TRUE;
                $locations = $this->GeopointModel->locations();
                $this->viewdata['locations'] = $this->utils->chosenOptions($locations, 'node_id', 'provincia');

                //GET USER OWNERS
                if($user->owners){
                    foreach($user->owners as $owner){
                        $this->viewdata['user_owners'][] = $owner->node_id.'-'.$owner->username;
                    }
                }
                //GET ALL OWNERS
                $owners = $this->UserModel->all(FALSE, 3, FALSE);
                if($owners) $this->viewdata['all_owners'] = $this->utils->chosenOptions($owners,'node_id','username');
            }
            if (in_array($role->id, array(3))){

                $this->viewdata['is_owner']=TRUE;
                //GET USER RESTAURANTS
                foreach ($user->restaurants as $restaurant) $this->viewdata['user_restaurants'][]=$restaurant->node_id.'-'.$restaurant->name;
                $this->load->model('Restaurant_Model','RestaurantModel');
                $all_restaurants = $this->RestaurantModel->user_restaurants(FALSE,FALSE);
                foreach ($all_restaurants as $restaurants) $this->_clean_obj($restaurants);
                $this->viewdata['all_restaurants'] = $this->utils->chosenOptions($all_restaurants,'node_id','name');

                
                //GET USER AGENTS
                if($user->agents){
                    foreach($user->agents as $agent){
                        $this->viewdata['user_agents'][] = $agent->node_id.'-'.$agent->username;
                    }
                }
                //GET ALL AGENTS
                $agents = $this->UserModel->all(FALSE, 4, FALSE);
                if($agents) $this->viewdata['all_agents'] = $this->utils->chosenOptions($agents,'node_id','username');
            }

        }
        //$this->utils->dump($this->viewdata);die;


        $user_roles = $this->RoleModel->user_roles();
        $this->viewdata['user_roles'] = $this->utils->chosenOptions($user->roles, 'id', 'description');
        foreach ($user->roles as $role) $this->viewdata['roles_user'][] = $role->id . '-' . $role->description;

        if (isset($user->geopoints)) {
            foreach ($user->geopoints as $geopoint) $this->viewdata['user_geopoints'][] = $geopoint->node_id . '-' . $geopoint->administrative_area_level_3;
            $locations = $this->GeopointModel->locations();
            $this->viewdata['locations'] = $this->utils->chosenOptions($locations, 'node_id', 'provincia');
        }

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('usermail', 'Usermail', 'trim|required|xss_clean');
        $this->form_validation->set_rules('plainpwd', 'Password', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else {

/*
            $this->utils->dump($this->postrequest);

            $actual_roles = array();
            foreach ($user->roles as $actual_role) $actual_roles[]=$actual_role->id;
            $new_roles = $this->utils->chosenResult($this->postrequest->user_roles);



            //ERO UN AGENTE ED ORA NON PIU'?
            if(in_array(4,$actual_roles) AND !in_array(4,$new_roles)){
                echo 'RIMUOVO RISTORATORI AGENTE LI ASSOCIO AD ADMIN<br />';
            }else{
                //echo 'MANTENGO RISTORATORI<br />';
                echo 'HO PROVINCE ASSOCIATE?<br />';
                if(!$this->usersession->is_admin AND !isset($this->postrequest->user_locations) OR sizeof($this->postrequest->user_roles)<1){
                    '...NON HO PROVINCE ASSOCIATE!!! ERRORE!';
                }
                if(!$this->usersession->is_admin AND !isset($this->postrequest->user_locations) OR sizeof($this->postrequest->user_roles)<1){
                    '...NON HO PROVINCE ASSOCIATE!!! ERRORE!';
                }
                echo 'HO RISTORATORI ASSOCIATI?<br />';
            }
            //ERO UN RISTORATORE ED ORA NON PIU'?
            if(in_array(3,$actual_roles) AND !in_array(3,$new_roles)){
                echo 'DISASSOCIO GLI AGENTI E LO SEGNALO<br />';
            }else{
                echo 'MANTENGO AGENTI<br />';
            }

            echo '$actual_roles';
            echo '<pre>';
            print_r($actual_roles);
            echo '<pre>';

            echo '$new_roles';
            echo '<pre>';
            print_r($new_roles);
            echo '<pre>';
*/




            //CONTROLLO CHE SIA ASSEGNATO ALMENO UN RUOLO
            if (!isset($this->postrequest->user_roles) OR sizeof($this->postrequest->user_roles)<1) {
                $this->session->set_flashdata('error', $this->lang->line('missing_user_roles'));
                redirect('/users/edit/',$nid, 'refresh');
            }
            //AGENTE: CONTROLLO CHE SIA ASSEGNATO ALMENO UNA PROVINCIA
            $chosen_roles = $this->utils->chosenResult($this->postrequest->user_roles);
            if(in_array(4,$chosen_roles)){
                if(!$this->usersession->is_admin AND !isset($this->postrequest->user_locations) OR sizeof($this->postrequest->user_roles)<1) {
                    $this->session->set_flashdata('error', $this->lang->line('missing_user_locations'));
                    redirect('/users/edit/',$nid, 'refresh');
                }
            }

            //CONTROLLO CHE LO USERNAME SIA UNICO
            if ($this->postrequest->username !== $user->username AND !$this->UserModel->isunique('username', $this->postrequest->username)) {
                $this->session->set_flashdata('error', $this->lang->line('username_not_unique'));
                redirect('/users/edit/',$nid, 'refresh');
            }
            //CONTROLLO CHE LO USERNAME SIA UNICO
            if ($this->postrequest->usermail !== $user->usermail AND !$this->UserModel->isunique('usermail', $this->postrequest->usermail)) {
                $this->session->set_flashdata('error', $this->lang->line('usermail_not_unique'));
                redirect('/users/edit/',$nid, 'refresh');
            }

            //AGGIORNO L'UTENTE
            $update_user = array(
                'node_id' => $nid,
                'username' => $this->postrequest->username,
                'usermail' => $this->postrequest->usermail,
                'plainpwd' => $this->postrequest->plainpwd,
                'userpwd' => $this->utils->encrypt($this->postrequest->plainpwd),
                'first_name' => $first_name = (isset($this->postrequest->first_name)) ? $this->postrequest->first_name : NULL,
                'last_name' => $last_name = (isset($this->postrequest->last_name)) ? $this->postrequest->last_name : NULL,
                'status' => $status = (isset($this->postrequest->status)) ? strip_tags($this->postrequest->status) : NULL,
                'phone' => $phone = (isset($this->postrequest->phone)) ? $this->postrequest->phone : NULL,
            );

            $update_user = array_filter($update_user);
            $this->UserModel->update($update_user);
            //AGGIORNO IL NODO
            if(isset($this->postrequest->first_name) AND isset($this->postrequest->last_name)){
                $this->NodeModel->update(array('id' => $nid, 'name' => $this->postrequest->first_name.' '.$this->postrequest->last_name));
            }else{
                $this->NodeModel->update(array('id' => $nid, 'name' => $this->postrequest->username));
            }

            //AGGIORNO I RUOLI
            $this->RoleModel->delete_user_roles($nid);
            foreach ($this->postrequest->user_roles as $role) $this->RoleModel->insert_user_roles(array('user_id' => $nid,'role_id' => $role));

            //AGENTE: AGGIORNO LE PROVINCIE
            if (isset($this->postrequest->user_locations)) {
                $this->GeopointModel->delete_node_geopoint($nid);
                foreach ($this->utils->chosenResult($this->postrequest->user_locations) as $location) $this->GeopointModel->insert_node_geopoint(array('node_id' => $nid,'geopoint_id' => $location));
            }
            //AGENTE: AGGIORNO I RISTORATORI
            if (isset($this->postrequest->user_owners) AND sizeof($this->postrequest->user_owners)>0) {
                $user_owners = $this->utils->chosenResult($this->postrequest->user_owners);
                $this->load->model('Agent_Model','AgentModel');
                foreach($user_owners as $owner) $this->AgentModel->insert(array('agent_id'=>$nid,'owner_id'=>$owner));
            }
            //RISTORATORE: AGGIORNO GLI AGENTI
            if (isset($this->postrequest->user_agents) AND sizeof($this->postrequest->user_agents)>0) {
                $user_agents = $this->utils->chosenResult($this->postrequest->user_agents);
                $this->load->model('Agent_Model','AgentModel');
                foreach($user_agents as $user_agent) $this->AgentModel->insert(array('owner_id'=>$nid,'agent_id'=>$user_agent));
            }


            //AGGIORNO L'AVATAR
            $this->_user_dirs($nid);
            $this->_avatar($nid);

            $this->session->set_flashdata('success', $this->lang->line('update_success'));
            redirect('/users/edit/'.$nid, 'refresh');
        }

        $this->viewdata['data'] = $user;
        $user_roles = $this->RoleModel->user_roles();
        $this->viewdata['user_roles'] = $this->utils->chosenOptions($user_roles, 'id', 'description');
        foreach ($user->roles as $role)
            $this->viewdata['roles_user'][] = $role->id . '-' . $role->description;

        
        $this->viewdata['plainpwd'] = $this->UserModel->getplainpwd($user->id);
        //$this->utils->dump($this->viewdata);die;

        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_user.js');
        
        if(isset($this->getrequest->json)){ $this->utils->jsonResponse($user);die; }
        if(isset($this->getrequest->node)){ $this->utils->dump($user);die; }
        if(isset($this->getrequest->viewdata)){ $this->utils->dump($this->viewdata);die; }
        
        $this->show('/users/edit');
    }


    public function create() {

        if (!$this->usersession->is_admin) $this->_logout();

        $user = new stdClass;
        $user->type->object = 'users';
        $this->_object_tags($user);
        $this->_get_node_tags_chosen_options($user);

        
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|is_unique['.$this->dbprefix.'users.username]');
        $this->form_validation->set_rules('usermail', 'Usermail', 'trim|required|xss_clean|is_unique['.$this->dbprefix.'users.usermail]');
        $this->form_validation->set_rules('plainpwd', 'Password', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
        } else {
            
            if($this->postrequest->user_roles==4){
                if(!isset($this->postrequest->user_locations) OR sizeof($this->postrequest->user_locations)<1){
                    $this->viewdata['input']=$this->postrequest;
                    $this->session->set_flashdata('error', $this->lang->line('missing_user_locations'));
                    redirect('/users/create/', 'refresh');                                                   
                }
            }
            
            //$this->utils->dump($this->postrequest);die;
            
            //CREA NODO
            $nid = $this->_create(8);
            //CREO L'UTENTE
            $new_user = array(
                'node_id' => $nid,
                'username' => $this->postrequest->username,
                'usermail' => $this->postrequest->usermail,
                'plainpwd' => $this->postrequest->plainpwd,
                'userpwd' => $this->utils->encrypt($this->postrequest->plainpwd),
                'first_name' => $first_name = (isset($this->postrequest->first_name)) ? $this->postrequest->first_name : NULL,
                'last_name' => $last_name = (isset($this->postrequest->last_name)) ? $this->postrequest->last_name : NULL,
                'status' => $status = (isset($this->postrequest->status)) ? strip_tags($this->postrequest->status) : NULL,
                'phone' => $phone = (isset($this->postrequest->phone)) ? $this->postrequest->phone : NULL,
                'avatar'=>$this->config->item('user_default_logo')
            );
            $new_user = array_filter($new_user);           
            $this->UserModel->insert($new_user); 
            //AGGIORNA NODO
            $this->NodeModel->update(array('id' => $nid, 'name' => $this->postrequest->username));
            //ASSEGNO RUOLO
            $this->RoleModel->insert_user_roles(array('user_id' => $nid,'role_id' => $this->postrequest->user_roles));
            //ASSEGNO RUOLO UTENTE
            $this->RoleModel->insert_user_roles(array('user_id' => $nid,'role_id' => 5));
            //AGENTE: AGGIORNO LE PROVINCIE
            if (isset($this->postrequest->user_locations)) {
                $this->GeopointModel->delete_node_geopoint($nid);
                foreach ($this->utils->chosenResult($this->postrequest->user_locations) as $location) $this->GeopointModel->insert_node_geopoint(array('node_id' => $nid,'geopoint_id' => $location));
            }            
            
            //AGGIORNO L'AVATAR
            $this->_user_dirs($nid);
            $this->_avatar($nid);

            $this->registration_mail($nid);
            
            $this->session->set_flashdata('success', $this->lang->line('create_success'));
            redirect('/users/edit/'.$nid, 'refresh');            
//            
//            if(isset($this->postrequest->user_locations) AND sizeof($this->postrequest->user_locations)>0){
//                $user_locations = $this->utils->chosenResult($this->postrequest->user_locations); 
//                $this->utils->dump($user_locations); 
//            }
//           
//           $this->utils->dump($this->postrequest);
//           die; 
        }
        


        $this->load->model('Restaurant_Model', 'RestaurantModel');
        $all_restaurants = $this->RestaurantModel->user_restaurants(FALSE, FALSE);
        foreach ($all_restaurants as $restaurants) $this->_clean_obj($restaurants);
        $this->viewdata['all_restaurants'] = $this->utils->chosenOptions($all_restaurants, 'node_id', 'name');
        $locations = $this->GeopointModel->locations();
        $this->viewdata['locations'] = $this->utils->chosenOptions($locations, 'node_id', 'provincia');
        //GET ALL OWNERS
        $owners = $this->UserModel->all(FALSE, 3, FALSE);
        if ($owners) $this->viewdata['all_owners'] = $this->utils->chosenOptions($owners, 'node_id', 'username');
        $this->load->model('Restaurant_Model', 'RestaurantModel');
        $all_restaurants = $this->RestaurantModel->user_restaurants(FALSE, FALSE);
        foreach ($all_restaurants as $restaurants)
            $this->_clean_obj($restaurants);
        $this->viewdata['all_restaurants'] = $this->utils->chosenOptions($all_restaurants, 'node_id', 'name');
        $agents = $this->UserModel->all(FALSE, 4, FALSE);
        if ($agents) $this->viewdata['all_agents'] = $this->utils->chosenOptions($agents, 'node_id', 'username');
        $locations = $this->GeopointModel->locations();
        $this->viewdata['locations'] = $this->utils->chosenOptions($locations, 'node_id', 'provincia');
        $this->viewdata['data'] = $user;
        $user_roles = $this->RoleModel->user_roles();
        foreach ($user_roles as $user_role) $this->viewdata['user_roles'][$user_role->id]=$this->lang->line($user_role->name);       
        //$this->viewdata['user_roles'] = $this->utils->chosenOptions($user_roles, 'id', 'description');                
        //$this->utils->dump($this->viewdata);die;

        $this->_set_Jquery();
        $this->_set_ChosenJS();
        $this->_set_JqueryValidate();
        $this->_set_custom_js('/assets/js/custom/edit_user.js');
        $this->show('/users/create');
    }
    
    public function activate($nid) {
        $this->NodeModel->update(array('id' => $nid, 'active' => 1));
        $this->activate_mail($nid);
        $this->session->set_flashdata('success', $this->lang->line('activate_success'));
        redirect($_SERVER['HTTP_REFERER'], 'refresh');
    }

    public function deactivate($nid) {
        $this->NodeModel->update(array('id' => $nid, 'active' => 0));
        $this->deactivate_mail($nid);
        $this->session->set_flashdata('success', $this->lang->line('deactivate_success'));
        redirect($_SERVER['HTTP_REFERER'], 'refresh');
    }    

    public function testactivationmail($nid){
        $this->registration_mail($nid);
        $this->activate_mail($nid);
        $this->deactivate_mail($nid);
    }
    
    public function registration_mail($nid){
        
            $user = $this->_get($nid,TRUE);
            $user->plainpwd = $this->UserModel->getplainpwd($user->id);            
            $data['user']=$user;            
            $this->load->library('parser');
            $data['subject']='Benvenuto in Gustag!';
            $data['altbody'] = $this->parser->parse('mails/registration_txt', $data,TRUE);
            $data['htmlbody'] = $this->parser->parse('mails/registration', $data,TRUE);  
            if($this->_send_mail($data)) $this->UserModel->update(array('node_id'=>$nid,'registration_mail'=>1));
            
    }
    
    public function activate_mail($nid){
        
            $user = $this->_get($nid,TRUE);
            $user->plainpwd = $this->UserModel->getplainpwd($user->id);            
            $data['user']=$user;            
            $this->load->library('parser');
            $data['subject']='Attivazione profilo Gustag!';
            $data['altbody'] = $this->parser->parse('mails/activate_txt', $data,TRUE);
            $data['htmlbody'] = $this->parser->parse('mails/activate', $data,TRUE);  
            if($this->_send_mail($data)) return TRUE;
            
    }    
    
    public function deactivate_mail($nid){
        
            $user = $this->_get($nid,TRUE);
            $user->plainpwd = $this->UserModel->getplainpwd($user->id);            
            $data['user']=$user;            
            $this->load->library('parser');
            $data['subject']='Disattivazione profilo Gustag';
            $this->load->config('mail');
            $data['smtp_user']=$this->config->item('smtp_user');                    
            $data['altbody'] = $this->parser->parse('mails/deactivate_txt', $data,TRUE);
            $data['htmlbody'] = $this->parser->parse('mails/deactivate', $data,TRUE);  
            if($this->_send_mail($data)) return TRUE;
            
    }    
    


}
