$(function() {

    var min_char = 2;
    var separator = ", ";
    var element = 'province';
    var searchurl = '/generic/getprovince';

    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    $("#"+element+"")
            .bind("keydown", function(event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).data("ui-autocomplete").menu.active) {
            event.preventDefault();
        }
    })
            .autocomplete({
        source: function(request, response) {

            $.getJSON(searchurl, {
                term: extractLast(request.term)
            }, response);
        },
        search: function() {
            var term = extractLast(this.value);
            if (term.length < min_char) {
                return false;
            }
        },
        focus: function() {
            return false;
        },
        select: function(event, ui) {
            var terms = split(this.value);
            terms.pop();
            terms.push(ui.item.value);
            terms.push("");
            this.value = terms.join(separator);
            return false;
        }
    });
});   