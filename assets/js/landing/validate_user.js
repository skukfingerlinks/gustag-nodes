$(function() {

    $("#register_form").validate({
        rules: {
            email: {
                required: true,
                remote: {
                    url: "/landing/unique",
                    //type: "post",
                    async:false
                }
            },
        },
        messages: {
            email: {
                required: "Indirizzo mail mancante",
                remote: "Indirizzo gi&agrave; presente"
            },
        }
    });
});