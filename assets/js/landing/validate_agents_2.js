$(function() {

    $.validator.addMethod("selectTextNotEquals", function(value, select, arg) {
        return arg != $(select).find('option:selected').text();
    }, "Text must not equal arg.");

    $.validator.addMethod("textOnly",
            function(value, element) {
                //return /^[a-zA-Z()]+$/.test(value);
                return /^(?:(?:[a-zA-Z0-9]+ *)+)$/.test(value);

            },
            "Alpha Characters Only."
            );
    $.validator.addMethod("maggiorenne",
            function(value, element) {
                return parseInt(value) >= 18;
            },
            "You are a boy!"
            );
    $.validator.addMethod("isemail",
            function(value, element) {
                var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/i);
                return emailRegex.test(value);
            },
            "Not a email address!"
            );





    $("#agents_form").validate({
        rules: {
            first_name: {
                required: true,
                minlength: 3,
                textOnly: true
            },
            last_name: {
                required: true,
                minlength: 3,
                textOnly: true
            },
            age: {
                required: true,
                minlength: 2,
                number: true,
                maggiorenne: true
            },
            usermail: {
                required: true,
                email: true,
            },
            phone: {
                required: true,
                number: true,
            },
            province: {
                required: true,
            }

        },
        messages: {
            first_name: {
                required: "Nome mancante",
                minlength: "Il nome deve essere composto da almeno 3 caratteri",
                textOnly: "Il nome non pu&ograve;  contenere numeri o caratteri speciali",
            },
            last_name: {
                required: "Cognome mancante",
                minlength: "Il cognome deve essere composto da almeno 3 caratteri",
                textOnly: "Il cognome non pu&ograve;  contenere numeri o caratteri speciali",
                //
            },
            age: {
                required: "Et&agrave;   mancante",
                minlength: "L'et&agrave; deve essere composta da almeno 2 cifre",
                number: "L'et&agrave;   inserita non &egrave; valida",
                maggiorenne: "Possono registrarsi solo i maggiorenni",
            },
            usermail: {
                required: "indirizzo mail non valido",
            },
            phone: {
                required: "recapito telefonico non valido",
            },
            province: {
                required: "Indica almeno una provincia"
            },
        }
    });
});