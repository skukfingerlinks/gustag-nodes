$.validator.setDefaults({ignore: ''});

$.validator.addMethod("notEqual", function(value, element, param) {
 return this.optional(element) || value != $(param).val();
}, "This has to be different...");


function selectAll(selectelement) {
    $(selectelement).prop('selected', true);
}

$().ready(function() {

    //RESET CUSTOM ERROR MESSAGE
    $('.long_input').bind('change keypress click', function(e) {
        var elemid = $(this).attr('id');
        $('#'+elemid+'_error').html('');
        //var elemerror = $('#'+elemid+'_error').html();
        //alert(elemerror);
    });

    $("#website").click(function(event) {
        if (!$.trim(this.value).length)
            $(this).val('http://');
    });

    $("#restaurant_info_all").click(function(event) {
        selectAll("#restaurant_info");
    });

    $("#phone").bind('change keypress', function(e) {
        //$.trim(this).val();
        $(this).val($(this).val().replace(' ', ''));
    });

    $("#edit_restaurant").validate({
        rules: {
            name: "required",
            desc: "required",
            address: "required",
            phone: {
                required: true,
                number: true
            },
            seats: {
                //required: true,
                number: true,
                min: 4,
            },
        },
        messages: {
            name: "Perfavore inserisci il nome del Locale",
            desc: "Perfavore inserisci una breve descrizione del Locale",
            address: "Perfavore inserisci l'indirizzo del Locale",
            phone: {
                required: "Perfavore inserisci il telefono del Locale",
                number: "Perfavore inserisci un numero di telefono valido"
            },
            seats: {
                //required: "Perfavore inserisci il numero minimo di posti riservati a Gustag",
                number: "Perfavore inserisci un numero valido",
                min: "Perfavore inserisci un numero maggiore di 4",
            },
        }
    });

});