function showMap(lat, lng, locations, mapid, defzoom) {

    var map = new google.maps.Map(document.getElementById(mapid), {
        zoom: defzoom,
        center: new google.maps.LatLng(lat, lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}

function showPoint(lat,lng,mapid, defzoom) {
    var point = new google.maps.LatLng(lat,lng);
    var mapOptions = {
        zoom: defzoom,
        center: point,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById(mapid), mapOptions);

    var marker = new google.maps.Marker({
        position: point,
        map: map,
        //title: 'Hello World!'
    });
}