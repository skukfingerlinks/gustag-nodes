function openEventDialog(content){
    
    $("#eventcalendar_dialog").dialog({
        width: 350,
        height:350,
        modal: true,
    });
    
    $("#eventcalendar_dialog_content").html('<br />'+content);    
}


function deleteEventDate(nid, eventdate) {
    $.post("/ajax/deleteEventDate", {node_id: nid, date: eventdate}).done(function(data) {
        //alert(data);
        //$("#event_dialog").dialog({width: 350,height:150});
        //$("#event_dialog_content").html('<br />Rimossa data '+caldate);
        //alert( "Data Loaded: " + data );
    });

}
function setEventDate(nid, eventdate) {
    

    $.post("/ajax/setEventDate", {node_id: nid, date: eventdate}).done(function(data) {
        //$("#event_dialog").dialog({width: 350,height:150});
        //$("#event_dialog_content").html('<br />Aggiunta data '+caldate);
        //alert( "Data Loaded: " + data );
    });
}
function resetEvent(nid) {
    alert('resetEvent');
    $.post("/ajax/resetEventDate", {node_id: nid}).done(function(data) {
        //$("#event_dialog").dialog({width: 350,height:150});
        //$("#event_dialog_content").html('<br />Aggiunta data '+caldate);
        //alert( "Data Loaded: " + data );
    });
}

$(document).ready(function() {

    var nid = $("#nid").val();
    

    $('#reset').click(function() {
        event.preventDefault();
        //resetEvent(nid);
        //alert('eccolo!');
        openEventDialog('<p>Vuoi cancellare tutte le date dell\'Evento?</p><p>Verranno rimosse tutte le date sinora settate</p><p><a href="/events/removedates/'+nid+'" class="button">S&igrave;, Rimuovi tutto</a></p>');
        /*
        event.preventDefault();
        $('.pickevent').each(function(index) {
            $(this).removeClass("datehighlight");
        })
        $('.assigned').each(function(index) {
            $(this).removeClass("assigned");
        })
        */

    });

    $(".pickevent").click(function(event) {
        event.preventDefault();
        
        //$(this).toggleClass("datehighlight");
        var eventdate = $(this).attr("title");
        $('#missing_dates').remove();
        //$('#missing_dates').removeClass("warning");

        //RIMUOVO LA DATA DALLE DATE SE L'ELEMENTO HA GIA LA CLASSE 'assigned';
        if ($(this).hasClass("assigned")) {
            deleteEventDate(nid, eventdate);
            $(this).removeClass("datehighlight");
            $(this).removeClass("assigned");
        } else {
            setEventDate(nid, eventdate);
            $(this).addClass("assigned");
        }
        $("#dates").val($("#dates").val() + ',' + eventdate);
    });


});
