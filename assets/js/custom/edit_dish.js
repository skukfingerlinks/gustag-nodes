$.validator.setDefaults({ignore: ''});

$().ready(function() {

    /*
    $("#name").bind('blur', function(e) {
        if( $(this).val() ) {
          $("#desc").html($(this).val());
        }        
    });
    */


    $("#edit_dish").validate({
        rules: {
            name: "required",
            desc: "required",            
        },
        messages: {
            name: "Perfavore inserisci il nome del Piatto",
            desc: "Perfavore inserisci una breve descrizione del Piatto",
        }
    });

});