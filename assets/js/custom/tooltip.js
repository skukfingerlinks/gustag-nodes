$(function() {

    $('.tooltip').tooltip({
        tooltipClass: "gartner_tooltip",
        content: function() {
            return $(this).attr('data');
        }
    });

});