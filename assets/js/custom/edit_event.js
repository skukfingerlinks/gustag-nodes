$.validator.setDefaults({ignore: ''});

$.validator.addMethod('integer', function(value, element, param) {
    return (value != 0) && (value == parseInt(value, 10));
}, 'Please enter a non zero integer value!');

function selectAll(selectelement) {
    $(selectelement).prop('selected', true);
}

$().ready(function() {

    $("#website").click(function(event) {
        if (!$.trim(this.value).length)
            $(this).val('http://');
    });

    $("#event_info_all").click(function(event) {
        selectAll("#event_info");
    });

    $("#phone").bind('change keypress', function(e) {
        //$.trim(this).val();
        $(this).val($(this).val().replace(' ', ''));
    });

    $("#edit_event").validate({
        rules: {
            name: "required",
            desc : "required",
            phone: {
                //required: true,
                number: true,
                minlength: 8
            },
            /*
            seats: {
                number: true,
                min: 1,
                integer: true,
            },
            */
            address: {
                required: true,
                minlength: 2
            },
            /*
             addressinfo: {
             required: true,
             minlength: 2
             },
             */
            /*
             username: {
             required: true,
             minlength: 2
             },
             password: {
             required: true,
             minlength: 5
             },
             confirm_password: {
             required: true,
             minlength: 5,
             equalTo: "#password"
             },
             email: {
             required: true,
             email: true
             },
             topic: {
             required: "#newsletter:checked",
             minlength: 2
             },
             agree: "required"
             */
        },
        messages: {
            name: "Perfavore inserisci il nome dell\'Evento",
            desc: "Perfavore inserisci una breve descrizione dell\'Evento",
            phone: {
                //required: "Perfavore inserisci il telefono dell\'Evento",
                number: "Perfavore inserisci un numero di telefono valido",
                minlength: "Perfavore inserisci un numero di telefono valido (almeno 8 cifre)"
            },
            /*
            seats: {
                number: "Perfavore inserisci un numero valido",
                min: "Perfavore inserisci un numero maggiore di zero",
                integer: "Perfavore inserisci un numero intero"
            },
            */
            address: {
                required: "Perfavore inserisci l\'indirizzo dell\'Evento",
                minlength: "Perfavore inserisci l\'indirizzo completo dell\'Evento",
            },
            /*
             addressinfo: {
             required: "Perfavore inserisci l\'indirizzo dell\'Evento",
             minlength: "Perfavore inserisci l\'indirizzo completo dell\'Evento",
             },
             */
            /*
             username: {
             required: "Please enter a username",
             minlength: "Your username must consist of at least 2 characters"
             },
             password: {
             required: "Please provide a password",
             minlength: "Your password must be at least 5 characters long"
             },
             confirm_password: {
             required: "Please provide a password",
             minlength: "Your password must be at least 5 characters long",
             equalTo: "Please enter the same password as above"
             },
             email: "Please enter a valid email address",
             agree: "Please accept our policy"
             */
        }
    });

});