$().ready(function() {

    $("#usermail").bind('change blur', function(e) {
          var email = $(this).val();
          var array = email.split('@');
          $('#username').val(array[0]);
          //console.log(array[0]);
                      
    });

//    $("#user_roles").change(function () {
//        alert('changed!');
//    });

    
    /* AGENTS */
    $('#agents_locations').hide();    
    $("#user_roles").change(function () {           
        var role_selected = $(this).find(":selected").val(); 
        //alert(role_selected);
        console.log(role_selected);
        
        if(role_selected==4){
            $('#agents_locations').show();
            //$("#dialog_agents_locations").dialog({width: 350,height:350,modal: true,});  
            $('#phone_label').addClass("req");
        }else{
            $('#agents_locations').hide();
            $('#phone_label').removeClass("req");
        }
        
        
    }).trigger('change'); 
    /* AGENTS */

/*    
$("select#user_roles").change(function(){
    alert($(this).val());
});  
*/    

    $("#phone").bind('change keypress', function(e) {
        $(this).val($(this).val().replace(' ', ''));
    });

    $("#edit_owner").validate({
        rules: {
            username: "required",
            usermail: "required",
            plainpwd: {
             required: true,
             minlength: 5
            },
            phone: {
                required: true,
                number: true,
                minlength: 8
            },
        },
        messages: {
            username: "Perfavore inserisci lo username dell\'Utente",
            usermail: "Perfavore inserisci la usermail dell\'Utente",
            plainpwd: {
                required: "Perfavore inserisci la password dell\'Utente",
                minlength: "la password dell\'Utente deve contenere almeno 5 caratteri",
            },
            phone: {
                required: "Perfavore indica un numero di telefono",
                number: "Perfavore inserisci un numero di telefono valido",
                minlength: "Perfavore inserisci un numero di telefono valido (almeno 8 cifre)"
            },
        }
    });

});