function openCalendarDialog(content){
    
    $("#calendar_dialog").dialog({
        width: 350,
        height:350,
        modal: true,
    });
    
    $("#calendar_dialog_content").html('<br />'+content);    
}


function deleteCalendarDate(nid, caldate) {
    $.post("/ajax/deleteCalendarDate", {node_id: nid, date: caldate}).done(function(data) {
        //$("#calendar_dialog").dialog({width: 350,height:150});
        //$("#calendar_dialog_content").html('<br />Rimossa data '+caldate);
        //alert( "Data Loaded: " + data );
    });

}
function setCalendarDate(nid, caldate) {
    
    $.post("/ajax/setCalendarDate", {node_id: nid, date: caldate}).done(function(data) {
        //$("#calendar_dialog").dialog({width: 350,height:150});
        //$("#calendar_dialog_content").html('<br />Aggiunta data '+caldate);
        //alert( "Data Loaded: " + data );
    });
}
function resetCalendar(nid) {
    
    $.post("/ajax/resetCalendarDate", {node_id: nid}).done(function(data) {
        //$("#calendar_dialog").dialog({width: 350,height:150});
        //$("#calendar_dialog_content").html('<br />Aggiunta data '+caldate);
        //alert( "Data Loaded: " + data );
    });
}

$(document).ready(function() {

    var nid = $("#nid").val();
    

    $('#reset').click(function() {
        event.preventDefault();        
        openCalendarDialog('<p>Vuoi cancellare tutte le date di chiusura?</p><p>Verranno rimosse tutte le date di chiusura sinora settate</p><p><a href="/restaurants/removedates/'+nid+'" class="button">S&igrave;, Rimuovi tutto</a></p>');
        /*
        event.preventDefault();
        $('.pick').each(function(index) {
            $(this).removeClass("datehighlight");
        })
        $('.closed').each(function(index) {
            $(this).removeClass("closed");
        })
        */

    });

    $(".pick").click(function(event) {
        event.preventDefault();
        
        //$(this).toggleClass("datehighlight");
        var calendardate = $(this).attr("title");
        //$('#missing_dates').remove();
        //$('#missing_dates').removeClass("warning");

        //RIMUOVO LA DATA DALLE DATE SE L'ELEMENTO HA GIA LA CLASSE 'closed';
        if ($(this).hasClass("closed")) {
            deleteCalendarDate(nid, calendardate);
            $(this).removeClass("datehighlight");
            $(this).removeClass("closed");
        } else {
            setCalendarDate(nid, calendardate);
            $(this).addClass("closed");
        }
        $("#dates").val($("#dates").val() + ',' + calendardate);
    });


});
